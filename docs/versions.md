Version history
===============

version 3.3.2
-------------

- Allow totals in cart to optionally show multiple vouchers with voucher title as a description rather than the former "vouchers"
- Added a boolean attribute code to control whether a SKU is set as "promotional" in basket calls ([get-basket-price](https://docs.spaaza.com/api/get-basket-price/) and [add-basket](https://docs.spaaza.com/api/add-basket/)), allowing control over whether promotional campaign logic is applied to that SKU - see [spend_on_promotional_items](https://docs.spaaza.com/api/campaign-types-fields/#general-campaign-fields) in Spaaza campaigns documentation

version 3.3.1
-------------

- Added percentage discount voucher handling - see [voucher types](https://docs.spaaza.com/api/vouchers/#voucher-types) Spaaza API documentation for more details
- Fixed voucher locking issue`

Version 3.3.0
-------------

- Add branch_business_owner_code config + use it in basket requests

Version 3.2.4
-------------

- Made module compatible with PHP 8.1

Version 3.2.3
-------------

- Add Spaaza config to debug bundle zip file.
- Bugfixes

Version 3.2.1
-------------

- Documentation updates

Version 3.2.0
-------------

- Add referral_code and signup_referral_code extension attributes.
- Add data validator before customer save.
- Bugfixes

Version 3.1.2
-------------

- A bugfix release that can handle a discount (by a cart price rule) on shipping costs.

Version 3.1.0
-------------

- The REST API is now ready for use with multiple chains. See `rest-api.md` for changes in endpoints. All changes are backwards compatible and nothing will change if you only have one Spaaza chain configured.

Version 3.0.0
-------------

- Changes in this version are all about supporting customers with 'website' scope. You can now have different Spaaza connection settings for different websites.

### Backward incompatible changes in public methods

- `\Spaaza\Loyalty\Model\Connector\Voucher::lockVoucher()` now needs a customer model instead of only a customer id.

Version 2.2.0
-------------

- Added a 'Download Debug Info' button in the Magento backend which allows a backend user to download the log files and info about all outgoing API requests.

Version 2.0.2
-------------

- `\Spaaza\Loyalty\Model\VoucherManagement::getAvailableVouchers()` now has an extra parameter `$includeBasketCampaignVouchers` which controls whether basket campaign vouchers will be returned. Previous (pre-2.0.0) versions of the module would never return basket campaign vouchers because Spaaza didn't know about the basket contents, so setting the default value of this parameter to `false` makes the module a little more backward compatible.

Version 2.0.1
-------------

- Documentation changes.

Version 2.0.0
-------------

### Backward incompatible changes

- `\Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface` has been moved to `\Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface`  
  This is because the voucher distribution will now also be stored on quote level. Also, the data model has been moved from `Spaaza\Loyalty\Model\Data\Order\VoucherDistributionItem` to `Spaaza\Loyalty\Model\Data\VoucherDistributionItem`.
- Renamed `\Spaaza\Loyalty\Model\Connector\Sales` to `\Spaaza\Loyalty\Model\Connector\Order`. The events still get dispatched, but are  deprecated - please use plugins.
- Moved `\Spaaza\Loyalty\Model\Connector\Sales::sendCreditMemo()` to a separate class: `\Spaaza\Loyalty\Model\Connector\Ceditmemo`. The events still get dispatched, but are deprecated - please use plugins.

### Deprecated

- All helpers except the 'data' helper have been marked as deprecated. If you created a plugin for one of the helper methods, see the phpDoc of the original helper method to get hints about moving them. Please note that the 'data' helper _will be_ deprecated in the near future.
- The events that are being dispatched in the Sales related connector classes (`Order` and `Creditmemo`) are deprecated. Find a method in the class to create a plugin for.
