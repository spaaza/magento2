<?php

namespace Spaaza\Loyalty\Model\Config\Source;

class EntityType implements \Magento\Framework\Data\OptionSourceInterface
{
    const ENTITY_TYPE_ORDER = 'order';
    const ENTITY_TYPE_CREDITMEMO = 'creditmemo';
    const ENTITY_TYPE_CUSTOMER = 'customer';
    const ENTITY_TYPE_QUOTE = 'quote';

    /**
     * @inheritdoc
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        foreach ($this->getAllOptions() as $option) {
            $result[] = [
                'label' => $option['label'],
                'value' => $option['value'],
            ];
        }
        return $result;
    }

    /**
     * Get all options with some 'special keys'
     *
     * @return array  Array of associative arrays with option info
     */
    public function getAllOptions()
    {
        return [
            [
                'value' => self::ENTITY_TYPE_ORDER,
                'label' => __('Order'),
            ],
            [
                'value' => self::ENTITY_TYPE_CREDITMEMO,
                'label' => __('Credit Memo'),
            ],
            [
                'value' => self::ENTITY_TYPE_CUSTOMER,
                'label' => __('Customer'),
            ],
            [
                'value' => self::ENTITY_TYPE_QUOTE,
                'label' => __('Quote'),
            ],
        ];
    }

    /**
     * Get the label for a value from the option list
     *
     * @param string $value
     * @return string
     */
    public function getLabelForValue($value)
    {
        foreach ($this->getAllOptions() as $option) {
            if (isset($option['value']) && $option['value'] == $value && isset($option['label'])) {
                return $option['label'];
            }
        }
        return $value;
    }
}
