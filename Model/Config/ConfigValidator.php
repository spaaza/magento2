<?php

namespace Spaaza\Loyalty\Model\Config;

class ConfigValidator
{
    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $moduleConfig;

    /**
     * @var \Magento\Customer\Model\Config\Share
     */
    protected $customerShareConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Spaaza\Loyalty\Model\Config $moduleConfig,
        \Magento\Customer\Model\Config\Share $customerShareConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->moduleConfig = $moduleConfig;
        $this->customerShareConfig = $customerShareConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Validate customer scope specific issues
     *
     * @return bool
     * @throws \Spaaza\Loyalty\Exception\ConfigValidationException
     */
    public function validateCustomerScope()
    {
        $uniqueWebsiteIds = $this->getUniqueWebsiteIds();
        if (count($uniqueWebsiteIds) === 1) {
            return true;
        }

        $uniqueConfiguredChainIds = $this->getUniqueConfiguredChainIds();
        if ($this->customerShareConfig->isWebsiteScope()
            && (count($uniqueWebsiteIds) !== count($uniqueConfiguredChainIds))
        ) {
            throw new \Spaaza\Loyalty\Exception\ConfigValidationException(
                __('Not all websites have a unique configuration.'),
                __('Because customers are configured to have a website sharing scope, you need to have a different Spaaza chain configured per website.') // phpcs:ignore
            );
        }
        if ($this->customerShareConfig->isGlobalScope()
            && count($uniqueConfiguredChainIds) > 1
        ) {
            throw new \Spaaza\Loyalty\Exception\ConfigValidationException(
                __('You have configured more than one Spaaza chain.'),
                __('Because customers are configured to have a global sharing scope, you cannot have website specific Spaaza API Client configuration.') // phpcs:ignore
            );
        }
        return true;
    }

    /**
     * Get all unique configured chain ids
     *
     * @return int[]
     */
    protected function getUniqueConfiguredChainIds(): array
    {
        $websiteIds = $this->getUniqueWebsiteIds();
        $chainIds = [];
        foreach ($websiteIds as $websiteId) {
            $chainIds[] = $this->moduleConfig->getChainId($websiteId);
        }
        return array_unique($chainIds);
    }

    /**
     * Get the ids of all unique, active websites
     *
     * A website is 'active' if it has at least one active store.
     *
     * @return int[]
     */
    protected function getUniqueWebsiteIds(): array
    {
        $stores = $this->storeManager->getStores(false);
        $activeWebsiteIds = [];
        foreach ($stores as $store) {
            if ($store->getIsActive() && $store->getWebsiteId() > 0) {
                $activeWebsiteIds[] = $store->getWebsiteId();
            }
        }
        return array_unique($activeWebsiteIds);
    }
}
