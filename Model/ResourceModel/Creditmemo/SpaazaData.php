<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Creditmemo;

/**
 * Class SpaazaData
 *
 * @method save(\Spaaza\Loyalty\Api\Data\Creditmemo\SpaazaDataInterface $object)
 */
class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_creditmemo_data', 'creditmemo_id');
    }
}
