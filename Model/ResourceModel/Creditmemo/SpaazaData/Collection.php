<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'creditmemo_id';

    protected function _construct()
    {
        $this->_init(
            \Spaaza\Loyalty\Model\Creditmemo\SpaazaData::class,
            \Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData::class
        );
    }
}
