<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Order;

use Spaaza\Loyalty\Api\Data\Order\SpaazaDataInterface as OrderSpaazaDataInterface;
use Spaaza\Loyalty\Model\Order\SpaazaData as SpaazaDataModel;

/**
 * Class SpaazaData
 *
 * @method save(SpaazaDataInterface $object)
 */
class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherInfoSerializer
     */
    protected $voucherInfoSerializer;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherDistributionSerializer
     */
    protected $voucherDistributionSerializer;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Spaaza\Loyalty\Model\VoucherInfoSerializer $voucherInfoSerializer,
        \Spaaza\Loyalty\Model\VoucherDistributionSerializer $voucherDistributionSerializer,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->voucherInfoSerializer = $voucherInfoSerializer;
        $this->voucherDistributionSerializer = $voucherDistributionSerializer;
    }

    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_order_data', 'order_id');
    }

    /**
     * @param SpaazaDataModel $object
     * @return SpaazaData
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->serializeData($object);
        return parent::_beforeSave($object);
    }

    /**
     * @param SpaazaDataModel $object
     * @return SpaazaData
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->unserializeData($object);
        return parent::_afterSave($object);
    }

    /**
     * @param SpaazaDataModel $object
     */
    private function serializeData($object)
    {
        $vouchers = $object->getData(OrderSpaazaDataInterface::KEY_VOUCHERS);
        $object->setData(
            OrderSpaazaDataInterface::KEY_VOUCHERS,
            $this->voucherInfoSerializer->serializeVouchers($vouchers)
        );
        $voucherDistribution = $object->getData(OrderSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION);
        $object->setData(
            OrderSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION,
            $this->voucherDistributionSerializer->serializeVoucherDistribution($voucherDistribution)
        );
    }

    /**
     * @param SpaazaDataModel $object
     */
    private function unserializeData($object)
    {
        $vouchers = $object->getData(OrderSpaazaDataInterface::KEY_VOUCHERS);
        $object->setData(
            OrderSpaazaDataInterface::KEY_VOUCHERS,
            $this->voucherInfoSerializer->unserializeVouchers($vouchers)
        );
        $voucherDistribution = $object->getData(OrderSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION);
        $object->setData(
            OrderSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION,
            $this->voucherDistributionSerializer->unserializeVoucherDistribution($voucherDistribution)
        );
    }
}
