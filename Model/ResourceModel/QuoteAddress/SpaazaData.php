<?php

namespace Spaaza\Loyalty\Model\ResourceModel\QuoteAddress;

use Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface as QuoteAddressSpaazaDataInterface;
use Spaaza\Loyalty\Model\QuoteAddress\SpaazaData as SpaazaDataModel;

class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherInfoSerializer
     */
    protected $voucherInfoSerializer;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherDistributionSerializer
     */
    protected $voucherDistributionSerializer;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Spaaza\Loyalty\Model\VoucherInfoSerializer $voucherInfoSerializer,
        \Spaaza\Loyalty\Model\VoucherDistributionSerializer $voucherDistributionSerializer,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
        $this->voucherInfoSerializer = $voucherInfoSerializer;
        $this->voucherDistributionSerializer = $voucherDistributionSerializer;
    }

    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_quote_address_data', 'quote_address_id');
    }

    /**
     * @param SpaazaDataModel $object
     * @return SpaazaData
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->serializeData($object);
        return parent::_beforeSave($object);
    }

    /**
     * @param SpaazaDataModel $object
     * @return SpaazaData
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->unserializeData($object);
        return parent::_afterSave($object);
    }

    /**
     * @param SpaazaDataModel $object
     */
    private function serializeData($object)
    {
        $vouchers = $object->getData(QuoteAddressSpaazaDataInterface::KEY_VOUCHERS);
        $object->setData(
            QuoteAddressSpaazaDataInterface::KEY_VOUCHERS,
            $this->voucherInfoSerializer->serializeVouchers($vouchers)
        );
        $voucherDistribution = $object->getData(QuoteAddressSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION);
        $object->setData(
            QuoteAddressSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION,
            $this->voucherDistributionSerializer->serializeVoucherDistribution($voucherDistribution)
        );
    }

    /**
     * @param SpaazaDataModel $object
     */
    private function unserializeData($object)
    {
        $vouchers = $object->getData(QuoteAddressSpaazaDataInterface::KEY_VOUCHERS);
        $object->setData(
            QuoteAddressSpaazaDataInterface::KEY_VOUCHERS,
            $this->voucherInfoSerializer->unserializeVouchers($vouchers)
        );
        $voucherDistribution = $object->getData(QuoteAddressSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION);
        $object->setData(
            QuoteAddressSpaazaDataInterface::KEY_VOUCHER_DISTRIBUTION,
            $this->voucherDistributionSerializer->unserializeVoucherDistribution($voucherDistribution)
        );
    }
}
