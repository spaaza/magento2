<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Customer;

class SpaazaData extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_isPkAutoIncrement = false;
        $this->_init('spaaza_customer_data', 'customer_id');
    }

    /**
     * Load an object
     *
     * Added 'website id' to limit loading to a specific website.
     *
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param mixed $value
     * @param string $field field to load by (defaults to model id)
     * @param int|null $websiteId  Only look for customers associated to this specific website
     * @return $this
     */
    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null, int $websiteId = null)
    {
        $object->beforeLoad($value, $field);
        if ($field === null) {
            $field = $this->getIdFieldName();
        }

        $connection = $this->getConnection();
        if ($connection && $value !== null) {
            $select = $this->_getLoadSelect($field, $value, $object, $websiteId);
            $data = $connection->fetchRow($select);

            if ($data) {
                $object->setData($data);
            }
        }

        $this->unserializeFields($object);
        $this->_afterLoad($object);
        $object->afterLoad();
        $object->setOrigData();
        $object->setHasDataChanges(false);

        return $this;
    }

    /**
     * Retrieve select object for load object data
     *
     * Added 'website id' to limit loading to a specific website.
     *
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param int|null $websiteId  Only look for customers associated to this specific website
     * @return \Magento\Framework\DB\Select
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function _getLoadSelect($field, $value, $object, ?int $websiteId = null)
    {
        if (!$websiteId) {
            return parent::_getLoadSelect($field, $value, $object);
        }
        $field = $this->getConnection()->quoteIdentifier(sprintf('%s.%s', $this->getMainTable(), $field));
        $select = $this->getConnection()->select()->from($this->getMainTable())->where($field . '=?', $value);
        $select->join(
            ['customer' => 'customer_entity'],
            'customer.entity_id = ' . $this->getMainTable() . '.customer_id'
        );
        $select->where('customer.website_id = ?', $websiteId);
        return $select;
    }
}
