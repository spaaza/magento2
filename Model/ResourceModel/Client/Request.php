<?php

namespace Spaaza\Loyalty\Model\ResourceModel\Client;

class Request extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('spaaza_client_request', 'request_id');
    }

    /**
     * Cleanup requests based on status and age in days
     *
     * @param mixed $status Array of statuses or a string
     * @param int $days
     * @return int  The number of deleted requests
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function cleanupRequests($status, $days)
    {
        $date = date('Y-m-d H:i:s', strtotime(sprintf('-%d days', $days)));
        return $this->getConnection()->delete(
            $this->getMainTable(),
            [
                'status IN (?)' => $status,
                'created_at < ?' => $date,
            ]
        );
    }
}
