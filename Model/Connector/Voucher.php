<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\InputException;
use Spaaza\Loyalty\Model\Config;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;
use Magento\Sales\Api\Data\OrderInterface;

class Voucher
{
    const VOUCHER_LOCK_PERIOD = 600;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    private $requestQueue;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    private $requestFactory;

    /**
     * @var Registry
     */
    private $connectorRegistry;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    private $customerSpaazaDataManagement;

    public function __construct(
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        Registry $connectorRegistry,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $customerSpaazaDataManagement,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Config $config
    ) {
        $this->requestQueue = $requestQueue;
        $this->requestFactory = $requestFactory;
        $this->connectorRegistry = $connectorRegistry;
        $this->config = $config;
        $this->customerSpaazaDataManagement = $customerSpaazaDataManagement;
        $this->storeManager = $storeManager;
    }

    /**
     * Claim a voucher (synchronous)
     *
     * @param string $voucherKey
     * @param CustomerInterface $customer
     * @return array  The json decoded API response
     * @throws InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function claimVoucher($voucherKey, CustomerInterface $customer)
    {
        $spaazaData = $this->customerSpaazaDataManagement->applyExtensionAttributes($customer);
        if (!$spaazaData->getUserId()) {
            throw new InputException(__('Cannot do voucher actions for a non-Spaaza user'));
        }

        $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())
            ->getDefaultStore()
            ->getId();

        /** @var RequestInterface $request */
        $request = $this->requestFactory->create()
            ->setStoreId($storeId)
            ->setPath('claim-voucher.json')
            ->setPayload(
                [
                    'voucher_key' => $voucherKey,
                    'user_id' => $spaazaData->getUserId(),
                ]
            )
            ->setMethod(RequestInterface::METHOD_POST)
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true]);

        $result = $this->requestQueue->sendSynchronousRequest($request, true);
        $this->connectorRegistry->deleteCard($customer->getId());
        return $result;
    }

    /**
     * Unclaim a voucher (synchronous)
     *
     * @param string $voucherKey
     * @param CustomerInterface $customer
     * @return array  The json decoded API response
     * @throws InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function unclaimVoucher($voucherKey, CustomerInterface $customer)
    {
        $spaazaData = $this->customerSpaazaDataManagement->applyExtensionAttributes($customer);
        if (!$spaazaData->getUserId()) {
            throw new InputException(__('Cannot do voucher actions for a non-Spaaza user'));
        }

        $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())
            ->getDefaultStore()
            ->getId();

        /** @var RequestInterface $request */
        $request = $this->requestFactory->create()
            ->setStoreId($storeId)
            ->setPath('unclaim-voucher.json')
            ->setPayload(
                [
                    'voucher_key' => $voucherKey,
                    'user_id' => $spaazaData->getUserId(),
                ]
            )
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setMethod(RequestInterface::METHOD_POST)
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true]);

        $result = $this->requestQueue->sendSynchronousRequest($request, true);
        $this->connectorRegistry->deleteCard($customer->getId());
        return $result;
    }

    /**
     * Lock a voucher (asynchronous if needed)
     *
     * @param string $voucherKey
     * @param CustomerInterface $customer
     * @param OrderInterface|null $order
     * @return void
     */
    public function lockVoucher($voucherKey, CustomerInterface $customer, OrderInterface $order = null)
    {
        $payload = [
            'voucher_key' => $voucherKey,
            'voucher_lock_period' => $this->config->getVoucherLockPeriod(),
        ];
        if ($order && $order->getIncrementId()) {
            $payload['retailer_basket_code'] = $order->getIncrementId();
        }

        $storeId = $this->storeManager->getWebsite($customer->getWebsiteId())
            ->getDefaultStore()
            ->getId();

        /** @var RequestInterface $request */
        $request = $this->requestFactory->create()
            ->setStoreId($storeId)
            ->setPath('lock-voucher.json')
            ->setPayload($payload)
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setMethod(RequestInterface::METHOD_PUT)
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true]);

        $this->requestQueue->addRequest($request, true);
        $this->connectorRegistry->deleteCard($customer->getId());
    }
}
