<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Customer\Api\Data\AddressInterface as AddressData;
use Magento\Customer\Api\Data\CustomerInterface as CustomerData;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Spaaza\Client\ErrorsException;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;

/**
 * Customer connector
 */
class Customer
{
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterfaceFactory
     */
    protected $extensionAttributeFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Customer\Api\Data\AddressInterfaceFactory
     */
    protected $addressFactory;

    /**
     * @var \Magento\Customer\Api\Data\CustomerExtensionFactory
     */
    protected $customerExtensionFactory;

    /**
     * @var \Magento\Customer\Api\CustomerMetadataInterface
     */
    protected $customerMetadata;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    protected $requestQueue;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    protected $requestFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $cacheGetCard = [];

    /**
     * @var Identifier\CustomerIdProvider
     */
    protected $customerIdProvider;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Registry
     */
    private $connectorRegistry;

    public function __construct(
        \Spaaza\Loyalty\Api\Data\Customer\SpaazaDataInterfaceFactory $extensionAttributeFactory,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        \Spaaza\Loyalty\Helper\Data $helper,
        Registry $connectorRegistry,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Api\Data\AddressInterfaceFactory $addressFactory,
        \Magento\Customer\Api\Data\CustomerExtensionFactory $customerExtensionFactory,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Customer\Api\CustomerMetadataInterface $customerMetadata,
        \Spaaza\Loyalty\Model\Connector\Identifier\CustomerIdProvider $customerIdProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->addressRepository = $addressRepository;
        $this->customerRepository = $customerRepository;
        $this->customerExtensionFactory = $customerExtensionFactory;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->addressFactory = $addressFactory;
        $this->extensionAttributeFactory = $extensionAttributeFactory;
        $this->customerMetadata = $customerMetadata;
        $this->helper = $helper;
        $this->requestQueue = $requestQueue;
        $this->requestFactory = $requestFactory;
        $this->logger = $logger;
        $this->connectorRegistry = $connectorRegistry;
        $this->customerIdProvider = $customerIdProvider;
        $this->storeManager = $storeManager;
    }

    /**
     * Get the Spaaza 'card' of a customer (synchronous)
     *
     * Exceptions will be handled and never be re-thrown.
     *
     * @param CustomerData $customer
     * @param bool $force Force a new request to the server? Normally, will only perform one call per Magento request
     * @return mixed  Returns NULL if an error occurred, FALSE if user not found, Array with details if found
     * @throws ErrorsException
     * @throws LocalizedException
     */
    public function getCard(CustomerData $customer, $force = false)
    {
        return $this->getSpaazaCard($customer, true, $force);
    }

    /**
     * Get card and cache results (synchronous)
     *
     * This gets cached for the lifetime of the (website) request because this call can be made
     * by multiple methods in one request.
     *
     * Exception handling gets done within this function.
     *
     * @param $spaazaUserIdOrEmail
     * @param int|null $customerId The id of the Magento customer to be linked (for reference on the Request object and
     *                             for local caching)
     * @param bool $graceful Ignore exceptions or not? If FALSE, then exceptions will be thrown.
     *                       Except for 'user_not_found'.
     * @param bool $force Force a call instead of looking in the cache first
     * @return mixed  Returns NULL if an error occurred, FALSE if user not found, Array with details if found
     * @throws ErrorsException
     * @throws LocalizedException
     */
    protected function getSpaazaCard(CustomerData $customer, $graceful = true, $force = false)
    {
        $customerId = $customer->getId();
        $result = $this->connectorRegistry->getCard($customerId);
        if ($force || $result === null) {
            $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($customer);
            if ($spaazaUserId = $spaazaData->getUserId()) {
                $this->helper->debugLog('Get Spaaza card for user', ['id' => $spaazaUserId]);
                $identification = [
                    'user_id' => $spaazaUserId
                ];
            } else {
                $this->helper->debugLog('Get Spaaza card for user', ['email' => $customer->getEmail()]);
                $identification = [
                    'username' => $customer->getEmail()
                ];
            }
            try {
                /** @var RequestInterface $request */
                $request = $this->requestFactory->create()
                    ->setStoreId($this->getStoreIdForCustomer($customer))
                    ->setPath('get-card.json')
                    ->setPayload($identification)
                    ->setMethod(RequestInterface::METHOD_GET)
                    ->setOptions(['send_chain_id' => true, 'send_hostname' => true])
                    ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER);

                if ($customerId) {
                    $request->setEntityId($customerId);
                }

                $result = $this->requestQueue->sendSynchronousRequest($request, true);
            } catch (ErrorsException $e) {
                if ($e->hasName('user_not_found')) {
                    // this is not really an error
                    $result = false;
                } else {
                    if (!$graceful) {
                        throw $e;
                    }
                }
                // phpcs:ignore Magento2.Exceptions.ThrowCatch
            } catch (\Exception $e) {
                // do nothing
                if (!$graceful) {
                    throw $e;
                }
            }
        }
        if ($result || $result === false) {
            $this->connectorRegistry->setCard($customerId, $result);
        }
        return $result;
    }

    /**
     * Update a Spaaza user from a Magento customer
     *
     * @param CustomerData $customer
     * @param bool $trySynchronous First try the request synchronous? Otherwise the request will only be queued.
     * @param bool $createIfNotExists  Create the Spaaza user if it does not exist
     * @return void
     */
    public function updateSpaazaUserFromCustomer(
        CustomerData $customer,
        $trySynchronous = true,
        $createIfNotExists = false
    ) {
        try {
            $this->helper->debugLog('Update Spaaza user for Magento customer', ['customer_id' => $customer->getId()]);

            $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($customer);
            if ($spaazaData->getUserId() == 0) {
                $this->helper->debugLog(
                    'Magento customer has no Spaaza user id',
                    ['customer_id' => $customer->getId()]
                );
                $userData = $this->getCard($customer);
                if (!empty($userData['user_info']['id'])) {
                    // The Spaaza user exists: update Spaaza extension attribute
                    $this->helper->debugLog(
                        'Update customer identification',
                        ['customer_id' => $customer->getId(), 'user_id' => $userData['user_info']['id']]
                    );
                    $spaazaData->setUserId($userData['user_info']['id']);
                    if (!empty($data['entity_code']['code'])) {
                        $spaazaData->setMemberNumber($data['entity_code']['code']);
                    }
                } elseif ($userData === false && $createIfNotExists) {
                    // The Spaaza user does not exist: create it
                    $this->helper->debugLog(
                        'Create a Spaaza user for customer',
                        ['customer_id' => $customer->getId()]
                    );
                    $this->createSpaazaUser($customer, $trySynchronous);
                    // invalidate cache
                    $this->connectorRegistry->deleteCard($customer->getId());
                } elseif ($userData === false) {
                    $this->helper->debugLog('Spaaza user does not exist');
                    // Don't do an alter-user request
                    return;
                } else {
                    $this->helper->debugLog('Some error occurred while getting user info from Spaaza');
                    // Don't do an alter-user request
                    return;
                }
            }

            $data = $this->getSpaazaUserInfoForCustomer($customer);
            if ($address = $this->getDefaultBillingAddress($customer, false)) {
                $data = array_merge($data, $this->getSpaazaUserInfoForAddress($address));
            }
            if ($this->updateCustomerHash($customer, $address)) {
                $this->helper->debugLog(
                    'SpaazaData hash has changed » update user',
                    ['customer_id' => $customer->getId()]
                );
                $request = $this->requestFactory->create()
                    ->setStoreId($this->getStoreIdForCustomer($customer))
                    ->setPath('internal/alter-user.json')
                    ->setPayload($data)
                    ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
                    ->setEntityId($customer->getId())
                    ->setMethod(RequestInterface::METHOD_PUT)
                    ->setOptions(['send_hostname' => true])
                    ->setCallback('customerSetSpaazaIdentification');
                $this->requestQueue->addRequest($request, $trySynchronous);
                $this->saveExtensionAttribute($customer);
                $this->connectorRegistry->deleteCard($customer->getId());
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * Get the default store id of the website the customer belongs to
     *
     * Requests need a store id, but customers have a website scope.
     *
     * @param CustomerData $customer
     * @return int
     * @throws LocalizedException
     */
    protected function getStoreIdForCustomer(CustomerData $customer)
    {
        $websiteId = $customer->getWebsiteId();
        return $this->storeManager->getWebsite($websiteId)
            ->getDefaultStore()
            ->getId();
    }

    /**
     * Update a Magento customer from Spaaza; save it if changed
     *
     * @param CustomerData $customer
     * @param bool $createIfNotExists Create a Spaaza user if it does not exist?
     * @return void
     * @throws ErrorsException
     * @throws LocalizedException
     */
    public function updateCustomerFromSpaaza(CustomerData $customer, $createIfNotExists = false)
    {
        try {
            $spaazaData = $this->getCard($customer);
            if ($spaazaData === false && $createIfNotExists) {
                // User does not exist
                $this->createSpaazaUser($customer);
            } elseif (!empty($spaazaData['user_info'])) {
                $this->updateCustomerFromSpaazaUserInfo($customer, $spaazaData['user_info']);
            }
        } catch (ErrorsException $e) {
            if ($e->hasName('user_not_found') && $createIfNotExists) {
                $this->createSpaazaUser($customer);
            }
        }
    }

    /**
     * Update a Magento customer from Spaaza user info; save it if changed
     *
     * @param CustomerData $customer
     * @param array $userInfo  User info, as it gets provided by Spaaza API
     * @return CustomerData  The updated customer data object
     * @throws InputException
     * @throws LocalizedException
     */
    public function updateCustomerFromSpaazaUserInfo(CustomerData $customer, array $userInfo)
    {
        $result = $customer;
        if (!empty($userInfo)) {
            $this->updateCustomerDataFromSpaazaUserInfo($customer, $userInfo);
            if ($address = $this->getDefaultBillingAddress($customer, true)) {
                $this->updateAddressDataFromSpaazaUserInfo($address, $userInfo);
            }
            if ($this->updateCustomerHash($customer, $address)) {
                // hash has changed: save the customer and address BUT DO NOT SYNC AFTERWARDS
                $result = $this->saveCustomerWithoutSyncing($customer);
                try {
                    // hash has changed: save the customer and address BUT DO NOT SYNC AFTERWARDS
                    $this->saveAddressWithoutSyncing($address, $result);
                } catch (InputException $e) {
                    // do nothing; apparently the address did not validate
                    $this->helper->debugLog(
                        'New address data from Spaaza did not validate',
                        ['address_id' => $address->getId(), 'customer_id' => $customer->getId()]
                    );
                }
            }
        }
        return $result;
    }

    /**
     * Create a Spaaza user from a Magento customer (asynchronous if needed)
     *
     * If the action succeeds, the id of the newly created Spaaza user will be set on the customer
     * and the Spaaza extension attribute will be updated.
     *
     * @param CustomerData $customer
     * @param bool $trySynchronous  First try a synchronous call?
     * @return void
     * @throws LocalizedException
     */
    public function createSpaazaUser(CustomerData $customer, $trySynchronous = true)
    {
        $this->helper->debugLog('Create Spaaza user', ['customer_id' => $customer->getId()]);
        $userInfo = $this->getSpaazaUserInfoForCustomer($customer, true);

        $request = $this->requestFactory->create()
            ->setStoreId($this->getStoreIdForCustomer($customer))
            ->setPath('internal/add-user.json')
            ->setPayload($userInfo)
            ->setEntityType(EntityType::ENTITY_TYPE_CUSTOMER)
            ->setEntityId($customer->getId())
            ->setMethod(RequestInterface::METHOD_POST)
            ->setOptions(['send_chain_id' => true, 'send_hostname' => true])
            ->setCallback('customerSetSpaazaIdentification');

        $address = $this->getDefaultBillingAddress($customer, false);
        $this->updateCustomerHash($customer, $address);

        $this->requestQueue->addRequest($request, $trySynchronous);
        $this->connectorRegistry->deleteCard($customer->getId());
    }

    /**
     * Get Spaaza representation of a customer (ready to be sent to Spaaza)
     *
     * The output needs to be typed because it gets used to create a hash.
     *
     * @param CustomerData $customer
     * @param bool $includeAddressData
     * @return array
     * @throws LocalizedException
     */
    public function getSpaazaUserInfoForCustomer(CustomerData $customer, $includeAddressData = false): array
    {
        // It could be that the Spaaza extension attribute was not present in the customer object while saving
        $extensionAttribute = $this->spaazaDataManagement->applyExtensionAttributes($customer);

        $userInfo = [
            'first_name' => (string)$customer->getFirstname(),
            'last_name' => (string)$customer->getLastname(),
            'birthday' => (string)$customer->getDob() > 0 ? substr($customer->getDob(), 0, 10) : '',
            'username' => (string)$customer->getEmail(),
            'webshop_customer_id' => $this->customerIdProvider->getAuthenticationPointIdentifier($customer),
        ];

        $gender = $this->getSpaazaGender($customer->getGender());
        if ($gender !== null) {
            $userInfo['gender'] = $gender;
        }

        if ($extensionAttribute) {
            if ($extensionAttribute->getUserId()) {
                $userInfo['user_id'] = (int)$extensionAttribute->getUserId();
            }
            $userInfo['printed_mailing_list_subscribed'] = (bool)$extensionAttribute->getPrintedMailingListSubscribed();
            $userInfo['mailing_list_subscribed'] = (bool)$extensionAttribute->getMailingListSubscribed();
            $userInfo['mailing_list_sub_offered'] = (bool)true;
            $userInfo['programme_opted_in'] = (bool)$extensionAttribute->getProgrammeOptedIn();
            $userInfo['referral_code'] = $extensionAttribute->getSignupReferralCode();
        }

        if ($includeAddressData) {
            $address = $this->getDefaultBillingAddress($customer, false);
            if ($address) {
                $userInfo = array_merge($userInfo, $this->getSpaazaUserInfoForAddress($address));
            }
        }

        return $userInfo;
    }

    /**
     * Get the Spaaza representation of a Magento gender id (output of customer->getGender())
     *
     * @param int $magentoCustomerValue
     * @return string|null  M for male, F for email, empty string for 'not specified', NULL if we cannot determine
     */
    public function getSpaazaGender($magentoCustomerValue): ?string
    {
        try {
            if (!$magentoCustomerValue) {
                return '';
            }

            $genderAttribute = $this->customerMetadata->getAttributeMetadata('gender');
            if (!$genderAttribute) {
                return null;
            }

            $options = $genderAttribute->getOptions();
            if (!count($options)) {
                return null;
            }

            $label = null;
            foreach ($options as $option) {
                if ($option->getValue() == $magentoCustomerValue) {
                    $label = $option->getLabel();
                    break;
                }
            }

            if ($label) {
                // first, try to select on first letter of the label
                $firstLetter = strtoupper(substr($label, 0, 1));
                if ($firstLetter == 'M') {
                    return 'M';
                } elseif (in_array($firstLetter, ['F', 'V'])) {
                    return 'F';
                } elseif ($firstLetter == 'N') {
                    // Not specified
                    return '';
                }
            }

            // then try to select by default Magento ids
            if ($magentoCustomerValue == 1) {
                return 'M';
            } elseif ($magentoCustomerValue == 2) {
                return 'F';
            } elseif ($magentoCustomerValue == 3) {
                // Not specified
                return '';
            }
        } catch (\Exception $e) {
            return null;
        }
        // we don't know, but feel free to create a plugin
        return null;
    }

    /**
     * Get the Magento gender id that represents a certain Spaaza gender value
     *
     * @param string $spaazaGenderValue
     * @return string|null  The Magento gender attribute value,
     */
    public function getMagentoGender($spaazaGenderValue)
    {
        if ($spaazaGenderValue == null) {
            return null;
        }

        try {
            $genderAttribute = $this->customerMetadata->getAttributeMetadata('gender');
            if (!$genderAttribute) {
                return null;
            }

            $options = $genderAttribute->getOptions();
            if (!count($options)) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }

        switch (strtoupper($spaazaGenderValue)) {
            case 'M':
                $firstLetter = ['M'];
                $id = 1;
                break;
            case 'F':
                $firstLetter = ['F', 'V'];
                $id = 2;
                break;
            case '':
                $firstLetter = ['N'];
                $id = 3;
                break;
            default:
                return null;
        }

        // first try based on first letter
        foreach ($options as $option) {
            if (in_array(strtoupper(substr($option->getLabel(), 0, 1)), $firstLetter)) {
                return $option->getValue();
            }
        }
        // then try based on id
        foreach ($options as $option) {
            if ($option->getValue() == $id) {
                return $option->getValue();
            }
        }

        // we don't know, but feel free to create a plugin
        return null;
    }

    /**
     * Get Spaaza representation of an address, to be merged with the customer data
     *
     * @param AddressData $address
     * @return array
     */
    public function getSpaazaUserInfoForAddress(AddressData $address)
    {
        $streetLines = array_values($address->getStreet());
        if (!$streetLines) {
            $streetLines = [];
        }
        $streetLines = array_pad($streetLines, 3, '');

        return [
            'address_streetname' => $streetLines[0],
            'address_housenumber' => $streetLines[1],
            'address_housenumber_extension' => $streetLines[2],
            'address_postalcode' => $address->getPostcode(),
            'address_towncity' => $address->getCity(),
            'country_code' => $address->getCountryId(),
            'phone_number' => $address->getTelephone(),
        ];
    }

    /**
     * Get the default billing address for a customer
     *
     * @param CustomerData $customer
     * @param bool $createIfNotExists  Create the address if it doesn't exist?
     * @return AddressData|null
     * @throws LocalizedException
     */
    public function getDefaultBillingAddress(CustomerData $customer, $createIfNotExists = true)
    {
        $billingAddress = null;
        $billingAddressId = $customer->getDefaultBilling();
        if ($billingAddressId) {
            try {
                $billingAddress = $this->addressRepository->getById($billingAddressId);
            } catch (NoSuchEntityException $e) {
                $billingAddressId = 0;
            }
        }
        if (!$billingAddressId && $createIfNotExists) {
            $billingAddress = $this->addressFactory->create();
            $billingAddress->setCustomerId($customer->getId())
                ->setIsDefaultBilling(true);
            if (!$customer->getDefaultShipping()) {
                $billingAddress->setIsDefaultShipping(true);
            }
        }
        return $billingAddress;
    }

    /**
     * Update customer data from Spaaza data ('user_info' fetched from the API)
     *
     * @param CustomerData $customer
     * @param array $data
     * @return void
     */
    protected function updateCustomerDataFromSpaazaUserInfo(CustomerData $customer, array $data = [])
    {
        if (!empty($data['first_name'])) {
            $customer->setFirstname($data['first_name']);
        }
        if (!empty($data['last_name'])) {
            $customer->setLastname($data['last_name']);
        }
        if (isset($data['birthday'])) {
            $customer->setDob($data['birthday']);
        }

        if (isset($data['gender'])) {
            $gender = $this->getMagentoGender($data['gender']);
            if ($gender !== null) {
                $customer->setGender($gender);
            }
        }
        if (isset($data['username']) && strpos($data['username'], '@') !== false) {
            $customer->setEmail($data['username']);
        }

        $this->spaazaDataManagement->applyExtensionAttributes($customer);
        $extensionData = $customer->getExtensionAttributes()->getSpaazaData();
        if (!empty($data['id'])) {
            $extensionData->setUserId($data['id']);
        }
        if (!empty($data['entity_code']['code'])) {
            $extensionData->setMemberNumber($data['entity_code']['code']);
        }
        if (isset($data['mailing_list']['mailing_list_subscribed'])) {
            $extensionData->setMailingListSubscribed($data['mailing_list']['mailing_list_subscribed']);
        }
        if (isset($data['mailing_list']['printed_mailing_list_subscribed'])) {
            $extensionData->setPrintedMailingListSubscribed($data['mailing_list']['printed_mailing_list_subscribed']);
        }
        if (isset($data['opt_in_programme']['programme_opted_in'])) {
            $extensionData->setProgrammeOptedIn($data['opt_in_programme']['programme_opted_in']);
        }
        if (isset($data['referral_code'])) {
            $extensionData->setReferralCode($data['referral_code']);
        }
    }

    /**
     * Update address data from Spaaza data ('user_info' fetched from the API)
     *
     * @param AddressData $address
     * @param array $data
     */
    protected function updateAddressDataFromSpaazaUserInfo(AddressData $address, array $data = [])
    {
        if (!empty($data['first_name'])) {
            $address->setFirstname($data['first_name']);
        }
        if (!empty($data['last_name'])) {
            $address->setLastname($data['last_name']);
        }
        $street = $address->getStreet();
        if (!$street) {
            $street = [];
        }
        $street = array_pad($street, 3, '');
        if (isset($data['address_streetname'])) {
            $street = ['', '', ''];
            $street[0] = $data['address_streetname'];
            if (isset($data['address_housenumber'])) {
                $street[1] = $data['address_housenumber'];
            }
            if (isset($data['address_housenumber_extension'])) {
                $street[2] = $data['address_housenumber_extension'];
            }
            if (!$street[2]) {
                // trim array items
                unset($street[2]);
                if (!$street[1]) {
                    unset($street[1]);
                }
            }
        }
        $address->setStreet($street);

        $address->setPostcode($data['address_postalcode'] ?? null);
        $address->setCity($data['address_towncity'] ?? null);
        $address->setCountryId($data['country_code'] ?? null);
        $address->setTelephone($data['phone_number'] ?? null);
    }

    /**
     * Get the data of a customer that is to be used for creating the customer data hash
     *
     * @param CustomerData $customer
     * @return array
     */
    public function getCustomerHashData(CustomerData $customer)
    {
        $data = $this->getSpaazaUserInfoForCustomer($customer);
        // unset the identification fields; they're not part of the hash data
        unset($data['member_number']);
        unset($data['user_id']);
        return $data;
    }

    /**
     * Get the data of an address that is to be used for creating the customer data hash
     * @param AddressData $address
     * @return array
     */
    public function getAddressHashData(AddressData $address)
    {
        return $this->getSpaazaUserInfoForAddress($address);
    }

    /**
     * Get the hash of a data array
     *
     * At this moment, json_encode() is used, but it could be replaced in the future
     * with another serialization type which produces the same serialized string for
     * different data types.
     *
     * @param array $data
     * @return bool|string
     */
    protected function getDataHash(array $data)
    {
        return substr(sha1(json_encode($data)), 0, 16);
    }

    /**
     * Update the hash in the customer extension attribute
     *
     * @param CustomerData $customer
     * @param AddressData|null $address
     * @return bool TRUE if the hash has changed, FALSE if not
     */
    public function updateCustomerHash(CustomerData $customer, AddressData $address = null)
    {
        $hashData = [
            'customer' => $this->getCustomerHashData($customer),
        ];
        if ($address) {
            $hashData['address'] = $this->getAddressHashData($address);
        }

        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($customer);
        if ($spaazaData) {
            $hashData = array_merge($hashData, [
                'member_number' => $spaazaData->getMemberNumber(),
                'programme_opted_in' => $spaazaData->getProgrammeOptedIn(),
                'mailing_list_subscribed' => $spaazaData->getMailingListSubscribed(),
                'printed_mailing_list_subscribed' => $spaazaData->getPrintedMailingListSubscribed(),
            ]);
        }

        $this->helper->debugLog('Customer data for hash', $hashData['customer']);
        $this->helper->debugLog('Address data for hash', $hashData['address'] ?? []);

        $newHash = $this->getDataHash($hashData);
        $this->spaazaDataManagement->applyExtensionAttributes($customer);

        $extensionAttribute = $customer->getExtensionAttributes()->getSpaazaData();
        $oldHash = $extensionAttribute->getLastHash();
        if ($oldHash != $newHash) {
            $this->helper->debugLog(
                'Spaaza hash has changed',
                [
                    'old_hash' => $oldHash,
                    'new_hash' => $newHash,
                    'customer' => $customer->getId(),
                    'address' => $address ? $address->getId() : null
                ]
            );
            $extensionAttribute->setLastHash($newHash);
            $customer->getExtensionAttributes()->setSpaazaData($extensionAttribute);
            return true;
        }
        return false;
    }

    /**
     * Save the Spaaza customer extension attribute (NOT the customer)
     *
     * @param CustomerData $customer
     * @throws LocalizedException
     */
    public function saveExtensionAttribute(CustomerData $customer)
    {
        if ($customer->getExtensionAttributes()
            && $spaazaData = $customer->getExtensionAttributes()->getSpaazaData()
        ) {
            $spaazaData->setCustomerId($customer->getId());
            $this->spaazaDataManagement->save($spaazaData);
        }
    }

    /**
     * Save a customer without syncing data to Spaaza
     *
     * @param CustomerData $customer
     * @param string|null $passwordHash  A new password hash, if any
     * @return CustomerData
     * @throws InputException
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\State\InputMismatchException
     */
    public function saveCustomerWithoutSyncing(CustomerData $customer, ?string $passwordHash = null)
    {
        $this->spaazaDataManagement->setDoNotSyncOnSave($customer);
        $result = $this->customerRepository->save($customer, $passwordHash);
        $this->spaazaDataManagement->resetDoNotSyncOnSave($customer);
        return $result;
    }

    /**
     * Save an address
     *
     * @param AddressData $address
     * @param CustomerData $customer  Customer data object, to set the 'do not sync' flag
     * @return void
     * @throws LocalizedException
     */
    public function saveAddressWithoutSyncing(AddressData $address, CustomerData $customer)
    {
        $this->spaazaDataManagement->setDoNotSyncOnSave($customer);
        if (!$address->getCustomerId()) {
            $address->setCustomerId($customer->getId());
        }
        $this->addressRepository->save($address);
        $this->spaazaDataManagement->resetDoNotSyncOnSave($customer);
    }
}
