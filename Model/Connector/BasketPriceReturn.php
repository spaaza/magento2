<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;

/**
 * UNUSED at this moment
 *
 * @internal
 */
class BasketPriceReturn
{
    const SPAAZA_ITEM_TYPE_PRODUCT = 'product';

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    protected $requestFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    protected $requestQueue;

    /**
     * @var Identifier\LockingCodeProvider
     */
    protected $lockingCodeProvider;

    /**
     * @var Identifier\UserIdentityProvider
     */
    protected $userIdentityProvider;

    /**
     * @var BasketItemIdentifierProviderInterface
     */
    protected $basketItemIdentifierProvider;

    /**
     * @var Identifier\BasketCodeProvider
     */
    protected $basketCodeProvider;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Identifier\RetailerIdentityProvider
     */
    protected $retailerIdentityProvider;

    public function __construct(
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        Identifier\LockingCodeProvider $lockingCodeProvider,
        Identifier\UserIdentityProvider $userIdentityProvider,
        Identifier\RetailerIdentityProvider $retailerIdentityProvider,
        BasketItemIdentifierProviderInterface $basketItemIdentifierProvider,
        \Spaaza\Loyalty\Model\Connector\Identifier\BasketCodeProvider $basketCodeProvider,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->config = $config;
        $this->requestFactory = $requestFactory;
        $this->requestQueue = $requestQueue;
        $this->lockingCodeProvider = $lockingCodeProvider;
        $this->userIdentityProvider = $userIdentityProvider;
        $this->basketItemIdentifierProvider = $basketItemIdentifierProvider;
        $this->basketCodeProvider = $basketCodeProvider;
        $this->orderRepository = $orderRepository;
        $this->retailerIdentityProvider = $retailerIdentityProvider;
    }

    public function getBasketPriceForCreditmemo(CreditmemoInterface $creditmemo)
    {
        $request = $this->createBasketPriceRequest($creditmemo);
        if (!$request) {
            return null;
        }
        return $this->requestQueue->sendSynchronousRequest($request);
    }

    public function createBasketPriceRequest(CreditmemoInterface $creditmemo): ?RequestInterface
    {
        $userIdentity = $this->userIdentityProvider->getUserIdentityForCreditMemo($creditmemo);

        // Compose the payload
        $payload = [
            'entity' => $this->retailerIdentityProvider->getRetailerIdentity($creditmemo->getStoreId()),
            'user' => $userIdentity,
            'basket' => $this->getBasketData($creditmemo),
        ];


        if ($payload) {
            try {
                return $this->requestFactory->create()
                    ->setStoreId($creditmemo->getStoreId())
                    ->setPath('auth/get-basket-price.json')
                    ->setPayload($payload)
                    ->setEntityType(\Spaaza\Loyalty\Model\Config\Source\EntityType::ENTITY_TYPE_CREDITMEMO)
                    ->setEntityId($creditmemo->getEntityId())
                    ->setMethod(RequestInterface::METHOD_POST_JSON);
            } catch (\Exception $e){
                return null;
            }
        }
        return null;
    }

    /**
     * Get all relevant JSON data for a basket based on a creditmemo
     *
     * @api
     * @param CreditmemoInterface $creditmemo
     * @return array
     */
    public function getBasketData(CreditmemoInterface $creditmemo): array
    {
        $basketItems = [];
        $order = $this->getOrderForCreditMemo($creditmemo);

        foreach ($creditmemo->getItems() as $creditmemoItem) {
            $basketItemData = $this->getBasketItemData($creditmemoItem);
            if ($basketItemData) {
                $basketItemData['original_retailer_basket_code'] =
                    $this->basketCodeProvider->getBasketCodeForOrder($order);
                $basketItems[] = $basketItemData;
            }
        }

        return [
            // 'in_store' or 'online'
            'app_platform_type' => 'online',
            // any ID in the retailer's system used to identify a particular user basket.
            'retailer_basket_code' => $this->basketCodeProvider->getBasketCodeForCreditmemo($creditmemo),
            'basket_total_price' => 0,
            'shipping_charge' => 0, // @todo (float)$quoteAddress->getBaseShippingInclTax(),
            // @todo 'voucher_locking_code' => $this->lockingCodeProvider->getLockingCodeForCart($quote->getId()),
            // 'basket_vouchers_lock_exclusively' => true,
            'basket_currency' => [
                'currency_code' => $creditmemo->getBaseCurrencyCode() // 3 letter currency code
            ],
            'basket_tax' => [], //@todo $this->getBasketTaxDataForQuote($quote),
            'basket_items' => $basketItems,
        ];
    }

    /**
     * Get an order by its id, or return null
     *
     * @param CreditmemoInterface $creditmemo
     * @return OrderInterface|null
     */
    protected function getOrderForCreditMemo(CreditmemoInterface $creditmemo): ?OrderInterface
    {
        $orderId = $creditmemo->getOrderId();
        $order = null;
        try {
            if ($orderId > 0) {
                $order = $this->orderRepository->get($orderId);
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $order = null;
        }
        return $order;
    }

    /**
     * Get all relevant JSON data for a (return) basket item
     *
     * @api
     * @param CreditmemoItemInterface $creditmemoItem
     * @return array|null
     */
    public function getBasketItemData(CreditmemoItemInterface $creditmemoItem): ?array
    {
        if ($creditmemoItem->getOrderItem()->getParentItemId()) {
            return null;
        }
        $price = $creditmemoItem->getBasePriceInclTax();
        $qty = (float)$creditmemoItem->getQty();
        $discount = $creditmemoItem->getBaseDiscountAmount() / $qty;

        $salePrice = $price - $discount;
        $basketItem = [
            'item_quantity' => (float)$qty,
            'item_price' => -1 * (float)$salePrice,
            'item_subtotal' => -1 * ($creditmemoItem->getBaseRowTotalInclTax() - $creditmemoItem->getBaseDiscountAmount()),
            // The credit memo item has no id yet because it's not saved yet
            'retailer_item_code' => 'C' . (string)$creditmemoItem->getOrderItemId(),
            'retailer_product_code' => $creditmemoItem->getSku(),
            'item_name' => $creditmemoItem->getName(),
            'item_type' => self::SPAAZA_ITEM_TYPE_PRODUCT,
            'return_item' => true,
        ];
        $basketItem = array_replace(
            $basketItem,
            $this->basketItemIdentifierProvider->getIdentifierForCreditmemoItem($creditmemoItem)
        );
        return $basketItem;
    }
}
