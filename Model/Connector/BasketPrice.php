<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CartItemInterface;
use Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Helper\Data as HelperData;

class BasketPrice
{
    const SPAAZA_ITEM_TYPE_PRODUCT = 'product';

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    protected $quoteAddressSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    protected $requestFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    protected $requestQueue;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helperData;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Identifier\LockingCodeProvider
     */
    protected $lockingCodeProvider;

    /**
     * @var BasketItemIdentifierProviderInterface
     */
    protected $basketItemIdentifierProvider;

    /**
     * @var Identifier\UserIdentityProvider
     */
    protected $userIdentityProvider;

    /**
     * @var Identifier\RetailerIdentityProvider
     */
    protected $retailerIdentityProvider;

    public function __construct(
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $quoteAddressSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        HelperData $helperData,
        BasketItemIdentifierProviderInterface $basketItemIdentifierProvider,
        Identifier\LockingCodeProvider $lockingCodeProvider,
        Identifier\UserIdentityProvider $userIdentityProvider,
        Identifier\RetailerIdentityProvider $retailerIdentityProvider
    ) {
        $this->quoteAddressSpaazaDataManagement = $quoteAddressSpaazaDataManagement;
        $this->config = $config;
        $this->requestFactory = $requestFactory;
        $this->requestQueue = $requestQueue;
        $this->helperData = $helperData;
        $this->lockingCodeProvider = $lockingCodeProvider;
        $this->basketItemIdentifierProvider = $basketItemIdentifierProvider;
        $this->userIdentityProvider = $userIdentityProvider;
        $this->retailerIdentityProvider = $retailerIdentityProvider;
    }

    /**
     * Perform a 'get-basket-price' call to Spaaza
     *
     * @param CartInterface $quote
     * @return array|mixed|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getBasketPrice(CartInterface $quote)
    {
        $request = $this->createBasketPriceRequest($quote);
        if (!$request) {
            return null;
        }
        return $this->requestQueue->sendSynchronousRequest($request);
    }

    /**
     * Create a request object, ready to be sent to Spaaza
     *
     * @param CartInterface $quote
     * @return RequestInterface|null
     */
    public function createBasketPriceRequest(CartInterface $quote): ?RequestInterface
    {
        $quoteAddress = $quote->getIsVirtual() ? $quote->getBillingAddress() : $quote->getShippingAddress();
        $userIdentity = $this->userIdentityProvider->getUserIdentityForCartAddress($quoteAddress);
        if (!$userIdentity && !$this->config->isAnonymousGetBasketPriceEnabled()) {
            // user cannot be identified: skip this order
            return null;
        }

        // Compose the payload
        $payload = [
            'entity' => $this->retailerIdentityProvider->getRetailerIdentity($quote->getStoreId()),
            'user' => $userIdentity,
            'basket' => $this->getBasketData($quote),
        ];


        if ($payload) {
            try {
                return $this->requestFactory->create()
                    ->setStoreId($quote->getStoreId())
                    ->setPath('auth/get-basket-price.json')
                    ->setPayload($payload)
                    ->setEntityType(\Spaaza\Loyalty\Model\Config\Source\EntityType::ENTITY_TYPE_QUOTE)
                    ->setEntityId($quote->getId())
                    ->setMethod(RequestInterface::METHOD_POST_JSON);
            } catch (\Exception $e){
                return null;
            }
        }
        return null;
    }

    /**
     * Get all relevant JSON data for a basket
     *
     * @api
     * @param CartInterface $quote
     * @return array
     */
    public function getBasketData(CartInterface $quote): array
    {
        $quoteAddress = $quote->getIsVirtual() ? $quote->getBillingAddress() : $quote->getShippingAddress();

        $basketItems = [];
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $basketItems[] = $this->getBasketItemData($quoteItem);
        }

        return [
            // 'in_store' or 'online'
            'app_platform_type' => 'online',
            // any ID in the retailer's system used to identify a particular user basket.
            'retailer_basket_code' => 'Q' . $quote->getId(),
            'basket_total_price' => (float)$quoteAddress->getSubtotalInclTax(),
            'shipping_charge' => (float)$quoteAddress->getBaseShippingInclTax(),
            'voucher_locking_code' => $this->lockingCodeProvider->getLockingCodeForCart($quote->getId()),
            'basket_vouchers_lock_exclusively' => true,
            'basket_currency' => [
                'currency_code' => $quote->getBaseCurrencyCode() // 3 letter currency code
            ],
            'basket_tax' => $this->getBasketTaxDataForQuote($quote),
            'basket_items' => $basketItems,
        ];
    }

    /**
     * Get the tax details for a quote
     *
     * Returns an array like this:
     * [
     *   ['tax_rate' => 21.0, 'tax_total' => 1.29],
     *   ['tax_rate' => 9.0, 'tax_total' => 0.27]
     * ]
     *
     * @param CartInterface $quote
     * @return array
     */
    protected function getBasketTaxDataForQuote(CartInterface $quote): array
    {
        $taxRates = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            $percent = $item->getTaxPercent();

            $key = sprintf('K%5d', $percent * 10000);
            if (!isset($taxRates[$key])) {
                $taxRates[$key] = [
                    'tax_rate' => round($percent / 100, 2),
                    'tax_total' => 0,
                ];
            }
            $taxRates[$key]['tax_total'] += $item->getBaseTaxAmount();
        }
        return array_values($taxRates);
    }

    /**
     * Get all relevant JSON data for a basket item
     *
     * @api
     * @param \Magento\Quote\Model\Quote\Item $quoteItem
     * @return array
     */
    public function getBasketItemData(CartItemInterface $quoteItem): array
    {
        $price = $quoteItem->getBasePriceInclTax();
        $qty = (float)$quoteItem->getQty();
        $sku = $quoteItem->getSku();
        $isPromotional = $this->helperData->checkIfProductSkuIsSetToPromotional($sku);
        $discount = $quoteItem->getBaseDiscountAmount() / $qty;

        $children = $quoteItem->getChildren();
        $simpleItem = null;
        if (count($children)) {
            $simpleItem = reset($children);
        }
        if (!$simpleItem) {
            $simpleItem = $quoteItem;
        }

        $salePrice = $price - $discount;
        if($isPromotional) {
            $basketItem = [
                'item_quantity' => (float)$qty,
                'item_price' => (float)$salePrice,
                'item_subtotal' => (float)$quoteItem->getBaseRowTotalInclTax() - $quoteItem->getBaseDiscountAmount(),
                'retailer_item_code' => (string)$quoteItem->getId(),
                'retailer_product_code' => $sku,
                'item_name' => $quoteItem->getName(),
                'item_type' => self::SPAAZA_ITEM_TYPE_PRODUCT,
                'item_is_promotional' => true
            ];
        } else {
            $basketItem = [
                'item_quantity' => (float)$qty,
                'item_price' => (float)$salePrice,
                'item_subtotal' => (float)$quoteItem->getBaseRowTotalInclTax() - $quoteItem->getBaseDiscountAmount(),
                'retailer_item_code' => (string)$quoteItem->getId(),
                'retailer_product_code' => $sku,
                'item_name' => $quoteItem->getName(),
                'item_type' => self::SPAAZA_ITEM_TYPE_PRODUCT,
            ];
        }
        $basketItem = array_replace(
            $basketItem,
            $this->basketItemIdentifierProvider->getIdentifierForCartItem($simpleItem)
        );
        return $basketItem;
    }
}
