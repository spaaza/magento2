<?php

namespace Spaaza\Loyalty\Model\Connector\Identifier;

/**
 * Provide the retailer identity used in basket calls
 */
class RetailerIdentityProvider
{
    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $moduleConfig;

    public function __construct(
        \Spaaza\Loyalty\Model\Config $moduleConfig
    ) {
        $this->moduleConfig = $moduleConfig;
    }

    /**
     * Generate the retailer identity part of a basket call
     *
     * Typically used as 'entity' key on the top level of a request body.
     *
     * @see https://docs.spaaza.com/api/add-basket/#identifying-the-retailer
     * @param int $storeId
     * @return array
     */
    public function getRetailerIdentity(int $storeId): array
    {
        return [
            // 'chain' or 'business'
            'entity_type' => 'chain',
            // Spaaza ID of the retailer entity
            'entity_id' => $this->moduleConfig->getChainIdForStore($storeId),
            // A retailer-specific branch code used by the retailer, defaults to 'WEBSHOP'
            'branch_business_owner_code' => $this->getBranchBusinessOwnerCode($storeId),
        ];
    }

    /**
     * Get the config value, or default if config value is empty
     *
     * @param int $storeId
     * @return string
     */
    protected function getBranchBusinessOwnerCode(int $storeId): string
    {
        $configValue = $this->moduleConfig->getBranchBusinessOwnerCodeForStore($storeId);
        if (!$configValue || !trim($configValue)) {
            $configValue = 'WEBSHOP';
        }
        return $configValue;
    }
}
