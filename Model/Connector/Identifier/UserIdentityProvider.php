<?php

namespace Spaaza\Loyalty\Model\Connector\Identifier;

use Magento\Quote\Api\Data\AddressInterface as QuoteAddressInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\CreditmemoInterface;

class UserIdentityProvider
{
    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    protected $customerSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    protected $quoteAddressSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    public function __construct(
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $customerSpaazaDataManagement,
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $quoteAddressSpaazaDataManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->customerSpaazaDataManagement = $customerSpaazaDataManagement;
        $this->quoteAddressSpaazaDataManagement = $quoteAddressSpaazaDataManagement;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Get the data to identify a user for a creditmemo
     *
     * @param CreditmemoInterface $creditmemo
     * @return array
     */
    public function getUserIdentityForCreditMemo(CreditmemoInterface $creditmemo): array
    {
        $orderId = $creditmemo->getOrderId();
        $order = $this->orderRepository->get($orderId);
        return $this->getUserIdentityForOrder($order);
    }

    /**
     * Get the user identity part for use in an add-basket call
     *
     * @param OrderInterface $order
     * @return array
     */
    public function getUserIdentityForOrder(OrderInterface $order): array
    {
        $payload = [];
        $spaazaUserId = null;
        $spaazaMemberNumber = null;
        $customer = null;

        $spaazaData = $order->getExtensionAttributes()->getSpaazaData();
        if ($spaazaData->getUserId()) {
            $spaazaUserId = $spaazaData->getUserId();
        } elseif ($spaazaData->getMemberNumber()) {
            $spaazaMemberNumber = $spaazaData->getMemberNumber();
        } elseif ($order->getCustomerId()) {
            $customerSpaazaData = $this->customerSpaazaDataManagement->getByCustomerId($order->getCustomerId());
            $spaazaUserId = $customerSpaazaData->getUserId();
        }

        if (!$spaazaUserId && !$spaazaMemberNumber) {
            // We have no Spaaza user to assign this order to
            return $payload;
        }

        if ($spaazaUserId) {
            $payload = [
                'member_programme' => 'spaaza',
                'spaaza_user_id' => (int)$spaazaUserId
            ];
        } elseif ($spaazaMemberNumber) {
            $payload = [
                'member_programme' => 'spaaza',
                'member_number' => $spaazaMemberNumber
            ];
        }

        return $payload;
    }

    /**
     * Get the data to identify a user for a quote address
     *
     * @param QuoteAddressInterface $quoteAddress
     * @return array
     */
    public function getUserIdentityForCartAddress(QuoteAddressInterface $quoteAddress): array
    {
        $payload = [];
        $spaazaUserId = null;
        $spaazaMemberNumber = null;
        $customer = null;

        $spaazaData = $this->quoteAddressSpaazaDataManagement->applyExtensionAttributes($quoteAddress);
        if ($spaazaData->getUserId()) {
            $spaazaUserId = $spaazaData->getUserId();
        } elseif ($spaazaData->getMemberNumber()) {
            $spaazaMemberNumber = $spaazaData->getMemberNumber();
        }

        if (!$spaazaUserId && !$spaazaMemberNumber) {
            // We have no Spaaza user to assign this quote address to
            return $payload;
        }

        if ($spaazaUserId) {
            $payload = [
                'member_programme' => 'spaaza',
                'spaaza_user_id' => (int)$spaazaUserId
            ];
        } elseif ($spaazaMemberNumber) {
            $payload = [
                'member_programme' => 'spaaza',
                'member_number' => $spaazaMemberNumber
            ];
        }
        return $payload;
    }
}
