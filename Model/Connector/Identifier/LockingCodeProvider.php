<?php

namespace Spaaza\Loyalty\Model\Connector\Identifier;

class LockingCodeProvider
{
    /**
     * Returns the locking code for a cart that will be used in Spaaza requests
     *
     * @param int $cartId
     * @return string
     */
    public function getLockingCodeForCart(int $cartId): string
    {
        return 'MQ' . $cartId;
    }
}
