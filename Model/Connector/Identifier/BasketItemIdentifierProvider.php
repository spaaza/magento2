<?php

namespace Spaaza\Loyalty\Model\Connector\Identifier;

use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\CreditmemoItemInterface;

class BasketItemIdentifierProvider implements \Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface
{
    /**
     * Get the identification of a product to send to Spaaza
     *
     * For now, sku is the only attribute that can be used. Of course, feel free to create
     * a plugin for this method!
     *
     * See the documentation of Spaaza:
     * https://docs.spaaza.com/#adding-a-completed-basket
     *
     * @api
     * @param OrderItemInterface $orderItem
     * @return array
     */
    public function getIdentifierForOrderItem(OrderItemInterface $orderItem): array
    {
        return ['item_barcode' => $orderItem->getSku()];
    }

    /**
     * Get the identification of a product to send to Spaaza
     *
     * @api
     * @see getIdentifierForOrderItem()
     * @param CreditmemoItemInterface $creditmemoItem
     * @return array
     */
    public function getIdentifierForCreditmemoItem(CreditmemoItemInterface $creditmemoItem): array
    {
        return ['item_barcode' => $creditmemoItem->getSku()];
    }

    /**
     * Get the identification for a quote item
     *
     * @api
     * @see getIdentifierForOrderItem()
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem
     * @return array
     */
    public function getIdentifierForCartItem(\Magento\Quote\Api\Data\CartItemInterface $cartItem): array
    {
        return ['item_barcode' => $cartItem->getSku()];
    }
}
