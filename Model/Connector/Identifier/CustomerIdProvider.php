<?php

namespace Spaaza\Loyalty\Model\Connector\Identifier;

class CustomerIdProvider
{
    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    public function __construct(
        \Spaaza\Loyalty\Model\Config $config
    ) {
        $this->config = $config;
    }

    /**
     * Get the Magento customer id that gets sent to Spaaza
     *
     * In Spaaza, this property is called the 'authentication point identifier' of the user.
     *
     * Normally, this will just be the customer id in Magento. But for development purposes, it can be useful
     * to have unique identifiers for each development/staging/acceptance environment while using the same Spaaza
     * environment.
     *
     * There is a 'hidden' setting for this in the module:
     * spaaza_loyalty/identification/environment_prefix
     *
     * You can best lock this setting in your env.php:
     * bin/magento config:set --lock-env spaaza_loyalty/identification/environment_prefix MYDEV
     *
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     */
    public function getAuthenticationPointIdentifier(\Magento\Customer\Api\Data\CustomerInterface $customer): string
    {
        return $this->config->getEnvironmentPrefix() . (string)$customer->getId();
    }
}
