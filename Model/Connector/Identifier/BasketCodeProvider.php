<?php

namespace Spaaza\Loyalty\Model\Connector\Identifier;

class BasketCodeProvider
{
    /**
     * Get the basket code for an order
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return string|null  Returns a string if there is an increment id set, otherwise returns null
     */
    public function getBasketCodeForOrder(\Magento\Sales\Api\Data\OrderInterface $order): ?string
    {
        if ($order->getIncrementId()) {
            return (string)$order->getIncrementId();
        }
        return null;
    }

    /**
     * Get the basket code for a credit memo
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return string|null  Returns a string if there is an increment id set, otherwise returns null
     */
    public function getBasketCodeForCreditmemo(\Magento\Sales\Api\Data\CreditmemoInterface $creditmemo): ?string
    {
        if ($creditmemo->getIncrementId()) {
            return (string)$creditmemo->getIncrementId();
        }
        return null;
    }
}
