<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Api\Data\VoucherInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;
use Spaaza\Loyalty\Helper\Data as HelperData;

class Order
{
    const DATE_TIME_FORMAT_ISO8601 = 'Y-m-d\TH:i:sO';

    /**
     * @var Identifier\LockingCodeProvider
     */
    protected $lockingCodeProvider;

    /**
     * @var BasketItemIdentifierProviderInterface
     */
    protected $basketItemIdentifierProvider;

    /**
     * @var Identifier\BasketCodeProvider
     */
    protected $basketCodeProvider;

    /**
     * @var Identifier\UserIdentityProvider
     */
    protected $userIdentityProvider;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    protected $taxHelper;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    protected $requestFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    protected $requestQueue;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Identifier\RetailerIdentityProvider
     */
    protected $retailerIdentityProvider;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helperData;

    public function __construct(
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        HelperData $helperData,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        Identifier\LockingCodeProvider $lockingCodeProvider,
        BasketItemIdentifierProviderInterface $basketItemIdentifierProvider,
        Identifier\BasketCodeProvider $basketCodeProvider,
        Identifier\UserIdentityProvider $userIdentityProvider,
        Identifier\RetailerIdentityProvider $retailerIdentityProvider,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->eventManager = $eventManager;
        $this->requestQueue = $requestQueue;
        $this->requestFactory = $requestFactory;
        $this->helperData = $helperData;
        $this->taxHelper = $taxHelper;
        $this->localeDate = $localeDate;
        $this->logger = $logger;
        $this->lockingCodeProvider = $lockingCodeProvider;
        $this->basketItemIdentifierProvider = $basketItemIdentifierProvider;
        $this->basketCodeProvider = $basketCodeProvider;
        $this->userIdentityProvider = $userIdentityProvider;
        $this->retailerIdentityProvider = $retailerIdentityProvider;
    }

    /**
     * Send an order to Spaaza (asynchronous if needed)
     *
     * This is a separate function to be able to call it separately
     *
     * @param OrderInterface $order
     * @param bool $trySynchronous  First try synchronous before falling back to asynchronous call?
     * @return void
     */
    public function sendOrder(OrderInterface $order, $trySynchronous = true)
    {
        try {
            $request = $this->createOrderRequest($order);
            if ($request) {
                $this->requestQueue->addRequest($request, $trySynchronous);
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * Create a request object for an order
     *
     * @api
     * @param OrderInterface $order
     * @return null|RequestInterface
     */
    public function createOrderRequest(OrderInterface $order)
    {
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($order);

        if ($spaazaData->getBasketId()) {
            // don't send orders twice
            return null;
        }

        $userIdentity = $this->userIdentityProvider->getUserIdentityForOrder($order);

        $payload = [
            'entity' => $this->retailerIdentityProvider->getRetailerIdentity($order->getStoreId()),
            'user' => $userIdentity,
            'basket' => $this->getBasketData($order),
        ];

        $payload = $this->dispatchRequestEvent($order, $payload);

        if ($payload) {
            return $this->requestFactory->create()
                ->setStoreId($order->getStoreId())
                ->setPath('auth/add-basket.json')
                ->setPayload($payload)
                ->setEntityType(EntityType::ENTITY_TYPE_ORDER)
                ->setEntityId($order->getEntityId())
                ->setMethod(RequestInterface::METHOD_POST_JSON)
                ->setCallback('orderSaveBasketId');
        }
        return null;
    }

    /**
     * Dispatch an event to let other modules modify the request payload
     *
     * @deprecated Use a plugin for {@link createOrderRequest()} instead
     * @param OrderInterface $creditmemo
     * @param array $payload
     * @return array
     */
    protected function dispatchRequestEvent(OrderInterface $order, array $payload): array
    {
        $transport = new \Magento\Framework\DataObject(
            [
                'payload' => $payload,
            ]
        );
        $this->eventManager->dispatch('spaaza_creditmemo_request_before', [
            'order' => $order,
            'transport' => $transport,
        ]);
        return $transport->getData('payload');
    }

    /**
     * Get the basket data for an order
     *
     * @api
     * @param OrderInterface $order
     * @return array
     */
    public function getBasketData(OrderInterface $order): array
    {
        $storeTimezone = $this->localeDate->getConfigTimezone('store', $order->getStoreId());
        // This produces the datetime in UTC
        $dateTime = new \DateTime($order->getCreatedAt());
        // Now move it to the store's timezone
        $dateTime->setTimezone(new \DateTimeZone($storeTimezone));

        $basketItems = [];
        foreach ($order->getItems() as $orderItem) {
            $basketItemData = $this->getBasketItemData($orderItem, $order);
            if ($basketItemData) {
                $basketItems[] = $basketItemData;
            }
        }

        $claimedVouchers = $order->getExtensionAttributes()->getSpaazaData()->getVouchers();
        $basketVouchers = [];
        $honourVouchers = [];
        if (is_array($claimedVouchers)) {
            foreach ($claimedVouchers as $voucherInfo) {
                $isBasketVoucher = in_array(
                    $voucherInfo->getType(),
                    [ VoucherInterface::VOUCHER_TYPE_BASKET, VoucherInterface::VOUCHER_TYPE_BASKET_PCT ]
                );
                if (!$voucherInfo->getType() || $isBasketVoucher) {
                    $basketVouchers[] = ['voucher_key' => $voucherInfo->getKey()];
                } elseif ($voucherInfo->getType() == VoucherInterface::VOUCHER_TYPE_HONOUR) {
                    $honourVouchers[] = ['voucher_key' => $voucherInfo->getKey()];
                }
            }
        }

        $basketData = [
            // 'in_store' or 'online'
            'app_platform_type' => 'online',
            'basket_timestamp_iso8601' => $dateTime->format(self::DATE_TIME_FORMAT_ISO8601),
            'basket_timezone_name' => $storeTimezone,
            // any ID in the retailer's system used to identify a particular user basket.
            'retailer_basket_code' => $this->basketCodeProvider->getBasketCodeForOrder($order),
            'basket_total_price' => (float)$order->getGrandTotal() - (float)$order->getShippingInclTax(),
            'shipping_charge' => (float)$order->getShippingInclTax(),
            'basket_currency' => [
                'currency_code' => $order->getBaseCurrencyCode() // 3 letter currency code
            ],
            'basket_tax' => $this->getBasketTaxForOrder($order),
            'basket_vouchers' => $basketVouchers,
            'honour_vouchers' => $honourVouchers,
            'basket_items' => $basketItems,
        ];

        if ($voucherLockingCode = $this->getVoucherLockingCodeForOrder($order)) {
            $basketData['voucher_locking_code'] = $voucherLockingCode;
        }

        return $basketData;
    }

    /**
     * Get all relevant JSON data for a basket item
     *
     * @api
     * @param OrderItemInterface $orderItem
     * @param OrderInterface $order
     * @return array|null
     */
    public function getBasketItemData(OrderItemInterface $orderItem, OrderInterface $order): ?array
    {
        if ($orderItem->getParentItemId()) {
            // Child items don't hold a price
            return null;
        }
        if ($orderItem->getQtyOrdered() == 0) {
            // To prevent division by zero
            return null;
        }

        // This is a 'visible item' and should always hold the price and qty
        // In the case of a configurable item, this has the SKU of the simple and the product id of the
        // configurable product
        $price = (float)$orderItem->getPriceInclTax();
        $qty = (float)$orderItem->getQtyOrdered();
        $discount = $orderItem->getDiscountAmount() / $orderItem->getQtyOrdered();
        $sku = $orderItem->getSku();
        $isPromotional = $this->helperData->checkIfProductSkuIsSetToPromotional($sku);

        $salePrice = $price - $discount;
        if($isPromotional) {
            $basketItem = [
                'retailer_item_code' => (string)$orderItem->getItemId(),
                'item_quantity' => $qty,
                'item_price' => $salePrice,
                'retailer_product_code' => $sku,
                'product_name' => $orderItem->getName(),
                'item_is_promotional' => true
            ];
        } else {
            $basketItem = [
                'retailer_item_code' => (string)$orderItem->getItemId(),
                'item_quantity' => $qty,
                'item_price' => $salePrice,
                'retailer_product_code' => $sku,
                'product_name' => $orderItem->getName(),
            ];
        }
        $basketItem = array_replace(
            $basketItem,
            $this->basketItemIdentifierProvider->getIdentifierForOrderItem($orderItem)
        );
        return $this->dispatchBasketItemEvent($orderItem, $order, $basketItem);
    }

    /**
     * Dispatch an event to let other modules modify the amounts
     *
     * @deprecated Use a plugin for {@link getBasketItemData()} instead
     * @param OrderItemInterface $orderItem
     * @param OrderInterface $order
     * @param array $basketItem
     * @return array
     */
    protected function dispatchBasketItemEvent(
        OrderItemInterface $orderItem,
        OrderInterface $order,
        array $basketItem
    ): array {
        $transport = new \Magento\Framework\DataObject(
            [
                'product_sale_price' => (float)$basketItem['item_price'],
                'product_quantity' => (float)$basketItem['item_quantity'],
            ]
        );
        $this->eventManager->dispatch('spaaza_order_product_sale_price', [
            'order_item' => $orderItem,
            'order' => $order,
            'transport' => $transport,
        ]);
        $basketItem['item_quantity'] = (float)$transport->getData('product_quantity');
        $basketItem['item_price'] = (float)$transport->getData('product_sale_price');
        return $basketItem;
    }

    /**
     * Get the voucher locking code for the associated quote
     *
     * @param OrderInterface $order
     * @return string|null
     */
    protected function getVoucherLockingCodeForOrder(OrderInterface $order): ?string
    {
        if ($order->getQuoteId() > 0) {
            return $this->lockingCodeProvider->getLockingCodeForCart((int)$order->getQuoteId());
        }
        return null;
    }

    /**
     * Get the basket tax rates
     *
     * @param OrderInterface $order
     * @return array  Array ready to pass to Spaaza
     */
    protected function getBasketTaxForOrder(OrderInterface $order): array
    {
        $taxItems = $this->taxHelper->getCalculatedTaxes($order);
        $result = [];
        foreach ($taxItems as $taxItem) {
            $result[] = [
                'tax_rate' => round($taxItem['percent'] / 100, 2),
                'tax_total' => round($taxItem['tax_amount'], 2), // base_tax_amount is also available
            ];
        }
        return $result;
    }
}
