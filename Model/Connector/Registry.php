<?php

namespace Spaaza\Loyalty\Model\Connector;

class Registry
{
    private $cardCache = [];

    /**
     * Get the card cache for a customer
     *
     * @param $customerId
     * @return array|null|bool  Returns NULL if not found (there is no cache yet), FALSE if user does not exist
     */
    public function getCard($customerId)
    {
        if ($customerId && array_key_exists($customerId, $this->cardCache)) {
            return $this->cardCache[$customerId];
        }
        return null;
    }

    /**
     * Set the card cache for a customer
     *
     * @param int $customerId
     * @param bool|array|null $cardData
     * @return void
     */
    public function setCard($customerId, $cardData)
    {
        if ($cardData === null) {
            $cardData = false;
        }
        $this->cardCache[$customerId] = $cardData;
    }

    /**
     * Clear the card cache for a customer
     *
     * @param null $customerId
     */
    public function deleteCard($customerId = null)
    {
        if ($customerId) {
            unset($this->cardCache[$customerId]);
        } else {
            $this->cardCache = [];
        }
    }
}
