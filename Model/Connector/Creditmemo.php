<?php

namespace Spaaza\Loyalty\Model\Connector;

use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Model\Config\Source\EntityType;

class Creditmemo
{
    const DATE_TIME_FORMAT_ISO8601 = 'Y-m-d\TH:i:sO';

    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    protected $requestQueue;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Identifier\UserIdentityProvider
     */
    protected $userIdentityProvider;

    /**
     * @var BasketItemIdentifierProviderInterface
     */
    protected $basketItemIdentifierProvider;

    /**
     * @var Identifier\BasketCodeProvider
     */
    protected $basketCodeProvider;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory
     */
    protected $requestFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Identifier\RetailerIdentityProvider
     */
    protected $retailerIdentityProvider;

    public function __construct(
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Model\Client\Request\Queue $requestQueue,
        \Spaaza\Loyalty\Api\Data\Client\RequestInterfaceFactory $requestFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        BasketItemIdentifierProviderInterface $basketItemIdentifierProvider,
        Identifier\BasketCodeProvider $basketCodeProvider,
        Identifier\UserIdentityProvider $userIdentityProvider,
        Identifier\RetailerIdentityProvider $retailerIdentityProvider,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->requestQueue = $requestQueue;
        $this->logger = $logger;
        $this->userIdentityProvider = $userIdentityProvider;
        $this->basketItemIdentifierProvider = $basketItemIdentifierProvider;
        $this->basketCodeProvider = $basketCodeProvider;
        $this->eventManager = $eventManager;
        $this->config = $config;
        $this->requestFactory = $requestFactory;
        $this->localeDate = $localeDate;
        $this->orderRepository = $orderRepository;
        $this->retailerIdentityProvider = $retailerIdentityProvider;
    }

    /**
     * Send an order to Spaaza (asynchronous if needed)
     *
     * This is a separate function to be able to call it separately
     *
     * @param CreditmemoInterface $creditmemo
     * @param bool $trySynchronous First try synchronous before falling back to asynchronous call?
     * @return void
     */
    public function sendCreditmemo(CreditmemoInterface $creditmemo, $trySynchronous = true)
    {
        try {
            $request = $this->createCreditmemoRequest($creditmemo);
            if ($request) {
                $this->requestQueue->addRequest($request, $trySynchronous);
            }
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
    }

    /**
     * Send a credit memo to Spaaza (asynchronous if needed)
     *
     * @api
     * @param CreditmemoInterface $creditmemo
     * @return RequestInterface|null
     */
    public function createCreditmemoRequest(CreditmemoInterface $creditmemo): ?RequestInterface
    {
        $order = $this->orderRepository->get($creditmemo->getOrderId());

        $spaazaBasketId = $order->getExtensionAttributes()->getSpaazaData()->getBasketId();
        if (!$spaazaBasketId) {
            // apparently this order has not been sent to spaaza
            return null;
        }

        $userIdentity = $this->userIdentityProvider->getUserIdentityForCreditMemo($creditmemo);

        $payload = [
            'entity' => $this->retailerIdentityProvider->getRetailerIdentity($creditmemo->getStoreId()),
            'user' => $userIdentity,
            'basket' => $this->getBasketData($creditmemo),
        ];

        $payload = $this->dispatchRequestEvent($creditmemo, $payload);

        if ($payload) {
            return $this->requestFactory->create()
                ->setStoreId($creditmemo->getStoreId())
                ->setPath('auth/add-basket.json')
                ->setPayload($payload)
                ->setEntityType(EntityType::ENTITY_TYPE_CREDITMEMO)
                ->setEntityId($creditmemo->getEntityId())
                ->setMethod(RequestInterface::METHOD_POST_JSON)
                ->setCallback('creditmemoSaveBasketId');
        }
        return null;
    }

    /**
     * Dispatch an event to let other modules modify the request payload
     *
     * @deprecated Use a plugin for {@link createCreditmemoRequest()} instead
     * @param CreditmemoInterface $creditmemo
     * @param array $payload
     * @return array
     */
    protected function dispatchRequestEvent(CreditmemoInterface $creditmemo, array $payload): array
    {
        $transport = new \Magento\Framework\DataObject(
            [
                'payload' => $payload,
            ]
        );
        $this->eventManager->dispatch('spaaza_creditmemo_request_before', [
            'creditmemo' => $creditmemo,
            'transport' => $transport,
        ]);
        return $transport->getData('payload');
    }

    /**
     * Get the basket data for a credit memo
     *
     * @api
     * @param CreditmemoInterface $creditmemo
     * @return array
     */
    public function getBasketData(CreditmemoInterface $creditmemo)
    {
        $order = $this->getOrderForCreditMemo($creditmemo);
        $storeTimezone = $this->localeDate->getConfigTimezone('store', $creditmemo->getStoreId());
        // This produces the datetime in UTC
        $dateTime = new \DateTime($creditmemo->getCreatedAt());
        // Now move it to the store's timezone
        $dateTime->setTimezone(new \DateTimeZone($storeTimezone));

        $basketItems = [];
        foreach ($creditmemo->getItems() as $creditmemoItem) {
            $basketItemData = $this->getBasketItemData($creditmemoItem, $creditmemo);
            if ($basketItemData && $order) {
                $basketItemData['original_retailer_basket_code'] =
                    $this->basketCodeProvider->getBasketCodeForOrder($order);
                $basketItems[] = $basketItemData;
            }
        }

        return [
            'app_platform_type' => 'online', // 'in_store' or 'online'
            'basket_timestamp_iso8601' => $dateTime->format(self::DATE_TIME_FORMAT_ISO8601),
            // any ID in the retailer's system used to identify a particular user basket.
            'retailer_basket_code' => $this->basketCodeProvider->getBasketCodeForCreditmemo($creditmemo),
            'basket_total_price' => -1.0 * ($creditmemo->getGrandTotal() - $creditmemo->getShippingInclTax()),
            'shipping_charge' => -1.0 * $creditmemo->getShippingInclTax(),
            'basket_currency' => [
                'currency_code' => $creditmemo->getBaseCurrencyCode() // 3 letter currency code
            ],
            'basket_tax' => $this->getBasketTaxForCreditmemo($creditmemo),
            'basket_items' => $basketItems,
        ];
    }

    /**
     * Get all relevant JSON data for a (return) basket item
     *
     * @api
     * @param CreditmemoItemInterface $creditmemoItem
     * @param CreditmemoInterface $creditmemo
     * @return array|null
     */
    public function getBasketItemData(CreditmemoItemInterface $creditmemoItem, CreditmemoInterface $creditmemo): ?array
    {
        if ($creditmemoItem->getOrderItem()->getParentItemId()) {
            // Child items don't hold a price
            return null;
        }
        if ($creditmemoItem->getQty() == 0) {
            // To prevent division by zero
            return null;
        }
        $price = $creditmemoItem->getBasePriceInclTax();
        $qty = (float)$creditmemoItem->getQty();
        $discount = $creditmemoItem->getBaseDiscountAmount() / $qty;

        $salePrice = $price - $discount;
        $basketItem = [
            'item_price' => -1 * (float)$salePrice,
            'item_quantity' => (float)$qty,
            'original_retailer_item_code' => (string)$creditmemoItem->getOrderItemId(),
            'retailer_product_code' => $creditmemoItem->getSku(),
            'return_item' => true,
        ];
        $basketItem = array_replace(
            $basketItem,
            $this->basketItemIdentifierProvider->getIdentifierForCreditmemoItem($creditmemoItem)
        );
        return $this->dispatchBasketItemEvent($creditmemoItem, $creditmemo, $basketItem);
    }

    /**
     * Dispatch an event to let other modules modify the amounts
     *
     * @deprecated Use a plugin for {@link getBasketItemData()} instead
     * @param CreditmemoItemInterface $creditmemoItem
     * @param CreditmemoInterface $creditmemo
     * @param array $basketItem
     * @return array
     */
    protected function dispatchBasketItemEvent(
        CreditmemoItemInterface $creditmemoItem,
        CreditmemoInterface $creditmemo,
        array $basketItem
    ): array {
        $transport = new \Magento\Framework\DataObject(
            [
                'product_sale_price' => (float)$basketItem['item_price'],
                'product_quantity' => (float)$basketItem['item_quantity'],
            ]
        );
        $this->eventManager->dispatch('spaaza_order_product_sale_price', [
            'order_item' => $creditmemoItem->getOrderItem(),
            'creditmemo_item' => $creditmemoItem,
            'return_item' => true,
            'order' => $this->getOrderForCreditMemo($creditmemo),
            'creditmemo' => $creditmemo,
            'transport' => $transport,
        ]);
        $basketItem['item_quantity'] = (float)$transport->getData('product_quantity');
        $basketItem['item_price'] = (float)$transport->getData('product_sale_price');
        return $basketItem;
    }

    /**
     * Get an order by its id, or return null
     *
     * @param CreditmemoInterface $creditmemo
     * @return OrderInterface|null
     */
    protected function getOrderForCreditMemo(CreditmemoInterface $creditmemo): ?OrderInterface
    {
        $orderId = $creditmemo->getOrderId();
        $order = null;
        try {
            if ($orderId > 0) {
                $order = $this->orderRepository->get($orderId);
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $order = null;
        }
        return $order;
    }

    /**
     * Get basket tax rates for a credit memo
     *
     * Unfortunately, there's no easy way like for orders. We need to compose the values ourselves.
     *
     * @param CreditmemoInterface $creditmemo The creditmemo to calculate tax for
     * @return array  Array ready to pass to Spaaza (with negative values)
     */
    protected function getBasketTaxForCreditmemo(CreditmemoInterface $creditmemo)
    {
        $taxRates = [];
        foreach ($creditmemo->getItems() as $creditmemoItem) {
            if ($orderItem = $creditmemoItem->getOrderItem()) {
                $percent = $orderItem->getTaxPercent();
                if (!isset($taxRates[$percent])) {
                    $taxRates[$percent] = [
                        'tax_rate' => round($percent / 100, 2),
                        'tax_total' => 0,
                    ];
                }
                $taxRates[$percent]['tax_total'] -= $creditmemoItem->getTaxAmount();
            }
        }
        return array_values($taxRates);
    }
}
