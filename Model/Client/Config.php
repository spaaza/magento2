<?php

namespace Spaaza\Loyalty\Model\Client;

use Magento\Store\Model\ScopeInterface;
use Spaaza\Loyalty\Model\Config as ConfigModel;

class Config extends \Spaaza\Client\Config
{
    const XML_PATH_CONNECT_TIMEOUT = 'spaaza_loyalty/client/connect_timeout';
    const XML_PATH_READ_TIMEOUT = 'spaaza_loyalty/client/read_timeout';
    const XML_PATH_SPAAZA_HOSTNAME = 'spaaza_loyalty/client/spaaza_hostname';
    const XML_PATH_BASE_URL = 'spaaza_loyalty/client/base_url';
    const XML_PATH_AUTH_KEY = 'spaaza_loyalty/client/auth_key';
    const XML_PATH_AUTH_SECRET = 'spaaza_loyalty/client/auth_secret';

    public $apiVersion = '1.4.2';
    public $version = 'v1';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var int|null
     */
    protected $websiteId;

    /**
     * @var ConfigModel
     */
    protected $spaazaConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Spaaza\Loyalty\Helper\Data $spaazaHelper,
        \Spaaza\Loyalty\Model\Config $spaazaConfig,
        ?int $websiteId = null
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->helper = $spaazaHelper;
        $this->encryptor = $encryptor;
        $this->websiteId = $websiteId;
        $this->spaazaConfig = $spaazaConfig;

        $config = $this->getMagentoConfig();
        parent::__construct($config);
    }

    /**
     * Get some default values
     *
     * @return array
     */
    protected function getDefaultConfig()
    {
        return [
            'connect_timeout' => 2,
            'read_timeout' => 5,
        ];
    }

    /**
     * Merge the default values with config values
     *
     * If a value is empty ('0', '' or null) and there is a default value in the
     * defaults array, then use that value.
     *
     * @param array $defaults  Default values - only specify the keys with default values
     * @param array $values  Configured values
     * @return array
     */
    protected function mergeDefaults(array $defaults, array $values)
    {
        foreach ($values as $key => $value) {
            if (array_key_exists($key, $defaults) && empty($value)) {
                $values[$key] = $defaults[$key];
            }
        }
        return $values;
    }

    /**
     * Get the Magento config to feed to the parent config object
     *
     * @return array  Associative array with key/value pairs
     */
    protected function getMagentoConfig()
    {
        $websiteId = $this->websiteId;
        if (empty($websiteId)) {
            // Force the 'global' scope if no store id given
            $websiteId = 0;
        }
        return $this->mergeDefaults($this->getDefaultConfig(), [
            'auth_key' => $this->getMagentoConfigValue(self::XML_PATH_AUTH_KEY, $websiteId),
            'auth_secret' => $this->getMagentoConfigValue(self::XML_PATH_AUTH_SECRET, $websiteId, true),
            'base_url' => $this->getMagentoConfigValue(self::XML_PATH_BASE_URL, $websiteId),
            'hostname' => $this->getMagentoConfigValue(self::XML_PATH_SPAAZA_HOSTNAME, $websiteId),
            'chain_id' => $this->spaazaConfig->getChainId($websiteId),
            'user_agent' => $this->getUserAgent(),
            'connect_timeout' => $this->getMagentoConfigValue(self::XML_PATH_CONNECT_TIMEOUT, 0),
            'read_timeout' => $this->getMagentoConfigValue(self::XML_PATH_READ_TIMEOUT, 0),
            'api_version' => $this->apiVersion,
        ]);
    }

    /**
     * Get a Magento config value for the given store scope
     *
     * @param string $key
     * @param int $websiteId
     * @param bool $decrypt
     * @return string
     */
    protected function getMagentoConfigValue(string $key, int $websiteId = 0, bool $decrypt = false)
    {
        $value = $this->scopeConfig->getValue($key, ScopeInterface::SCOPE_WEBSITE, $websiteId);
        if ($decrypt) {
            $value = $this->encryptor->decrypt($value);
        }
        return $value;
    }

    /**
     * Get the user agent string for the module (including the module version)
     *
     * The parent's user agent gets concatenated
     *
     * @return string
     */
    public function getUserAgent()
    {
        $version = $this->helper->getModuleVersion();

        $agent = 'MagentoSpaazaClient/' . $version;

        $parentAgent = parent::getUserAgent();
        if ($parentAgent) {
            $agent .= ' ' . $parentAgent;
        }
        return $agent;
    }
}
