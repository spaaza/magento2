<?php

namespace Spaaza\Loyalty\Model\Client;

/**
 * Factory class for @see \Spaaza\Loyalty\Model\Client\Config
 */
class ConfigFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create config based on website id
     *
     * @param int $websiteId
     * @return \Spaaza\Loyalty\Model\Client\Config
     */
    public function create(int $websiteId)
    {
        return $this->_objectManager->create(
            \Spaaza\Loyalty\Model\Client\Config::class,
            [
                'websiteId' => $websiteId
            ]
        );
    }
}
