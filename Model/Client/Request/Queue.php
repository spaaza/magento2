<?php

namespace Spaaza\Loyalty\Model\Client\Request;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\SortOrderBuilderFactory;
use Spaaza\Client\ErrorsException;
use Spaaza\Loyalty\Api\Data\Client\RequestInterface;
use Spaaza\Loyalty\Model\Client\Request;

class Queue
{
    const XML_PATH_PROCESS_REQUESTS_LIMIT = 'spaaza_loyalty/requests_queue/process_requests_limit';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var SearchCriteriaBuilderFactory
     */
    protected $searchCriteriaBuilderFactory;

    /**
     * @var SortOrderBuilderFactory
     */
    protected $sortOrderBuilderFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var CallbackProcessor
     */
    protected $callbackProcessor;

    /**
     * @var \Spaaza\Loyalty\Api\Client\RequestRepositoryInterface
     */
    protected $requestRepository;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $spaazaConfig;

    /**
     * @var \Spaaza\Loyalty\Model\Client\SpaazaClientProvider
     */
    protected $clientProvider;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        CallbackProcessor $callbackProcessor,
        \Spaaza\Loyalty\Api\Client\RequestRepositoryInterface $requestRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        SortOrderBuilderFactory $sortOrderBuilderFactory,
        \Spaaza\Loyalty\Model\Config $spaazaConfig,
        \Spaaza\Loyalty\Model\Client\SpaazaClientProvider $clientProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->dateTime = $dateTime;
        $this->logger = $logger;
        $this->callbackProcessor = $callbackProcessor;
        $this->requestRepository = $requestRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->sortOrderBuilderFactory = $sortOrderBuilderFactory;
        $this->spaazaConfig = $spaazaConfig;
        $this->clientProvider = $clientProvider;
        $this->storeManager = $storeManager;
    }

    /**
     * Add a new request to the queue with retries
     *
     * @param RequestInterface $request
     * @param bool $trySynchronous  Try to send it immediately? If not, or it fails, it will be picked up by the cron
     * @return RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addRequest(RequestInterface $request, $trySynchronous = false)
    {
        $this->requestRepository->save($request);
        if ($trySynchronous) {
            $this->sendAsynchronousRequest($request);
        }
        return $request;
    }

    /**
     * Send pending requests to Spaaza API
     *
     * @return array  Status info with keys 'error' and 'success'
     */
    public function sendPendingRequests()
    {
        $requests = $this->getPendingRequests();
        $result = [
            'success' => 0,
            'error' => 0,
        ];
        foreach ($requests as $request) {
            if ($this->sendAsynchronousRequest($request)) {
                $result['success']++;
            } else {
                $result['error']++;
            }
        }

        return $result;
    }

    /**
     * Send a request to the Spaaza API immediately and use asynchronous fallback
     *
     * If the request has not been saved before passing it to this method, this method will
     * also not save it. Otherwise, it will.
     * Failed requests will *always* be saved to facilitate the retry mechanism.
     *
     * This method will catch exceptions and log them in the request. It will never throw an
     * exception (except if saving the request after an error fails).
     *
     * @param Request $request
     * @param bool $force Force sending of the request? Otherwise, only pending requests will be sent
     * @return bool  TRUE if success, FALSE if not
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function sendAsynchronousRequest(RequestInterface $request, $force = false)
    {
        if (!$force && $request->getStatus() != RequestInterface::STATUS_PENDING) {
            // the request has already been sent, don't send it again
            return true;
        }
        $request->setIsSynchronous(0);

        try {
            $success = false;
            $this->doRequest($request);
            $success = true;
        } catch (ErrorsException $e) {
            if ($e->isFatal()) {
                // don't retry but mark as failed permanently
                $request->markError($e->getMessage());
                $this->requestRepository->save($request);
            } else {
                // do retry
                $request->setMessage($e->getMessage())
                    ->markPending();
                $this->requestRepository->save($request);
            }
        } catch (\Exception $e) {
            $request->setMessage($e->getMessage())
                ->markPending();
            $this->requestRepository->save($request);
        }
        return $success;
    }

    /**
     * Sends a synchronous request and returns the result
     *
     * Catches exceptions, but only to log them on the request and then re-throwing it.
     * If the request has not been saved before passing it to this method, this method will
     * also not save it. Otherwise, it will.
     *
     * @param Request $request
     * @param bool $save  Save the request before it's sent? Only for logging; there is no other reason to save it.
     * @return mixed  The json_decoded result
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function sendSynchronousRequest(RequestInterface $request, $save = false)
    {
        try {
            if ($save || $this->spaazaConfig->isDebugRequestsEnabled()) {
                $this->requestRepository->save($request);
            }
            $request->setIsSynchronous(1);
            return $this->doRequest($request);
        } catch (\Exception $e) {
            $request->markError($e->getMessage());
            if ($request->getId()) {
                $this->requestRepository->save($request);
            }
            throw $e;
        }
    }

    /**
     * Only sends the request, does not do any exception handling
     *
     * @param Request $request
     * @return array  The json_decoded result
     * @throws \Exception
     */
    protected function doRequest(Request $request)
    {
        if ($request->getStatus() == $request::STATUS_PROCESSING) {
            if (strtotime($request->getSentAt()) - $this->dateTime->gmtTimestamp() < 60) {
                // don't process requests in status processing that have been sent less than one minute ago
                return [];
            }
        }
        try {
            $websiteId = $this->storeManager->getStore($request->getStoreId())->getWebsiteId();
            $client = $this->clientProvider->getClientForWebsite((int)$websiteId);

            $request->setResponse(null);
            $request->markProcessing();
            if ($request->getId()) {
                $this->requestRepository->save($request);
            }
            $options = $request->getOptions();
            if (!$options) {
                $options = [];
            }
            $request->markSent();
            $startTime = microtime(true);

            if (!$request->getPayload()) {
                $payload = [];
            } else {
                $payload = $request->getPayload();
            }
            if (!is_array($payload)) {
                throw new \RuntimeException(sprintf('Malformed payload in request #%d', $request->getId()), true);
            }

            try {
                $responseData = $client->request($request->getPath(), $payload, $request->getMethod(), $options);
            } finally {
                $request->setExecutionTime(round((microtime(true) - $startTime) * 1000));
                if ($client->getLastResponse()) {
                    $request->setResponse($client->getLastResponse()->getBody());
                }
            }
            $request->markComplete();

            if ($request->getCallback()) {
                $callback = $request->getCallback();
                $callbackProcessor = $this->callbackProcessor;
                if (method_exists($callbackProcessor, $callback)) {
                    $callbackProcessor->$callback($request, $responseData, $this);
                } else {
                    $this->logger->warning(
                        sprintf(
                            'Callback %s does not exist for Spaaza Api Queue in request #%d',
                            $callback,
                            $request->getId()
                        )
                    );
                }
            }
            return $responseData;
        } finally {
            if ($request->getId()) {
                $this->requestRepository->save($request);
            }
        }
        return [];
    }

    /**
     * Get a (limited) collection of pending asynchronous requests, sorted oldest first
     *
     * @return RequestInterface[]
     */
    public function getPendingRequests(): array
    {
        /** @var SearchCriteriaBuilder $builder */
        $criteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        /** @var SortOrderBuilder $builder */
        $sortBuilder = $this->sortOrderBuilderFactory->create();

        $sortOrder = $sortBuilder
            ->setField('created_at')
            ->setDirection(SortOrder::SORT_DESC)
            ->create();

        $searchCriteria = $criteriaBuilder
            ->addFilter('status', Request::STATUS_PENDING)
            ->addFilter('is_synchronous', 0)
            ->addSortOrder($sortOrder)
            ->setPageSize(max((int)$this->scopeConfig->getValue(self::XML_PATH_PROCESS_REQUESTS_LIMIT), 2))
            ->setCurrentPage(1)
            ->create();

        return $this->requestRepository->getList($searchCriteria)->getItems();
    }
}
