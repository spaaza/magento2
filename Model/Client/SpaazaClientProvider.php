<?php

namespace Spaaza\Loyalty\Model\Client;

class SpaazaClientProvider
{
    /**
     * @var ConfigFactory
     */
    protected $clientConfigFactory;

    /**
     * @var \Spaaza\Client[]
     */
    protected $clients = [];

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Spaaza\Loyalty\Model\Client\ConfigFactory $clientConfigFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->clientConfigFactory = $clientConfigFactory;
        $this->logger = $logger;
    }

    /**
     * Get a client, preconfigured for a specific Magento website
     *
     * @param int $websiteId
     * @return \Spaaza\Client
     */
    public function getClientForWebsite(int $websiteId): \Spaaza\Client
    {
        if (!isset($this->clients[$websiteId])) {
            $clientConfig = $this->clientConfigFactory->create($websiteId);
            $this->clients[$websiteId] = new \Spaaza\Client($this->logger, $clientConfig);
        }
        return $this->clients[$websiteId];
    }
}
