<?php

namespace Spaaza\Loyalty\Model\Client;

class RequestManagement
{
    const XML_PATH_KEEP_ERROR_REQUESTS_DAYS = 'spaaza_loyalty/requests_queue/keep_error_requests_days';
    const XML_PATH_KEEP_COMPLETE_REQUESTS_DAYS = 'spaaza_loyalty/requests_queue/keep_complete_requests_days';

    protected $requestResource;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Spaaza\Loyalty\Model\ResourceModel\Client\Request $requestResource
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->requestResource = $requestResource;
    }

    /**
     * Cleanup old requests
     *
     * @return array  Status array with keys 'deleted_complete' and 'deleted_error'
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function cleanupRequests()
    {
        $keepCompleteDays = (int)$this->scopeConfig->getValue(self::XML_PATH_KEEP_COMPLETE_REQUESTS_DAYS);
        $keepErrorDays = (int)$this->scopeConfig->getValue(self::XML_PATH_KEEP_ERROR_REQUESTS_DAYS);

        $status = [
            'deleted_complete' => 0,
            'deleted_error' => 0,
        ];
        if ($keepCompleteDays > 0) {
            // keep for a minimum of 1 day
            $keepCompleteDays = max($keepCompleteDays, 1);
            $completeStatuses = [
                Request::STATUS_COMPLETE,
            ];
            $status['deleted_complete'] +=
                $this->requestResource->cleanupRequests($completeStatuses, $keepCompleteDays);
        }
        if ($keepErrorDays > 0) {
            // keep for a minimum of 2 days, otherwise the chance of deleting legitimate pending requests is too high
            $keepErrorDays = max($keepErrorDays, 2);
            $errorStatuses = [
                Request::STATUS_PENDING,
                Request::STATUS_ERROR,
                Request::STATUS_PROCESSING,
            ];
            $status['deleted_error'] += $this->requestResource->cleanupRequests($errorStatuses, $keepErrorDays);
        }
        return $status;
    }
}
