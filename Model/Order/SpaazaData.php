<?php

namespace Spaaza\Loyalty\Model\Order;

use Magento\Framework\Model\AbstractModel;
use Spaaza\Loyalty\Api\Data\Order\SpaazaDataInterface;

/**
 * @method \Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData getResource()
 * @method \Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData\Collection getCollection()
 */
class SpaazaData extends AbstractModel implements SpaazaDataInterface
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherInfoSerializer
     */
    protected $voucherInfoSerializer;

    /**
     * @var VoucherDistributionSerializer
     */
    protected $voucherDistributionSerializer;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Spaaza\Loyalty\Model\VoucherInfoSerializer $voucherInfoSerializer,
        \Spaaza\Loyalty\Model\VoucherDistributionSerializer $voucherDistributionSerializer,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->voucherInfoSerializer = $voucherInfoSerializer;
        $this->voucherDistributionSerializer = $voucherDistributionSerializer;
    }

    protected function _construct()
    {
        $this->_init(\Spaaza\Loyalty\Model\ResourceModel\Order\SpaazaData::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getVouchers()
    {
        $value = $this->getData(self::KEY_VOUCHERS);
        if (!$value) {
            return [];
        }
        return $this->voucherInfoSerializer->unserializeVouchers($value);
    }

    /**
     * {@inheritdoc}
     */
    public function setVouchers($vouchers)
    {
        return $this->setData(
            self::KEY_VOUCHERS,
            $this->voucherInfoSerializer->unserializeVouchers($vouchers)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherDistribution()
    {
        $value = $this->getData(self::KEY_VOUCHER_DISTRIBUTION);
        if ($value === null) {
            return null;
        }
        return $this->voucherDistributionSerializer->unserializeVoucherDistribution($value);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherDistribution($voucherDistribution)
    {
        return $this->setData(
            self::KEY_VOUCHER_DISTRIBUTION,
            $this->voucherDistributionSerializer->serializeVoucherDistribution($voucherDistribution)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return (int)$this->getData(self::KEY_ORDER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::KEY_ORDER_ID, (int)$orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getBasketId()
    {
        return $this->getData(self::KEY_BASKET_ID) !== null
            ? (int)$this->getData(self::KEY_BASKET_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setBasketId(?int $basketId)
    {
        return $this->setData(self::KEY_BASKET_ID, $basketId);
    }

    /**
     * {@inheritdoc}
     */
    public function getMemberNumber()
    {
        return $this->getData(self::KEY_MEMBER_NUMBER);
    }

    /**
     * {@inheritdoc}
     */
    public function setMemberNumber($memberNumber)
    {
        return $this->setData(self::KEY_MEMBER_NUMBER, $memberNumber);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->getData(self::KEY_USER_ID) !== null
            ? (int)$this->getData(self::KEY_USER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId(?int $userId)
    {
        return $this->setData(self::KEY_USER_ID, $userId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmount()
    {
        return (float)$this->getData(self::KEY_VOUCHER_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmount($voucherAmount)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT, (float)$voucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseVoucherAmount()
    {
        return (float)$this->getData(self::KEY_BASE_VOUCHER_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseVoucherAmount($baseVoucherAmount)
    {
        return $this->setData(self::KEY_BASE_VOUCHER_AMOUNT, (float)$baseVoucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmountInvoiced()
    {
        return (float)$this->getData(self::KEY_VOUCHER_AMOUNT_INVOICED);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmountInvoiced($voucherAmountInvoiced)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT_INVOICED, (float)$voucherAmountInvoiced);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseVoucherAmountInvoiced()
    {
        return (float)$this->getData(self::KEY_BASE_VOUCHER_AMOUNT_INVOICED);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseVoucherAmountInvoiced($baseVoucherAmountInvoiced)
    {
        return $this->setData(self::KEY_BASE_VOUCHER_AMOUNT_INVOICED, (float)$baseVoucherAmountInvoiced);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmountRefunded()
    {
        return (float)$this->getData(self::KEY_VOUCHER_AMOUNT_REFUNDED);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmountRefunded($voucherAmountRefunded)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT_REFUNDED, (float)$voucherAmountRefunded);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseVoucherAmountRefunded()
    {
        return (float)$this->getData(self::KEY_BASE_VOUCHER_AMOUNT_REFUNDED);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseVoucherAmountRefunded($baseVoucherAmountRefunded)
    {
        return $this->setData(self::KEY_BASE_VOUCHER_AMOUNT_REFUNDED, (float)$baseVoucherAmountRefunded);
    }
}
