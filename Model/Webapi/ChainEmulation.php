<?php

namespace Spaaza\Loyalty\Model\Webapi;

/**
 * Emulate a website based on Chain Id
 */
class ChainEmulation
{
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    protected $emulation;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Model\Config\Share
     */
    protected $customerShareConfig;

    public function __construct(
        \Magento\Store\Model\App\Emulation $emulation,
        \Spaaza\Loyalty\Model\Config $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Config\Share $customerShareConfig
    ) {
        $this->emulation = $emulation;
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->customerShareConfig = $customerShareConfig;
    }

    /**
     * Emulate (the default store of) a website based on Spaaza Chain Id
     *
     * @param int|null $spaazaChainId
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function emulateBasedOnSpaazaChainId(?int $spaazaChainId): void
    {
        $websiteId = $this->getWebsiteIdForSpaazaChainId($spaazaChainId);
        if ($websiteId === null) {
            // No emulation needed
            return;
        }
        $store = $this->storeManager->getWebsite($websiteId)->getDefaultStore();
        $this->emulation->startEnvironmentEmulation($store->getId());
    }

    /**
     * Get the website id that is configured for the given Spaaza Chain Id
     *
     * Also checks if a Spaaza Chain Id is required and throws an exception if it's empty and required.
     *
     * @param int|null $spaazaChainId
     * @return int|null  Returns a website id, or null if $spaazaChainId is empty and is allowed to be empty
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWebsiteIdForSpaazaChainId(?int $spaazaChainId): ?int
    {
        $map = $this->config->getChainWebsiteMap();
        if (empty($spaazaChainId)) {
            if ($this->customerShareConfig->isGlobalScope()) {
                // Fine, we don't need a Chain Id and no emulation if customers are on global scope
                return null;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Missing Chain Id: Customers are configured to have a website sharing scope.')
                );
            }
        }
        if (isset($map[$spaazaChainId])) {
            return (int)$map[$spaazaChainId];
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('The given Chain Id is not configured in Magento.')
            );
        }
    }
}
