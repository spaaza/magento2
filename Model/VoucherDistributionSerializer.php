<?php

namespace Spaaza\Loyalty\Model;

use Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface;

class VoucherDistributionSerializer
{
    /**
     * @var \Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterfaceFactory
     */
    protected $voucherDistributionItemFactory;

    public function __construct(
        \Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterfaceFactory $voucherDistributionItemFactory
    ) {
        $this->voucherDistributionItemFactory = $voucherDistributionItemFactory;
    }

    /**
     * @param array|string $voucherDistribution
     * @return string|null
     */
    public function serializeVoucherDistribution($voucherDistribution): ?string
    {
        if ($voucherDistribution === null) {
            return null;
        }
        if (is_array($voucherDistribution)) {
            $distributionData = [];
            foreach ($voucherDistribution as $voucherDistributionItem) {
                if ($voucherDistributionItem instanceof \Magento\Framework\Api\AbstractSimpleObject) {
                    $distributionData[] = $voucherDistributionItem->__toArray();
                } elseif (is_array($voucherDistribution)) {
                    $distributionData[] = $voucherDistribution;
                }
            }
            return json_encode($distributionData);
        } elseif (is_string($voucherDistribution)) {
            $distributionData = json_decode($voucherDistribution, true);
            if (is_array($distributionData)) {
                return json_encode($distributionData);
            }
        }
        return null;
    }

    /**
     * @param array|string $voucherDistribution
     * @return array
     */
    public function unserializeVoucherDistribution($voucherDistribution): array
    {
        if (is_string($voucherDistribution)) {
            $voucherDistribution = json_decode($voucherDistribution, true);
        }
        if (is_array($voucherDistribution) && count($voucherDistribution) > 0) {
            $result = [];
            foreach ($voucherDistribution as $voucherDistributionItem) {
                if (is_array($voucherDistributionItem) && !empty($voucherDistributionItem['item_barcode'])) {
                    $result[] = $this->voucherDistributionItemFactory->create(
                        [
                            'data' => $voucherDistributionItem
                        ]
                    );
                } elseif ($voucherDistributionItem instanceof VoucherDistributionItemInterface) {
                    $result[] = $voucherDistributionItem;
                }
            }
            return $result;
        }
        return [];
    }

    /**
     * Returns voucher distribution items from a Spaaza response
     *
     * @param array $basketItems  The 'basket_items' item of a Spaaza response
     * @return VoucherDistributionItemInterface[]
     */
    public function createFromSpaazaResponse(array $basketItems): array
    {
        $voucherDistribution = [];
        foreach ($basketItems as $basketItem) {
            if (!empty($basketItem['basket_voucher_distribution']) && !empty($basketItem['item_barcode'])) {
                if (!isset($voucherDistribution[$basketItem['item_barcode']])) {
                    /** @var \Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface $voucherDistributionItem */
                    $voucherDistributionItem = $this->voucherDistributionItemFactory->create();
                    $voucherDistributionItem->setAmountTotal(0)
                        ->setQty(0)
                        ->setRetailerItemCode($basketItem['retailer_item_code'] ?? null)
                        ->setItemBarcode($basketItem['item_barcode']);
                    $voucherDistribution[$basketItem['item_barcode']] = $voucherDistributionItem;
                }
                $voucherDistributionItem = &$voucherDistribution[$basketItem['item_barcode']];
                foreach ($basketItem['basket_voucher_distribution'] as $distribution) {
                    if (!empty($distribution['voucher_distribution_amount'])) {
                        $voucherDistributionItem->setAmountTotal(
                            $voucherDistributionItem->getAmountTotal()
                            + $distribution['voucher_distribution_amount']
                        );
                    }
                }
                $voucherDistributionItem->setQty(
                    $voucherDistributionItem->getQty() + $basketItem['item_quantity']
                );
                unset($voucherDistributionItem);
            }
        }
        return array_values($voucherDistribution);
    }
}
