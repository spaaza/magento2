<?php

namespace Spaaza\Loyalty\Model\Customer;

use Magento\Customer\Api\Data\CustomerInterface;
use Spaaza\Loyalty\Exception\SpaazaDataValidationException;
use Spaaza\Loyalty\Exception\Validation\CustomerReferralCodeException;

class SpaazaDataValidator
{
    /**
     * Validate the customer Spaaza Data before saving the customer
     *
     * @param CustomerInterface $customer
     * @return void
     * @throws SpaazaDataValidationException
     */
    public function validateCustomer(CustomerInterface $customer): void
    {
        $spaazaData = $customer->getExtensionAttributes()->getSpaazaData();
        if (!$spaazaData) {
            // There is nothing to validate
            return;
        }
        if ($referralCode = $spaazaData->getSignupReferralCode()) {
            $this->validateReferralCode($referralCode);
        }
    }

    /**
     * Validate a referral code
     *
     * @throws CustomerReferralCodeException
     */
    public function validateReferralCode(string $referralCode)
    {
        $referralCode = trim($referralCode);
        if (strlen($referralCode) !== 6) {
            throw new CustomerReferralCodeException(
                __('The referral code must be 6 characters long.')
            );
        }
    }
}
