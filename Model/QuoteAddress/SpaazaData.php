<?php

namespace Spaaza\Loyalty\Model\QuoteAddress;

use Spaaza\Loyalty\Api\Data\QuoteAddress\SpaazaDataInterface;

/**
 * @method \Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData getResource()
 * @method \Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData\Collection getCollection()
 */
class SpaazaData extends \Magento\Framework\Model\AbstractModel implements SpaazaDataInterface
{
    /**
     * @var \Spaaza\Loyalty\Model\VoucherInfoSerializer
     */
    protected $voucherInfoSerializer;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherDistributionSerializer
     */
    protected $voucherDistributionSerializer;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Spaaza\Loyalty\Model\VoucherInfoSerializer $voucherInfoSerializer,
        \Spaaza\Loyalty\Model\VoucherDistributionSerializer $voucherDistributionSerializer,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->voucherInfoSerializer = $voucherInfoSerializer;
        $this->voucherDistributionSerializer = $voucherDistributionSerializer;
    }

    protected function _construct()
    {
        $this->_init(\Spaaza\Loyalty\Model\ResourceModel\QuoteAddress\SpaazaData::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getQuoteAddressId(): ?int
    {
        return $this->getData(self::KEY_QUOTE_ADDRESS_ID) !== null
            ? (int)$this->getData(self::KEY_QUOTE_ADDRESS_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setQuoteAddressId(?int $quoteAddressId)
    {
        return $this->setData(self::KEY_QUOTE_ADDRESS_ID, $quoteAddressId);
    }

    /**
     * {@inheritdoc}
     */
    public function getMemberNumber(): ?string
    {
        return $this->getData(self::KEY_MEMBER_NUMBER);
    }

    /**
     * {@inheritdoc}
     */
    public function setMemberNumber(?string $memberNumber)
    {
        return $this->setData(self::KEY_MEMBER_NUMBER, $memberNumber);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserId(): ?int
    {
        return $this->getData(self::KEY_USER_ID) !== null
            ? (int)$this->getData(self::KEY_USER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId(?int $userId)
    {
        return $this->setData(self::KEY_USER_ID, $userId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVouchers(): array
    {
        $value = $this->getData(self::KEY_VOUCHERS);
        if (!$value) {
            return [];
        }
        return $this->voucherInfoSerializer->unserializeVouchers($value);
    }

    /**
     * {@inheritdoc}
     */
    public function setVouchers($vouchers)
    {
        return $this->setData(self::KEY_VOUCHERS, $this->voucherInfoSerializer->unserializeVouchers($vouchers));
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmount(): float
    {
        return (float)$this->getData(self::KEY_VOUCHER_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmount(float $voucherAmount)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT, $voucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseVoucherAmount(): float
    {
        return (float)$this->getData(self::KEY_BASE_VOUCHER_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseVoucherAmount(float $baseVoucherAmount)
    {
        return $this->setData(self::KEY_BASE_VOUCHER_AMOUNT, $baseVoucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastHash(): ?string
    {
        return $this->getData(self::KEY_LAST_HASH);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastHash(?string $lastHash)
    {
        return $this->setData(self::KEY_LAST_HASH, $lastHash);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastRequestAt(): ?string
    {
        return $this->getData(self::KEY_LAST_REQUEST_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastRequestAt(?string $lastRequestAt)
    {
        return $this->setData(self::KEY_LAST_REQUEST_AT, $lastRequestAt);
    }


    /**
     * {@inheritdoc}
     */
    public function getVoucherDistribution()
    {
        $value = $this->getData(self::KEY_VOUCHER_DISTRIBUTION);
        if ($value === null) {
            return null;
        }
        return $this->voucherDistributionSerializer->unserializeVoucherDistribution($value);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherDistribution($voucherDistribution)
    {
        return $this->setData(
            self::KEY_VOUCHER_DISTRIBUTION,
            $this->voucherDistributionSerializer->serializeVoucherDistribution($voucherDistribution)
        );
    }
}
