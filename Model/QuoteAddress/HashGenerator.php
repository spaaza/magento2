<?php

namespace Spaaza\Loyalty\Model\QuoteAddress;

class HashGenerator
{
    /**
     * Get a hash of the quote address data relevant to Spaaza
     *
     * @param \Magento\Quote\Model\Quote\Address $address
     * @return string
     */
    public function getHash(\Magento\Quote\Api\Data\AddressInterface $address): string
    {
        $data = [
            $address->getCustomerId(),
            $address->getUpdatedAt(),
        ];

        foreach ($address->getAllItems() as $item) {
            $data[] = $item->getSku();
            $data[] = $item->getQty();
        }

        return substr(sha1(implode(',', $data)), 0, 16);
    }
}
