<?php

namespace Spaaza\Loyalty\Model\Invoice;

use Magento\Framework\Model\AbstractModel;
use Spaaza\Loyalty\Api\Data\Invoice\SpaazaDataInterface;

/**
 * @method \Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData getResource()
 * @method \Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData\Collection getCollection()
 */
class SpaazaData extends AbstractModel implements SpaazaDataInterface
{
    protected function _construct()
    {
        $this->_init(\Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getInvoiceId(): ?int
    {
        return $this->getData(self::KEY_INVOICE_ID) !== null
            ? $this->getData(self::KEY_INVOICE_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setInvoiceId(?int $invoiceId)
    {
        return $this->setData(self::KEY_INVOICE_ID, $invoiceId);
    }

    /**
     * {@inheritdoc}
     */
    public function getVoucherAmount()
    {
        return (float)$this->getData(self::KEY_VOUCHER_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setVoucherAmount($voucherAmount)
    {
        return $this->setData(self::KEY_VOUCHER_AMOUNT, (float)$voucherAmount);
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseVoucherAmount()
    {
        return (float)$this->getData(self::KEY_BASE_VOUCHER_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function setBaseVoucherAmount($baseVoucherAmount)
    {
        return $this->setData(self::KEY_BASE_VOUCHER_AMOUNT, (float)$baseVoucherAmount);
    }
}
