<?php

namespace Spaaza\Loyalty\Model\Invoice;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\InvoiceInterface;
use Spaaza\Loyalty\Api\Data\Invoice\SpaazaDataInterface;

class SpaazaDataManagement
{
    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Invoice\SpaazaDataInterfaceFactory
     */
    private $spaazaDataFactory;

    /**
     * @var \Magento\Sales\Api\Data\InvoiceExtensionFactory
     */
    private $invoiceExtensionFactory;

    /**
     * @var \Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData
     */
    private $spaazaDataResourceModel;

    /**
     * SpaazaDataManagement constructor.
     *
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Spaaza\Loyalty\Api\Data\Invoice\SpaazaDataInterfaceFactory $spaazaDataFactory
     * @param \Magento\Sales\Api\Data\InvoiceExtensionFactory $invoiceExtensionFactory
     * @param \Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData $spaazaDataResourceModel
     */
    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Api\Data\Invoice\SpaazaDataInterfaceFactory $spaazaDataFactory,
        \Magento\Sales\Api\Data\InvoiceExtensionFactory $invoiceExtensionFactory,
        \Spaaza\Loyalty\Model\ResourceModel\Invoice\SpaazaData $spaazaDataResourceModel
    ) {
        $this->helper = $helper;
        $this->spaazaDataFactory = $spaazaDataFactory;
        $this->invoiceExtensionFactory = $invoiceExtensionFactory;
        $this->spaazaDataResourceModel = $spaazaDataResourceModel;
    }

    /**
     * Ensures existing extension attributes and (loaded) Spaaza data
     *
     * @param InvoiceInterface $invoice
     * @return SpaazaDataInterface
     */
    public function applyExtensionAttributes(InvoiceInterface $invoice)
    {
        $extensionAttributes = $invoice->getExtensionAttributes();
        if (!$extensionAttributes) {
            $extensionAttributes = $this->invoiceExtensionFactory->create();
            $this->helper->debugLog(
                'Added extension attributes object to invoice',
                ['invoice' => $invoice->getEntityId() ?: 'new']
            );
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            if ($invoice->getEntityId()) {
                try {
                    $spaazaData = $this->getByInvoiceId($invoice->getEntityId());
                    $this->helper->debugLog(
                        'Got extension attribute from database',
                        ['invoice' => $invoice->getEntityId()]
                    );
                } catch (NoSuchEntityException $e) {
                    $spaazaData = $this->spaazaDataFactory->create();
                    $spaazaData->setInvoiceId($invoice->getEntityId());
                    $this->helper->debugLog(
                        'Created a new extension attribute for existing invoice',
                        ['invoice' => $invoice->getEntityId()]
                    );
                }
            } else {
                $spaazaData = $this->spaazaDataFactory->create();
                $this->helper->debugLog('Created a new extension attribute for new invoice');
            }
        }

        $extensionAttributes->setSpaazaData($spaazaData);
        $invoice->setExtensionAttributes($extensionAttributes);

        return $spaazaData;
    }

    /**
     * Retrieve Invoice SpaazaData By Invoice Id
     *
     * @param int $invoiceId
     * @return SpaazaDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByInvoiceId($invoiceId)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $invoiceId);
        if (!$spaazaData->getInvoiceId()) {
            throw new NoSuchEntityException(__('SpaazaData for invoice id "%1" does not exist.', $invoiceId));
        }
        return $spaazaData;
    }

    /**
     * Save Invoice SpaazaData
     *
     * @param SpaazaDataInterface $data
     * @return SpaazaDataInterface
     * @throws CouldNotSaveException
     */
    public function save(SpaazaDataInterface $data)
    {
        if (!$data->getInvoiceId()) {
            throw new CouldNotSaveException(__('Cannot save invoice Spaaza data without an invoice id'));
        }
        try {
            $this->helper->debugLog(
                'Save extension attribute',
                ['invoice' => $data->getInvoiceId()]
            );
            $this->spaazaDataResourceModel->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Could not save invoice Spaaza data: %1', $exception->getMessage()));
        }
        return $data;
    }
}
