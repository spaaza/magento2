<?php

namespace Spaaza\Loyalty\Model;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Spaaza\Loyalty\Api\CustomerManagementInterface;

class CustomerManagement implements CustomerManagementInterface
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManagement;

    /**
     * @var \Spaaza\Loyalty\Api\Data\AuthenticationResultInterfaceFactory
     */
    protected $authenticationResultFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilderFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Customer
     */
    protected $customerConnector;

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterfaceFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var Customer\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Api\Data\UserInfoInterfaceFactory
     */
    protected $userInfoInterfaceFactory;

    /**
     * @var Webapi\ChainEmulation
     */
    protected $chainEmulation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var Config
     */
    protected $moduleConfig;

    public function __construct(
        \Magento\Customer\Api\AccountManagementInterface $accountManagement,
        \Spaaza\Loyalty\Api\Data\AuthenticationResultInterfaceFactory $authenticationResultFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Customer\Api\Data\CustomerInterfaceFactory $customerFactory,
        \Spaaza\Loyalty\Model\Connector\Customer $customerConnector,
        \Spaaza\Loyalty\Api\Data\UserInfoInterfaceFactory $userInfoInterfaceFactory,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Webapi\ChainEmulation $chainEmulation,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Config $moduleConfig,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->accountManagement = $accountManagement;
        $this->authenticationResultFactory = $authenticationResultFactory;
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->customerConnector = $customerConnector;
        $this->encryptor = $encryptor;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->userInfoInterfaceFactory = $userInfoInterfaceFactory;
        $this->chainEmulation = $chainEmulation;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->moduleConfig = $moduleConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(string $email, string $password, ?int $spaazaChainId = null)
    {
        $result = $this->authenticationResultFactory->create();
        $result->setAuthenticated(false);

        // This also does validation, so also call it if $spaazaChainId is empty
        $this->chainEmulation->emulateBasedOnSpaazaChainId($spaazaChainId);

        try {
            $customer = $this->accountManagement->authenticate($email, $password);
            if ($customer->getId()) {
                $result->setAuthenticated(true)
                    ->setCustomerId($customer->getId());
                if ($customer->getWebsiteId()) {
                    $result->setWebsiteCode($this->storeManager->getWebsite($customer->getWebsiteId())->getCode());
                }
                if ($customer->getStoreId()) {
                    $result->setStoreCode($this->storeManager->getStore($customer->getStoreId())->getCode());
                }
            }
        } catch (AuthenticationException $e) {
            $result->setMessage($e->getMessage());
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function update(
        $userInfo,
        bool $createIfNotExists = false,
        ?int $spaazaChainId = null,
        ?string $websiteCode = null,
        ?string $storeCode = null
    ) {
        $customerId = null;
        $websiteId = $this->chainEmulation->getWebsiteIdForSpaazaChainId($spaazaChainId);

        if (!empty($userInfo['user_id'])) {
            $customerId = $this->spaazaDataManagement->findCustomerIdBySpaazaUserId(
                $userInfo['user_id'],
                $websiteId
            );
        } elseif (!empty($userInfo['member_number'])) {
            $customerId = $this->spaazaDataManagement->findCustomerIdBySpaazaMemberNumber(
                $userInfo['member_number'],
                $websiteId
            );
        }

        if ($customerId) {
            $customer = $this->customerRepository->getById($customerId);
        } else {
            $customer = null;
            if (!empty($userInfo['authentication_point_identifier'])) {
                try {
                    $customer = $this->customerRepository->getById($userInfo['authentication_point_identifier']);
                    $this->checkCustomerSpaazaUserInfo($customer, $userInfo);
                    // phpcs:ignore Magento2.Exceptions.ThrowCatch
                } catch (NoSuchEntityException $e) {
                    $customer = null;
                }
            }
            if (empty($customer) && !empty($userInfo['username'])) {
                try {
                    $customer = $this->customerRepository->get($userInfo['username'], $websiteId);
                    $this->checkCustomerSpaazaUserInfo($customer, $userInfo);
                    // phpcs:ignore Magento2.Exceptions.ThrowCatch
                } catch (NoSuchEntityException $e) {
                    $customer = null;
                }
            }
        }

        if (empty($customer) && $createIfNotExists) {
            $customer = $this->createNewCustomer($spaazaChainId, $websiteCode, $storeCode);
        }

        if (!empty($customer)) {
            $resultCustomer = $this->customerConnector->updateCustomerFromSpaazaUserInfo($customer, $userInfo);

            if (!empty($userInfo['password'])) {
                $passwordHash = $this->createPasswordHash($userInfo['password']);
                $resultCustomer = $this->customerConnector->saveCustomerWithoutSyncing($resultCustomer, $passwordHash);
            }

            if (isset($resultCustomer)) {
                // get the customer from the database again for if the address has changed
                $resultCustomer = $this->customerRepository->getById($resultCustomer->getId());
                return $resultCustomer;
            }
        } else {
            throw new NoSuchEntityException(__('Customer not found'));
        }
        return null;
    }

    /**
     * Create a new customer and assign it to a website and store
     *
     * @param int|null $spaazaChainId
     * @param string|null $websiteCode
     * @param string|null $storeCode
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function createNewCustomer(
        int $spaazaChainId = null,
        string $websiteCode = null,
        string $storeCode = null
    ) {
        // This returns null if $spaazaChainId is null and is allowed to be null
        $chainWebsiteId = $this->chainEmulation->getWebsiteIdForSpaazaChainId($spaazaChainId);
        $website = $websiteCode ? $this->storeManager->getWebsite($websiteCode) : null;
        $store = $storeCode ? $this->storeManager->getStore($storeCode) : null;

        if ($chainWebsiteId && !$website) {
            // Use the website that is configured to use $spaazaChainId
            $website = $this->storeManager->getWebsite($chainWebsiteId);
        }
        if (!$website) {
            // Use the current website if not specified
            $website = $this->storeManager->getWebsite();
        }
        if (!$store) {
            // Use the default store of the website if not specified
            $store = $website->getDefaultStore();
        }

        // Check if the given website is configured to use the given chain
        $websiteChainId = $this->moduleConfig->getChainId($website->getId());
        if ($spaazaChainId && $websiteChainId != $spaazaChainId) {
            throw new LocalizedException(
                __('Mismatch: website %1 is not configured to use chain %2.', $store->getCode(), $spaazaChainId)
            );
        }

        // Check if the store belongs to the website
        if (!in_array($store->getId(), $website->getStoreIds())) {
            throw new LocalizedException(
                __('Store %1 does not belong to website %2.', $store->getCode(), $website->getCode())
            );
        }

        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($website->getId())
            ->setStoreId($store->getId());
        return $customer;
    }

    /**
     * Check if a customer's member number and user id match the Spaaza user info
     *
     * @param CustomerInterface $customer
     * @param array $userInfo
     * @throws LocalizedException
     */
    protected function checkCustomerSpaazaUserInfo(CustomerInterface $customer, array $userInfo): void
    {
        $spaazaData = $customer->getExtensionAttributes()->getSpaazaData();
        if (!$spaazaData) {
            return;
        }
        if (!empty($userInfo['user_id'])
            && $spaazaData->getUserId()
            && $userInfo['user_id'] != $spaazaData->getUserId()
        ) {
            throw new LocalizedException(
                __('Found customer already has a Spaaza User Id: %1', $spaazaData->getUserId())
            );
        }
        if (!empty($userInfo['member_number'])
            && $spaazaData->getMemberNumber()
            && $userInfo['member_number'] != $spaazaData->getMemberNumber()
        ) {
            throw new LocalizedException(
                __('Found customer already has a Spaaza Member Number: %1', $spaazaData->getMemberNumber())
            );
        }
    }

    /**
     * Create a hash for the given password
     *
     * @param string $password
     * @return string
     */
    protected function createPasswordHash(string $password)
    {
        return $this->encryptor->getHash($password, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getSpaazaUserInfoByCustomerId(int $customerId)
    {
        $customer = $this->customerRepository->getById($customerId);
        $spaazaData = $this->customerConnector->getSpaazaUserInfoForCustomer($customer, true);
        return $this->userInfoInterfaceFactory->create(['data' => $spaazaData]);
    }

    /**
     * {@inheritdoc}
     */
    public function getSpaazaUserInfoByCustomerEmail(string $customerEmail)
    {
        $spaazaChainId = null;
        if ((int)$this->request->getParam('spaaza_chain_id') > 0) {
            $spaazaChainId = (int)$this->request->getParam('spaaza_chain_id');
        }
        // Only validate if Spaaza Chain Id can be left out
        $this->chainEmulation->emulateBasedOnSpaazaChainId($spaazaChainId);
        $customer = $this->customerRepository->get($customerEmail);
        $spaazaData = $this->customerConnector->getSpaazaUserInfoForCustomer($customer, true);
        return $this->userInfoInterfaceFactory->create(['data' => $spaazaData]);
    }

    /**
     * {@inheritdoc}
     */
    public function getSpaazaUserInfoByCustomerEmailAndChainId(int $spaazaChainId, string $customerEmail)
    {
        $this->chainEmulation->emulateBasedOnSpaazaChainId($spaazaChainId);
        $customer = $this->customerRepository->get($customerEmail);
        $spaazaData = $this->customerConnector->getSpaazaUserInfoForCustomer($customer, true);
        return $this->userInfoInterfaceFactory->create(['data' => $spaazaData]);
    }
}
