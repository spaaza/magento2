<?php

namespace Spaaza\Loyalty\Model\Data;

use Magento\Framework\Api\AbstractSimpleObject;
use Spaaza\Loyalty\Api\Data\AuthenticationResultInterface;

class AuthenticationResult extends AbstractSimpleObject implements AuthenticationResultInterface
{
    /**
     * {@inheritdoc}
     */
    public function getAuthenticated(): bool
    {
        return (bool)$this->_get(self::AUTHENTICATED);
    }

    /**
     * {@inheritdoc}
     */
    public function setAuthenticated($authenticated)
    {
        return $this->setData(self::AUTHENTICATED, (bool)$authenticated);
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerId(): ?int
    {
        return $this->_get(self::CUSTOMER_ID) !== null
            ? (int)$this->_get(self::CUSTOMER_ID)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId(?int $customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage(): ?string
    {
        return $this->_get(self::MESSAGE) !== null
            ? (string)$this->_get(self::MESSAGE)
            : null;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage(?string $message)
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteCode(): ?string
    {
        return $this->_get(self::WEBSITE_CODE) !== null
            ? (string)$this->_get(self::WEBSITE_CODE)
            : null;
    }

    /**
     * @inheritDoc
     */
    public function setWebsiteCode(?string $websiteCode)
    {
        return $this->setData(self::WEBSITE_CODE, $websiteCode);
    }

    /**
     * @inheritDoc
     */
    public function getStoreCode(): ?string
    {
        return $this->_get(self::STORE_CODE) !== null
            ? (string)$this->_get(self::STORE_CODE)
            : null;
    }

    /**
     * @inheritDoc
     */
    public function setStoreCode(?string $storeCode)
    {
        return $this->setData(self::STORE_CODE, $storeCode);
    }
}
