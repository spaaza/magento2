<?php

namespace Spaaza\Loyalty\Model\Data;

use Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class VoucherDistributionItem extends AbstractSimpleObject implements VoucherDistributionItemInterface
{
    /**
     * @inheritDoc
     */
    public function getItemBarcode(): string
    {
        return $this->_get(self::KEY_ITEM_BARCODE);
    }

    /**
     * @inheritDoc
     */
    public function getAmountTotal(): float
    {
        return $this->_get(self::KEY_AMOUNT_TOTAL);
    }

    /**
     * @inheritDoc
     */
    public function getQty(): float
    {
        return $this->_get(self::KEY_QTY);
    }

    /**
     * @inheritDoc
     */
    public function getRetailerItemCode(): ?string
    {
        $value = $this->_get(self::KEY_RETAILER_ITEM_CODE);
        return $value !== null ? (string)$value : null;
    }

    /**
     * @inheritDoc
     */
    public function setItemBarcode(string $itemBarcode): VoucherDistributionItemInterface
    {
        return $this->setData(self::KEY_ITEM_BARCODE, $itemBarcode);
    }

    /**
     * @inheritDoc
     */
    public function setAmountTotal(float $amount): VoucherDistributionItemInterface
    {
        return $this->setData(self::KEY_AMOUNT_TOTAL, $amount);
    }

    /**
     * @inheritDoc
     */
    public function setQty(float $qty): VoucherDistributionItemInterface
    {
        return $this->setData(self::KEY_QTY, $qty);
    }

    /**
     * @inheritDoc
     */
    public function setRetailerItemCode(?string $itemCode): VoucherDistributionItemInterface
    {
        return $this->setData(self::KEY_RETAILER_ITEM_CODE, $itemCode);
    }
}
