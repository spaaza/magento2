<?php

namespace Spaaza\Loyalty\Model\Data;

use Spaaza\Loyalty\Api\Data\VoucherInfoInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class VoucherInfo extends AbstractSimpleObject implements VoucherInfoInterface
{
    /**
     * @inheritDoc
     */
    public function getType()
    {
        return (string)$this->_get(self::KEY_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function setType(string $type)
    {
        return $this->setData(self::KEY_TYPE, $type);
    }

    /**
     * @inheritDoc
     */
    public function getKey()
    {
        return (string)$this->_get(self::KEY_KEY);
    }

    /**
     * @inheritDoc
     */
    public function setKey(string $key)
    {
        return $this->setData(self::KEY_KEY, $key);
    }

    /**
     * @inheritDoc
     */
    public function getAmount()
    {
        return (float)$this->_get(self::KEY_AMOUNT);
    }

    /**
     * @inheritDoc
     */
    public function setAmount(float $amount)
    {
        return $this->setData(self::KEY_AMOUNT, $amount);
    }

    /**
     * @inheritDoc
     */
    public function getLabel()
    {
        return (string)$this->_get(self::KEY_LABEL);
    }

    /**
     * @inheritDoc
     */
    public function setLabel(string $label)
    {
        return $this->setData(self::KEY_LABEL, $label);
    }
}
