<?php

namespace Spaaza\Loyalty\Model\Total\Creditmemo;

use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface;

class Voucher extends \Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal
{
    /**
     * @var BasketItemIdentifierProviderInterface
     */
    protected $basketItemIdentifierProvider;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\BasketPriceReturn
     */
    protected $basketPriceConnector;

    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    private $creditmemoSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $creditmemoSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        BasketItemIdentifierProviderInterface $basketItemIdentifierProvider,
        \Spaaza\Loyalty\Model\Connector\BasketPriceReturn $basketPriceConnector,
        array $data = []
    ) {
        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
        $this->creditmemoSpaazaDataManagement = $creditmemoSpaazaDataManagement;
        $this->orderRepository = $orderRepository;
        $this->basketItemIdentifierProvider = $basketItemIdentifierProvider;
        $this->basketPriceConnector = $basketPriceConnector;
        parent::__construct($data);
    }

    /**
     * Collect invoice Voucher total
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $order = $this->orderRepository->get($creditmemo->getOrderId());

        $creditmemoSpaazaData = $this->creditmemoSpaazaDataManagement->applyExtensionAttributes($creditmemo);
        $orderSpaazaData = $this->orderSpaazaDataManagement->applyExtensionAttributes($order);

        if ($orderSpaazaData->getVoucherAmount() > 0 && is_array($orderSpaazaData->getVoucherDistribution())) {
            $voucherDistribution = $orderSpaazaData->getVoucherDistribution();
            $voucherAmount = 0;
            foreach ($creditmemo->getAllItems() as $creditmemoItem) {
                if ($creditmemoItem->getQty() > 0 &&
                    empty($creditmemoItem->getOrderItem()->getParentItemId())
                    && is_array($voucherDistribution)
                ) {
                    $amountPerUnit = $this->getVoucherAmountPerUnit($creditmemoItem, $voucherDistribution);
                    $voucherAmount += $amountPerUnit * $creditmemoItem->getQty();
                }
            }

            // @todo Convert to base amount
            $baseVoucherAmount = $voucherAmount;

            // Set the used amount on the extension attribute
            $creditmemoSpaazaData->setVoucherAmount($voucherAmount);
            $creditmemoSpaazaData->setBaseVoucherAmount($baseVoucherAmount);

            // Decrease the invoice's grand total
            $creditmemo->setGrandTotal(max($creditmemo->getGrandTotal() - $voucherAmount, 0));
            $creditmemo->setBaseGrandTotal(max($creditmemo->getBaseGrandTotal() - $baseVoucherAmount, 0));
        }
        return parent::collect($creditmemo);
    }

    /**
     * Get the amount paid with Spaaza vouchers that will be returned in vouchers to the customer
     *
     * Unused at this moment. It could be used to determine the amount that will be refunded once the credit
     * memo has been created and sent to Spaaza. At this moment, we only save the refunded amount after a
     * successful add-basket call.
     *
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return float
     */
    public function getRefundVoucherAmount(\Magento\Sales\Model\Order\Creditmemo $creditmemo): float
    {
        $basketPrice = $this->basketPriceConnector->getBasketPriceForCreditmemo($creditmemo);
        $returnAmount = 0;
        foreach ($basketPrice['basket']['return_transactions'] ?? [] as $transaction) {
            foreach ($transaction['returned_items'] ?? [] as $item) {
                foreach ($item['voucher_distribution_refunds'] ?? [] as $voucher) {
                    $returnAmount += $voucher['amount'] ?? 0;
                }
            }
        }
        return (float)$returnAmount;
    }

    /**
     * Return the applied voucher amount per product, as it was saved in the order extension attribute
     *
     * @param CreditmemoItemInterface $creditmemoItem
     * @param \Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface[] $voucherDistribution
     * @return float
     */
    protected function getVoucherAmountPerUnit(
        CreditmemoItemInterface $creditmemoItem,
        array $voucherDistribution
    ): float {
        foreach ($voucherDistribution as $voucherDistributionItem) {
            $retailerItemCode = (int)$voucherDistributionItem->getRetailerItemCode();
            $itemMatches = false;
            if ($retailerItemCode) {
                $itemMatches = $retailerItemCode === (int)$creditmemoItem->getOrderItemId();
            } else {
                $identification = $this->basketItemIdentifierProvider->getIdentifierForCreditmemoItem($creditmemoItem);
                if ($identification && !empty($identification['item_barcode'])) {
                    $itemMatches = $identification['item_barcode'] == $voucherDistributionItem->getItemBarcode();
                }
            }
            if ($itemMatches && $voucherDistributionItem->getAmountTotal() > 0) {
                if ($voucherDistributionItem->getQty() === null) {
                    $qty = 1;
                } else {
                    $qty = $voucherDistributionItem->getQty();
                }
                if ($qty > 0) {
                    return $voucherDistributionItem->getAmountTotal() / $qty;
                }
            }
        }
        return 0;
    }
}
