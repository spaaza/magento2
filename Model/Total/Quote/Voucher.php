<?php

namespace Spaaza\Loyalty\Model\Total\Quote;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total as AddressTotal;

class Voucher extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * This is to prevent flooding the Spaaza servers with the same requests. If the cart contents didn't change,
     * the results of a get-basket-price call will be cached this number of seconds.
     */
    const RESULT_LIFETIME_SECONDS = 20;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\BasketPrice
     */
    protected $basketPriceConnector;

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\HashGenerator
     */
    protected $hashGenerator;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $dateTime;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherInfoSerializer
     */
    protected $voucherInfoSerializer;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherDistributionSerializer
     */
    protected $voucherDistributionSerializer;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $spaazaConfig;

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Model\VoucherManagement
     */
    private $voucherManagement;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Model\VoucherManagement $voucherManagement,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Spaaza\Loyalty\Model\Connector\BasketPrice $basketPriceConnector,
        \Spaaza\Loyalty\Model\QuoteAddress\HashGenerator $hashGenerator,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Spaaza\Loyalty\Model\VoucherInfoSerializer $voucherInfoSerializer,
        \Spaaza\Loyalty\Model\VoucherDistributionSerializer $voucherDistributionSerializer,
        \Spaaza\Loyalty\Model\Config $spaazaConfig
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->priceCurrency = $priceCurrency;
        $this->helper = $helper;
        $this->voucherManagement = $voucherManagement;
        $this->customerRepository = $customerRepository;
        $this->basketPriceConnector = $basketPriceConnector;
        $this->hashGenerator = $hashGenerator;
        $this->dateTime = $dateTime;
        $this->voucherInfoSerializer = $voucherInfoSerializer;
        $this->voucherDistributionSerializer = $voucherDistributionSerializer;
        $this->spaazaConfig = $spaazaConfig;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->helper->getVoucherTotalLabel();
    }

    /**
     * @api
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param AddressTotal $total
     * @return AddressTotal\AbstractTotal
     */
    public function collect(Quote $quote, ShippingAssignmentInterface $shippingAssignment, AddressTotal $total)
    {
        parent::collect($quote, $shippingAssignment, $total);

        if (!$quote->getCustomerId()) {
            return $this;
        }

        /** @var \Magento\Quote\Model\Quote\Address $address */
        $address = $shippingAssignment->getShipping()->getAddress();
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($address);

        if (empty($address->getAllItems())) {
            return $this;
        }

        try {
            $customer = $this->customerRepository->getById($quote->getCustomerId());
        } catch (NoSuchEntityException $e) {
            return $this;
        }

        $totalUsedVoucherAmount = 0;

        $hash = $this->hashGenerator->getHash($address);
		if($this->spaazaConfig->getVoucherTotalsCollapse()) {
			$resultExpired = $spaazaData->getLastRequestAt()
				< $this->dateTime->gmtDate(null, sprintf('-%d seconds', self::RESULT_LIFETIME_SECONDS));
		} else {
			$resultExpired = $spaazaData->getLastRequestAt()
				< $this->dateTime->gmtDate(null, sprintf('-%d seconds', 2));
		}

        if (!$this->spaazaConfig->isGetBasketPriceEnabled()) {
            $totalUsedVoucherAmount = 0;
            $spaazaData->setVouchers([]);
            $spaazaData->setVoucherDistribution([]);
            $spaazaData->setLastHash(null);
        } elseif ($hash !== $spaazaData->getLastHash() || $resultExpired) {
            try {
                $basketPrice = $this->basketPriceConnector->getBasketPrice($quote);
                $voucherInfos = [];
                if (!empty($basketPrice['basket'])) {
                    $voucherInfos = $this->voucherInfoSerializer->createFromSpaazaResponse(
                        $basketPrice['basket']
                    );
                }
                $spaazaData->setVouchers($voucherInfos);

                $voucherDistribution = [];
                if (!empty($basketPrice['basket']['basket_items'])) {
                    $voucherDistribution = $this->voucherDistributionSerializer->createFromSpaazaResponse(
                        $basketPrice['basket']['basket_items']
                    );
                }
                $spaazaData->setVoucherDistribution($voucherDistribution);

                $spaazaData->setLastHash($hash);
                $spaazaData->setLastRequestAt($this->dateTime->gmtDate());

                if ($customer->getExtensionAttributes()
                    && $customerSpaazaData = $customer->getExtensionAttributes()->getSpaazaData()
                ) {
                    $spaazaData->setUserId($customerSpaazaData->getUserId());
                    $spaazaData->setMemberNumber($customerSpaazaData->getMemberNumber());
                }
            } catch (\Exception $e) {
                $this->helper->debugLog($e);
            }
        }

        foreach ($spaazaData->getVouchers() as $voucherInfo) {
            $totalUsedVoucherAmount += $voucherInfo->getAmount();
        }

        $shippingAmount = $total->getShippingInclTax() - $total->getShippingDiscountAmount();
        $totalUsedVoucherAmount = min(
            $totalUsedVoucherAmount,
            max($total->getGrandTotal() - $shippingAmount, 0)
        );

        // @todo Convert to base amount
        $baseTotalUsedVoucherAmount = $totalUsedVoucherAmount;

        $spaazaData->setVoucherAmount($totalUsedVoucherAmount);
        $spaazaData->setBaseVoucherAmount($baseTotalUsedVoucherAmount);

        $total->setTotalAmount(
            \Spaaza\Loyalty\Helper\Data::TOTAL_CODE,
            -1 * round($spaazaData->getVoucherAmount(), 2)
        );

        $total->setBaseTotalAmount(
            \Spaaza\Loyalty\Helper\Data::TOTAL_CODE,
            -1 * round($spaazaData->getBaseVoucherAmount(), 2)
        );

        $total->setGrandTotal($total->getGrandTotal() - $totalUsedVoucherAmount);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() - $baseTotalUsedVoucherAmount);

        if ($address->getId() && !$address->isDeleted()) {
            $spaazaData->setQuoteAddressId($address->getId());
            $this->spaazaDataManagement->save($spaazaData);
        }

        return $this;
    }

    /**
     * @param Quote $quote
     * @param AddressTotal $total
     * @return array
     */
    public function fetch(Quote $quote, AddressTotal $total)
    {
        $value = $i = 0;
        $items = [];
        foreach ($quote->getAllAddresses() as $address) {
            $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($address);
            foreach ($spaazaData->getVouchers() as $voucherInfo) {
				// we check if the title has a length > 0 because otherwise the javascript may fail
                if($voucherInfo->getAmount() > 0 && strlen($voucherInfo->getLabel()) > 0) {
                    $items [] = [
                        'title' => $voucherInfo->getLabel(),
                        'value' => -1 * $voucherInfo->getAmount()
                    ];
                    $i++;
                }
            }
            $extensionAttributes = $address->getExtensionAttributes();
            if ($extensionAttributes && $extensionAttributes->getSpaazaData()) {
                $spaazaDataVoucherAmount = $extensionAttributes->getSpaazaData()->getVoucherAmount();
                $value += $spaazaDataVoucherAmount;
            }
        }

        if($i > 1 && $this->spaazaConfig->getVoucherTotalsCollapse()) {
            // in this case we need the value calculated earlier (the total value)
            $items = [
             [
                'title' => __($this->getLabel()),
                'value' => -1 * $value,
                 ]
            ];
        }

        return [
            'code' => \Spaaza\Loyalty\Helper\Data::TOTAL_CODE,
            'title' => __($this->getLabel()),
            'value' => $items,
            'full_info' => [],
        ];
    }
}
