<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

class SpaazaConfigProvider implements InfoProviderInterface
{
    /**
     * @var \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory
     */
    protected $configCollectionFactory;

    public function __construct(
        \Magento\Config\Model\ResourceModel\Config\Data\CollectionFactory $configCollectionFactory
    ) {
        $this->configCollectionFactory = $configCollectionFactory;
    }

    public function getContents(): ?string
    {
        /** @var \Magento\Config\Model\ResourceModel\Config\Data\Collection $collection */
        $collection = $this->configCollectionFactory->create();
        $collection->addPathFilter('spaaza_loyalty');
        $output = [];
        foreach ($collection->getItems() as $item) {
            $output[] = [
                'scope' => $item->getData('scope'),
                'scope_id' => (int)$item->getData('scope_id'),
                'path' => $item->getData('path'),
                'value' => $item->getData('value'),
            ];
        }
        return json_encode($output, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public function getFilename(): string
    {
        return 'spaaza-config.json';
    }
}
