<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

interface InfoProviderInterface
{
    public function getContents(): ?string;

    public function getFilename(): string;
}
