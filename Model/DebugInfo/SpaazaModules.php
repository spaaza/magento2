<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

class SpaazaModules
{
    /**
     * @var \Magento\Framework\Module\FullModuleList
     */
    protected $fullModuleList;

    /**
     * @var \Magento\Framework\Module\ModuleList
     */
    protected $enabledModuleList;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Module\FullModuleList $fullModuleList,
        \Magento\Framework\Module\ModuleList $moduleList,
        array $data = []
    ) {
        $this->fullModuleList = $fullModuleList;
        $this->enabledModuleList = $moduleList;
    }

    /**
     * Returns module names and versions
     *
     * @return array Array with keys 'name', 'version' and 'enabled'
     */
    public function getModulesAndVersions(): array
    {
        $moduleNames = $this->fullModuleList->getNames();
        $result = [];
        foreach ($moduleNames as $moduleName) {
            if (preg_match('/^Spaaza_/', $moduleName)) {
                $module = $this->fullModuleList->getOne($moduleName);
                $result[] = [
                    'name' => $moduleName,
                    'version' => $module['setup_version'] ?? null,
                    'enabled' => $this->enabledModuleList->has($moduleName),
                ];
            }
        }
        return $result;
    }

    public function getClientVersion(): ?string
    {
        if (class_exists(\Spaaza\Client::class)) {
            return \Spaaza\Client::VERSION;
        }
        return null;
    }
}
