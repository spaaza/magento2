<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

use Magento\Framework\Filesystem\DirectoryList;

class Bundler
{
    /**
     * @var InfoProviderInterface[]
     */
    protected $providers;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $filesystem;

    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        array $providers = []
    ) {
        $this->providers = $providers;
        $this->filesystem = $filesystem;
    }

    /**
     * Create the zip file and return the filename
     *
     * @return string  Returns the basename in the SYS_TMP directory
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    public function createZipFile(): string
    {
        $writeDir = $this->filesystem->getDirectoryWrite(DirectoryList::SYS_TMP);
        $zipArchive = new \ZipArchive();
        $filename = tempnam($writeDir->getAbsolutePath(), 'spaaza-') . '.zip';
        if ($zipArchive->open($filename, \ZipArchive::CREATE) !== true) {
            throw new \RuntimeException('Cannot create ZIP file');
        }
        foreach ($this->providers as $provider) {
            $contents = $provider->getContents();
            if ($contents !== null) {
                $zipArchive->addFromString(
                    $provider->getFilename(),
                    $provider->getContents()
                );
            }
        }
        $zipArchive->close();
        return basename($filename);
    }
}
