<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

class RequestsProvider implements InfoProviderInterface
{
    const LIMIT = 2000;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    public function getContents(): string
    {
        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from($connection->getTableName('spaaza_client_request'))
            ->order('request_id DESC')
            ->limit(self::LIMIT);
        return json_encode($connection->fetchAll($select));
    }

    public function getFilename(): string
    {
        return 'requests.json';
    }
}
