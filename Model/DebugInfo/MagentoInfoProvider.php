<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

class MagentoInfoProvider implements InfoProviderInterface
{
    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $magentoMetadata;

    /**
     * @var SpaazaModules
     */
    protected $spaazaModules;

    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $magentoMetadata,
        SpaazaModules $spaazaModules
    ) {
        $this->magentoMetadata = $magentoMetadata;
        $this->spaazaModules = $spaazaModules;
    }

    public function getContents(): ?string
    {
        $contents = [
            'product' => [
                'version' => $this->magentoMetadata->getVersion(),
                'name' => $this->magentoMetadata->getName(),
                'edition' => $this->magentoMetadata->getEdition(),
            ],
            'spaaza' => [
                'modules' => $this->spaazaModules->getModulesAndVersions(),
                'client' => [
                    'version' => $this->spaazaModules->getClientVersion(),
                ]
            ]
        ];
        return json_encode($contents, JSON_PRETTY_PRINT);
    }

    public function getFilename(): string
    {
        return 'magento-info.json';
    }
}
