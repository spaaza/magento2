<?php

namespace Spaaza\Loyalty\Model\DebugInfo;

class LogfileProvider implements InfoProviderInterface
{
    const LIMIT_BYTES = 3 * 1024 * 1024;

    /**
     * @var \Monolog\Handler\HandlerInterface
     */
    protected $logHandler;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    protected $fileSystem;

    public function __construct(
        \Monolog\Handler\HandlerInterface $logHandler,
        \Magento\Framework\Filesystem\Driver\File $fileSystem
    ) {
        $this->logHandler = $logHandler;
        $this->fileSystem = $fileSystem;
    }

    public function getContents(): ?string
    {
        $result = null;
        if ($this->logHandler instanceof \Magento\Framework\Logger\Handler\Base) {
            $path = $this->logHandler->getUrl();
            if ($this->fileSystem->isExists($path)) {
                $resource = $this->fileSystem->fileOpen($path, 'r');
                $this->fileSystem->fileSeek($resource, 0, SEEK_END);

                $startPosition = max($this->fileSystem->fileTell($resource) - self::LIMIT_BYTES, 0);
                $this->fileSystem->fileSeek($resource, $startPosition);
                while (!$this->fileSystem->endOfFile($resource)) {
                    $result .= $this->fileSystem->fileRead($resource, 4096);
                }
            }
        }
        return $result;
    }

    public function getFilename(): string
    {
        return 'spaaza.log';
    }
}
