<?php

namespace Spaaza\Loyalty\Model;

use Magento\Customer\Api\Data\CustomerInterface as CustomerData;
use Magento\Quote\Api\Data\CartInterface;
use Spaaza\Loyalty\Api\Data\VoucherInterface;

class VoucherManagement
{
    /**
     * @var \Spaaza\Loyalty\Model\Connector\Identifier\LockingCodeProvider
     */
    protected $lockingCodeProvider;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    private $customerSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Customer
     */
    private $customerConnector;

    /**
     * @var \Spaaza\Loyalty\Api\Data\VoucherInterfaceFactory
     */
    private $voucherFactory;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Voucher
     */
    private $voucherConnector;

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    private $quoteAddressSpaazaDataManagement;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    public function __construct(
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $customerSpaazaDataManagement,
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $quoteAddressSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Connector\Customer $customerConnector,
        \Spaaza\Loyalty\Model\Connector\Voucher $voucherConnector,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Spaaza\Loyalty\Api\Data\VoucherInterfaceFactory $voucherFactory,
        Connector\Identifier\LockingCodeProvider $lockingCodeProvider
    ) {
        $this->customerSpaazaDataManagement = $customerSpaazaDataManagement;
        $this->customerConnector = $customerConnector;
        $this->voucherFactory = $voucherFactory;
        $this->voucherConnector = $voucherConnector;
        $this->quoteAddressSpaazaDataManagement = $quoteAddressSpaazaDataManagement;
        $this->cartRepository = $cartRepository;
        $this->lockingCodeProvider = $lockingCodeProvider;
    }

    /**
     * Get a voucher data model for the given customer
     *
     * @param string $voucherKey
     * @param CustomerData $customer
     * @return null|VoucherInterface
     */
    public function getVoucher($voucherKey, CustomerData $customer)
    {
        $vouchers = $this->getAvailableVouchers($customer);
        foreach ($vouchers as $voucher) {
            if ($voucher->getVoucherKey() == $voucherKey) {
                return $voucher;
            }
        }
        return null;
    }

    /**
     * Create a voucher data models from data returned by get-card
     *
     * @param array $cardData
     * @return VoucherInterface[]
     */
    protected function createVouchersFromCardData($cardData)
    {
        $vouchers = [];
        if (!empty($cardData['basket_vouchers']) && is_array($cardData['basket_vouchers'])) {
            foreach ($cardData['basket_vouchers'] as $voucherData) {
                $vouchers[] = $this->voucherFactory->create(['data' => $voucherData]);
            }
        }
        if (!empty($cardData['honour_vouchers']) && is_array($cardData['honour_vouchers'])) {
            foreach ($cardData['honour_vouchers'] as $voucherData) {
                $vouchers[] = $this->voucherFactory->create(['data' => $voucherData]);
            }
        }
        return $vouchers;
    }

    /**
     * Can a voucher be spent?
     *
     * @api
     * @param VoucherInterface $voucher
     * @param CartInterface|null $quote  The quote the voucher must be spendable on
     * @return bool
     */
    public function canSpend(VoucherInterface $voucher, CartInterface $quote = null)
    {
        if (!$this->isClaimed($voucher)) {
            return false;
        }
        if ($voucher->getVoucherLocked()) {
            if ($voucher->getVoucherLockingCode()) {
                if (!$quote || !$quote->getId()) {
                    // We cannot guarantee that it can be spent
                    return false;
                }
                // If this voucher has a locking code, it MUST match the quote's locking code
                $lockingCode = $this->lockingCodeProvider->getLockingCodeForCart($quote->getId());
                if ($lockingCode != $voucher->getVoucherLockingCode()) {
                    return false;
                }
            } elseif ($voucher->getVoucherBasketOwnerCodeExclusive()) {
                if (!$quote) {
                    // We cannot guarantee that it can be spent
                    return false;
                }
                // If it has no locking code AND has a basket code, this basket code MUST match the quote id
                if ($quote->getId() != $voucher->getVoucherBasketOwnerCodeExclusive()) {
                    return false;
                }
            }
            // The voucher has not been locked to any particular basket
            return true;
        }
        // The voucher has not been locked
        return true;
    }

    /**
     * Is a voucher claimed?
     *
     * @param VoucherInterface $voucher
     * @return bool
     */
    public function isClaimed(VoucherInterface $voucher)
    {
        return $voucher->getVoucherStatus() == VoucherInterface::STATUS_CLAIMED;
    }

    /**
     * Can a voucher be claimed?
     *
     * @param VoucherInterface $voucher
     * @return bool
     */
    public function canClaim(VoucherInterface $voucher)
    {
        return $voucher->getVoucherStatus() == VoucherInterface::STATUS_GENERATED
            && !$voucher->getVoucherLocked();
    }

    /**
     * Claim a voucher
     *
     * @param VoucherInterface $voucher
     * @param CustomerData $customer
     * @param CartInterface|null $cart  If supplied, will collect totals and save the cart (quote)
     * @return bool
     */
    public function claimVoucher(VoucherInterface $voucher, CustomerData $customer, CartInterface $cart = null)
    {
        if ($this->canClaim($voucher)) {
            $this->voucherConnector->claimVoucher($voucher->getVoucherKey(), $customer);
            if ($cart) {
                $this->cartRepository->save($cart);
            }
            return true;
        }
        return false;
    }

    /**
     * Unclaim a voucher
     *
     * @param VoucherInterface $voucher
     * @param CustomerData $customer
     * @param CartInterface|null $cart  If supplied, will collect totals and save the cart (quote)
     * @return bool
     */
    public function unclaimVoucher(VoucherInterface $voucher, CustomerData $customer, CartInterface $cart = null)
    {
        if ($this->isClaimed($voucher)) {
            $result = $this->voucherConnector->unclaimVoucher($voucher->getVoucherKey(), $customer);
            if ($cart) {
                $this->cartRepository->save($cart);
            }
            return true;
        }
        return false;
    }

    /**
     * Lock a voucher
     *
     * @param VoucherInterface $voucher
     * @param CustomerData $customer
     * @return bool
     */
    public function lockVoucher(VoucherInterface $voucher, CustomerData $customer)
    {
        if (!$voucher->getVoucherLocked()) {
            $this->voucherConnector->lockVoucher($voucher->getVoucherKey(), $customer);
        }
        return true;
    }

    /**
     * Get the available vouchers for a customer
     *
     * Beware: this does not do any filtering, so unclaimed and locked vouchers
     * can also be returned.
     *
     * @param CustomerData $customer
     * @param bool $includeBasketCampaignVouchers  Also include basket campaign vouchers?
     * @return VoucherInterface[]
     */
    public function getAvailableVouchers(CustomerData $customer, bool $includeBasketCampaignVouchers = false)
    {
        try {
            $this->customerSpaazaDataManagement->applyExtensionAttributes($customer);
            if ($customer->getExtensionAttributes()->getSpaazaData()->getUserId()) {
                $cardData = $this->customerConnector->getCard($customer);
                if ($cardData) {
                    $vouchers = $this->createVouchersFromCardData($cardData);
                    if (!$includeBasketCampaignVouchers) {
                        foreach ($vouchers as $key => $voucher) {
                            if ($voucher->getCampaignType() === $voucher::CAMPAIGN_TYPE_BASKET) {
                                unset($vouchers[$key]);
                            }
                        }
                    }
                    return array_values($vouchers);
                }
            }
        } catch (\Exception $e) {
            return [];
        }
        return [];
    }

    /**
     * Get the voucher keys of the used vouchers in a quote
     *
     * @param CartInterface $cart
     * @return string[]
     */
    public function getUsedVoucherKeysInQuote(CartInterface $cart)
    {
        if ($cart->getIsVirtual()) {
            $address = $cart->getBillingAddress();
        } else {
            $address = $cart->getShippingAddress();
        }
        if ($address) {
            $spaazaData = $this->quoteAddressSpaazaDataManagement->applyExtensionAttributes($address);
            if (is_array($spaazaData->getVouchers())) {
                return array_keys($spaazaData->getVouchers());
            }
        }
        return [];
    }
}
