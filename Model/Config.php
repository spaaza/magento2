<?php

namespace Spaaza\Loyalty\Model;

use Magento\Store\Model\ScopeInterface;

class Config
{
    const XML_PATH_SYNCHRONOUS_SYNC_ENABLED = 'spaaza_loyalty/general/synchronous_sync_enabled';
    const XML_PATH_ASYNCHRONOUS_SYNC_ENABLED = 'spaaza_loyalty/general/asynchronous_sync_enabled';
    const XML_PATH_IS_PROMOTIONAL = 'spaaza_loyalty/general/is_promotional_attribute';
    const XML_PATH_DEBUG_LOG_ENABLED = 'spaaza_loyalty/general/debug_log_enabled';
    const XML_PATH_CHAIN_ID = 'spaaza_loyalty/client/chain_id';
    const XML_PATH_BRANCH_BUSINESS_OWNER_CODE = 'spaaza_loyalty/client/branch_business_owner_code';
    const XML_PATH_VOUCHERS_TOTAL_SORTORDER = 'sales/totals_sort/total_label';
    const XML_PATH_VOUCHERS_TOTAL_LABEL = 'spaaza_loyalty/vouchers/total_label';
    const XML_PATH_VOUCHERS_TOTAL_SHOW_ZERO = 'spaaza_loyalty/vouchers/total_show_zero';
    const XML_PATH_VOUCHERS_LOCK_PERIOD = 'spaaza_loyalty/vouchers/lock_period_seconds';
    const XML_PATH_QUEUE_MAX_ATTEMPTS = 'spaaza_loyalty/requests_queue/max_attempts';
    const XML_PATH_GET_BASKET_PRICE_ENABLED = 'spaaza_loyalty/general/get_basket_price_enabled';
    const XML_PATH_ANONYMOUS_GET_BASKET_PRICE_ENABLED = 'spaaza_loyalty/general/anonymous_get_basket_price_enabled';
    const XML_PATH_REQUESTS_LOG_ENABLED = 'spaaza_loyalty/general/requests_log_enabled';
    const XML_PATH_ENVIRONMENT_PREFIX = 'spaaza_loyalty/identification/environment_prefix';
    const XML_PATH_VOUCHERS_TOTAL_COLLAPSE = 'spaaza_loyalty/vouchers/collapse_totals';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManager
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManager $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Is synchronous sync enabled?
     *
     * @return bool
     */
    public function isSynchronousSyncEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_SYNCHRONOUS_SYNC_ENABLED);
    }

    /**
     * Is asynchronous sync enabled?
     *
     * @deprecated The setting has been removed as it was confusing and was checked way too little anyway
     * @return bool
     */
    public function isAsynchronousSyncEnabled(): bool
    {
        return true;
    }

    /**
     * Is sync enabled at all?
     *
     * At this time, sync is always enabled
     *
     * @return bool
     */
    public function isSyncEnabled(): bool
    {
        return true;
    }

    /**
     * Get the is promotional attribute
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return string|null
     */
    public function getIsPromotionalAttribute($store = null): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_IS_PROMOTIONAL,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Is the debug log enabled?
     *
     * @return bool
     */
    public function isDebugLogEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_DEBUG_LOG_ENABLED);
    }

    /**
     * Is 'debug requests' enabled?
     *
     * @return bool
     */
    public function isDebugRequestsEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_REQUESTS_LOG_ENABLED);
    }

    /**
     * Get the configured chain id for a website
     *
     * @param int|null $websiteId  If not filled, use the current website
     * @return int
     */
    public function getChainId(?int $websiteId = null): int
    {
        return (int)$this->scopeConfig->getValue(
            self::XML_PATH_CHAIN_ID,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteId
        );
    }

    /**
     * Get the configured chain id for a store
     *
     * @param int|null $storeId  If not filled, use the current website
     * @return int
     */
    public function getChainIdForStore(?int $storeId = null): int
    {
        $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
        return $this->getChainId($websiteId);
    }

    /**
     * Get the configured branch business owner code for a store
     *
     * Beware that this setting is saved on website level.
     *
     * @param int|null $storeId
     * @return string|null
     */
    public function getBranchBusinessOwnerCodeForStore(?int $storeId = null): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_BRANCH_BUSINESS_OWNER_CODE,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get a map with the chain id as key and website id as value
     *
     * @return array
     */
    public function getChainWebsiteMap(): array
    {
        $websites = $this->storeManager->getWebsites();
        $defaultStoreId = $this->storeManager->getDefaultStoreView()->getId();
        $map = [];
        foreach ($websites as $website) {
            $chainId = $this->getChainId($website->getId());
            // If there is no default store configured, we cannot use this website
            if (!$website->getDefaultStore()) {
                continue;
            }
            if (!isset($map[$chainId]) || in_array($defaultStoreId, $website->getStoreIds())) {
                // Don't overwrite an already mapped chain _unless_ the website contains the global default store
                $map[$chainId] = $website->getId();
            }
        }
        return $map;
    }

    /**
     * Get the sort order to use for displaying the vouchers total
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return int
     */
    public function getVoucherTotalSortOrder($store = null): int
    {
        return (int)$this->scopeConfig->getValue(
            self::XML_PATH_VOUCHERS_TOTAL_SORTORDER,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get the label to use for the vouchers total
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return string|null
     */
    public function getVoucherTotalLabel($store = null): ?string
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_VOUCHERS_TOTAL_LABEL,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    public function getVoucherTotalsCollapse($store = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_VOUCHERS_TOTAL_COLLAPSE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get the maximum attempts for a request in the queue before hitting failed status
     *
     * @return int
     */
    public function getRequestMaxAttempts(): int
    {
        return max((int)$this->scopeConfig->getValue(self::XML_PATH_QUEUE_MAX_ATTEMPTS), 1);
    }

    /**
     * Show the Vouchers total if it's 0?
     *
     * @param null|string|bool|int|\Magento\Store\Api\Data\StoreInterface $store
     * @return bool
     */
    public function showVoucherZeroTotal($store = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_VOUCHERS_TOTAL_SHOW_ZERO,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get the number of seconds to lock a voucher
     *
     * @return int
     */
    public function getVoucherLockPeriod(): int
    {
        return (int)max($this->scopeConfig->getValue(self::XML_PATH_VOUCHERS_LOCK_PERIOD), 120);
    }

    /**
     * Is 'get basket price' enabled?
     *
     * @return bool
     */
    public function isGetBasketPriceEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_GET_BASKET_PRICE_ENABLED);
    }

    /**
     * Get the prefix to use where environment specific prefixing is needed
     *
     * @see \Spaaza\Loyalty\Model\Connector\Identifier\CustomerIdProvider::getAuthenticationPointIdentifier()
     * @return string|null
     */
    public function getEnvironmentPrefix(): ?string
    {
        return $this->scopeConfig->getValue(self::XML_PATH_ENVIRONMENT_PREFIX);
    }

    /**
     * Is anonymous cart sending enabled?
     *
     * @return bool
     */
    public function isAnonymousGetBasketPriceEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(self::XML_PATH_ANONYMOUS_GET_BASKET_PRICE_ENABLED);
    }
}
