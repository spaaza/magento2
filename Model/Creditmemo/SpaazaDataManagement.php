<?php

namespace Spaaza\Loyalty\Model\Creditmemo;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Spaaza\Loyalty\Api\Data\Creditmemo\SpaazaDataInterface;

class SpaazaDataManagement
{
    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Api\Data\Creditmemo\SpaazaDataInterfaceFactory
     */
    private $spaazaDataFactory;

    /**
     * @var \Magento\Sales\Api\Data\CreditmemoExtensionFactory
     */
    private $creditmemoExtensionFactory;

    /**
     * @var \Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData
     */
    private $spaazaDataResourceModel;

    /**
     * SpaazaDataManagement constructor.
     *
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Spaaza\Loyalty\Api\Data\Creditmemo\SpaazaDataInterfaceFactory $spaazaDataFactory
     * @param \Magento\Sales\Api\Data\CreditmemoExtensionFactory $creditmemoExtensionFactory
     * @param \Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData $spaazaDataResourceModel
     */
    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Api\Data\Creditmemo\SpaazaDataInterfaceFactory $spaazaDataFactory,
        \Magento\Sales\Api\Data\CreditmemoExtensionFactory $creditmemoExtensionFactory,
        \Spaaza\Loyalty\Model\ResourceModel\Creditmemo\SpaazaData $spaazaDataResourceModel
    ) {
        $this->helper = $helper;
        $this->spaazaDataFactory = $spaazaDataFactory;
        $this->creditmemoExtensionFactory = $creditmemoExtensionFactory;
        $this->spaazaDataResourceModel = $spaazaDataResourceModel;
    }

    /**
     * Ensures existing extension attributes and (loaded) Spaaza data
     *
     * @param CreditmemoInterface $creditmemo
     * @return SpaazaDataInterface
     */
    public function applyExtensionAttributes(CreditmemoInterface $creditmemo)
    {
        $extensionAttributes = $creditmemo->getExtensionAttributes();
        if (!$extensionAttributes) {
            $extensionAttributes = $this->creditmemoExtensionFactory->create();
            $this->helper->debugLog(
                'Added extension attributes object to creditmemo',
                ['creditmemo' => $creditmemo->getEntityId() ?: 'new']
            );
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            if ($creditmemo->getEntityId()) {
                try {
                    $spaazaData = $this->getByCreditmemoId($creditmemo->getEntityId());
                    $this->helper->debugLog(
                        'Got extension attribute from database',
                        ['creditmemo' => $creditmemo->getEntityId()]
                    );
                } catch (NoSuchEntityException $e) {
                    $spaazaData = $this->spaazaDataFactory->create();
                    $spaazaData->setCreditmemoId($creditmemo->getEntityId());
                    $this->helper->debugLog(
                        'Created a new extension attribute for existing creditmemo',
                        ['creditmemo' => $creditmemo->getEntityId()]
                    );
                }
            } else {
                $spaazaData = $this->spaazaDataFactory->create();
                $this->helper->debugLog('Created a new extension attribute for new creditmemo');
            }
        }

        $extensionAttributes->setSpaazaData($spaazaData);
        $creditmemo->setExtensionAttributes($extensionAttributes);

        return $spaazaData;
    }

    /**
     * Retrieve Creditmemo SpaazaData By Creditmemo Id
     *
     * @param int $creditmemoId
     * @return SpaazaDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByCreditmemoId($creditmemoId)
    {
        $spaazaData = $this->spaazaDataFactory->create();
        $this->spaazaDataResourceModel->load($spaazaData, $creditmemoId);
        if (!$spaazaData->getCreditmemoId()) {
            throw new NoSuchEntityException(__('SpaazaData for creditmemo id "%1" does not exist.', $creditmemoId));
        }
        return $spaazaData;
    }

    /**
     * Save Creditmemo SpaazaData
     *
     * @param SpaazaDataInterface $data
     * @return SpaazaDataInterface
     * @throws CouldNotSaveException
     */
    public function save(SpaazaDataInterface $data)
    {
        if (!$data->getCreditmemoId()) {
            throw new CouldNotSaveException(__('Cannot save creditmemo Spaaza data without an creditmemo id'));
        }
        try {
            $this->helper->debugLog(
                'Save extension attribute',
                ['creditmemo' => $data->getCreditmemoId()]
            );
            $this->spaazaDataResourceModel->save($data);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Could not save creditmemo Spaaza data: %1', $exception->getMessage()));
        }
        return $data;
    }
}
