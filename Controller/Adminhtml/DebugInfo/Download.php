<?php

namespace Spaaza\Loyalty\Controller\Adminhtml\DebugInfo;

class Download extends \Magento\Backend\App\Action
{
    /**
     * @var \Spaaza\Loyalty\Model\DebugInfo\Bundler
     */
    protected $debugInfoBundler;

    /**
     * @var \Magento\Backend\App\Response\Http\FileFactory
     */
    protected $fileResponseFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectResultFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Spaaza\Loyalty\Model\DebugInfo\Bundler $debugInfoBundler,
        \Magento\Backend\App\Response\Http\FileFactory $fileResponseFactory,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectResultFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->debugInfoBundler = $debugInfoBundler;
        $this->fileResponseFactory = $fileResponseFactory;
        $this->redirectResultFactory = $redirectResultFactory;
        $this->messageManager = $messageManager;
    }

    public function execute()
    {
        try {
            $filename = $this->debugInfoBundler->createZipFile();
            return $this->fileResponseFactory->create(
                'spaaza-debuginfo.zip',
                [
                    'type' => 'filename',
                    'value' => $filename,
                ],
                \Magento\Framework\Filesystem\DirectoryList::SYS_TMP,
                'application/zip'
            );
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(
                __('Something went wrong while creating the zipfile: %s', $exception->getMessage())
            );
            return $this->redirectResultFactory->create()
                ->setRefererUrl();
        }
    }
}
