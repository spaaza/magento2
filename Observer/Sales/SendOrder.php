<?php

namespace Spaaza\Loyalty\Observer\Sales;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class SendOrder
 *
 * @event sales_order_invoice_pay
 */
class SendOrder implements ObserverInterface
{

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Order
     */
    private $orderConnector;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Model\Connector\Order $orderConnector,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->orderConnector = $orderConnector;
        $this->helper = $helper;
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $invoice */
        $invoice = $observer->getEvent()->getData('invoice');

        try {
            $orderId = $invoice->getOrderId();
            $order = $this->orderRepository->get($orderId);

            $this->orderConnector->sendOrder($order);
        } catch (\Exception $e) {
            $this->logger->error($e);
        }
    }
}
