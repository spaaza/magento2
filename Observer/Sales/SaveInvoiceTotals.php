<?php

namespace Spaaza\Loyalty\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Sales\Api\Data\InvoiceInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class SaveInvoiceTotals
 *
 * @event sales_order_invoice_save_after
 */
class SaveInvoiceTotals implements ObserverInterface
{
    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement
     */
    private $invoiceSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    public function __construct(
        \Spaaza\Loyalty\Model\Invoice\SpaazaDataManagement $invoiceSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {

        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
        $this->invoiceSpaazaDataManagement = $invoiceSpaazaDataManagement;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Save the Spaaza invoice totals
     *
     * @param EventObserver $observer
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function execute(EventObserver $observer)
    {
        /** @var \Magento\Sales\Model\Order\Invoice $invoice */
        $invoice = $observer->getEvent()->getData('invoice');
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderRepository->get($invoice->getOrderId());

        if (!$invoice->getExtensionAttributes() || !$invoice->getExtensionAttributes()->getSpaazaData()) {
            return;
        }
        if (!$invoice->getId()) {
            return;
        }
        if (!$order->getId()) {
            return;
        }

        $this->increaseOrderInvoicedAmount($invoice, $order);
        $this->saveTotals($invoice, $order);
    }

    /**
     * Increase the invoiced amount on the order object
     *
     * @param InvoiceInterface $invoice
     * @param OrderInterface $order
     * @return void
     */
    private function increaseOrderInvoicedAmount(InvoiceInterface $invoice, OrderInterface $order)
    {
        $spaazaData = $invoice->getExtensionAttributes()->getSpaazaData();
        if ($spaazaData->getVoucherAmount() > 0) {
            $orderSpaazaData = $this->orderSpaazaDataManagement->applyExtensionAttributes($order);

            $orderSpaazaData->setVoucherAmountInvoiced(
                $orderSpaazaData->getVoucherAmountInvoiced() + $spaazaData->getVoucherAmount()
            );
            $orderSpaazaData->setBaseVoucherAmountInvoiced(
                $orderSpaazaData->getBaseVoucherAmountInvoiced() + $spaazaData->getBaseVoucherAmount()
            );
        }
    }

    /**
     * Save the invoice and order totals
     *
     * This expects the invoice to have loaded extension attributes, order does not need
     * to have extension attributes loaded. If it has not, the order will be ignored.
     *
     * @param InvoiceInterface $invoice
     * @param OrderInterface $order
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    private function saveTotals(InvoiceInterface $invoice, OrderInterface $order)
    {
        $invoiceSpaazaData = $invoice->getExtensionAttributes()->getSpaazaData();
        $invoiceSpaazaData->setInvoiceId($invoice->getEntityId());
        $this->invoiceSpaazaDataManagement->save($invoiceSpaazaData);

        if ($order->getExtensionAttributes() && $order->getExtensionAttributes()->getSpaazaData()) {
            $orderSpaazaData = $order->getExtensionAttributes()->getSpaazaData();
            $orderSpaazaData->setOrderId($order->getEntityId());
            $this->orderSpaazaDataManagement->save($order->getExtensionAttributes()->getSpaazaData());
        }
    }
}
