<?php

namespace Spaaza\Loyalty\Observer\Sales;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class SaveCreditmemoTotals
 *
 * @event sales_order_creditmemo_save_after
 */
class SaveCreditmemoTotals implements ObserverInterface
{
    /**
     * @var \Spaaza\Loyalty\Model\Order\SpaazaDataManagement
     */
    private $orderSpaazaDataManagement;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    private $creditmemoSpaazaDataManagement;

    /**
     * SaveCreditmemoTotals constructor.
     *
     * @param \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $creditmemoSpaazaDataManagement
     * @param \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $creditmemoSpaazaDataManagement,
        \Spaaza\Loyalty\Model\Order\SpaazaDataManagement $orderSpaazaDataManagement,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {

        $this->orderSpaazaDataManagement = $orderSpaazaDataManagement;
        $this->orderRepository = $orderRepository;
        $this->creditmemoSpaazaDataManagement = $creditmemoSpaazaDataManagement;
    }

    /**
     * Save the Spaaza credit memo totals
     *
     * @param EventObserver $observer
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function execute(EventObserver $observer)
    {
        /** @var \Magento\Sales\Model\Order\Creditmemo $creditmemo */
        $creditmemo = $observer->getEvent()->getData('creditmemo');
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderRepository->get($creditmemo->getOrderId());

        if (!$creditmemo->getExtensionAttributes() || !$creditmemo->getExtensionAttributes()->getSpaazaData()) {
            return;
        }
        if (!$creditmemo->getId()) {
            return;
        }
        if (!$order->getId()) {
            return;
        }

        $this->increaseOrderRefundedAmount($creditmemo, $order);
        $this->saveTotals($creditmemo, $order);
    }

    /**
     * Increase the refunded amount on the order object
     *
     * @param CreditmemoInterface $creditmemo
     * @param OrderInterface $order
     * @return void
     */
    private function increaseOrderRefundedAmount(CreditmemoInterface $creditmemo, OrderInterface $order)
    {
        $spaazaData = $creditmemo->getExtensionAttributes()->getSpaazaData();
        if ($spaazaData->getVoucherAmount() > 0) {
            $orderSpaazaData = $this->orderSpaazaDataManagement->applyExtensionAttributes($order);

            $orderSpaazaData->setVoucherAmountRefunded(
                $orderSpaazaData->getVoucherAmountRefunded() + $spaazaData->getVoucherAmount()
            );
            $orderSpaazaData->setBaseVoucherAmountRefunded(
                $orderSpaazaData->getBaseVoucherAmountRefunded() + $spaazaData->getBaseVoucherAmount()
            );
        }
    }

    /**
     * Save the credit memo and order totals
     *
     * This expects the credit memo to have loaded extension attributes, order does not need
     * to have extension attributes loaded. If it has not, the order will be ignored.
     *
     * @param CreditmemoInterface $creditmemo
     * @param OrderInterface $order
     * @return void
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    private function saveTotals(CreditmemoInterface $creditmemo, OrderInterface $order)
    {
        $creditmemoSpaazaData = $creditmemo->getExtensionAttributes()->getSpaazaData();
        $creditmemoSpaazaData->setCreditmemoId($creditmemo->getEntityId());
        $this->creditmemoSpaazaDataManagement->save($creditmemoSpaazaData);

        if ($order->getExtensionAttributes() && $order->getExtensionAttributes()->getSpaazaData()) {
            $orderSpaazaData = $order->getExtensionAttributes()->getSpaazaData();
            $orderSpaazaData->setOrderId($order->getEntityId());
            $this->orderSpaazaDataManagement->save($order->getExtensionAttributes()->getSpaazaData());
        }
    }
}
