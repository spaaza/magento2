<?php

namespace Spaaza\Loyalty\Observer;

use Magento\Framework\Event\ObserverInterface;

abstract class AbstractObserver implements ObserverInterface
{
    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Customer
     */
    protected $customerConnector;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    public function __construct(
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Model\Connector\Customer $customerConnector
    ) {
        $this->config = $config;
        $this->customerConnector = $customerConnector;
        $this->helper = $helper;
    }
}
