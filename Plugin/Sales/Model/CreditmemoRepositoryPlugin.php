<?php

namespace Spaaza\Loyalty\Plugin\Sales\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;

/**
 * Class CreditmemoRepositoryPlugin
 *
 * @see CreditmemoRepositoryInterface
 * @package Spaaza\Loyalty
 */
class CreditmemoRepositoryPlugin
{
    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var bool
     */
    protected $isNewCreditmemoSave;

    /**
     * @var \Spaaza\Loyalty\Model\Connector\Creditmemo
     */
    protected $creditmemoConnector;

    public function __construct(
        \Spaaza\Loyalty\Model\Connector\Creditmemo $creditmemoConnector,
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->creditmemoConnector = $creditmemoConnector;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * Add SpaazaData extension attribute to a loaded order
     *
     * @param CreditmemoRepositoryInterface $subject
     * @param CreditmemoInterface $creditmemo
     * @return CreditmemoInterface
     */
    public function afterGet(
        CreditmemoRepositoryInterface $subject,
        CreditmemoInterface $creditmemo
    ) {
        $this->spaazaDataManagement->applyExtensionAttributes($creditmemo);
        return $creditmemo;
    }

    /**
     * Set a local fag if it's a new credit memo that is getting saved
     *
     * @param CreditmemoRepositoryInterface $subject
     * @param CreditmemoInterface $creditmemo
     * @return array
     */
    public function beforeSave(
        CreditmemoRepositoryInterface $subject,
        CreditmemoInterface $creditmemo
    ) {
        $this->isNewCreditmemoSave = !$creditmemo->getEntityId();
        return [$creditmemo];
    }

    /**
     * Save Spaaza data extension attribute and sync the customer data to Spaaza
     *
     * @param CreditmemoRepositoryInterface $subject
     * @param CreditmemoInterface $creditmemo
     * @return CreditmemoInterface
     */
    public function afterSave(
        CreditmemoRepositoryInterface $subject,
        CreditmemoInterface $creditmemo
    ) {
        try {
            $this->saveExtensionAttribute($creditmemo);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        if ($this->isNewCreditmemoSave) {
            $this->sendCreditmemo($creditmemo);
        }
        return $creditmemo;
    }

    /**
     * Persist the Spaaza extension attribute
     *
     * @param CreditmemoInterface $creditmemo
     * @return void
     * @throws CouldNotSaveException
     */
    private function saveExtensionAttribute(CreditmemoInterface $creditmemo)
    {
        $extensionAttributes = $creditmemo->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            return;
        }

        try {
            $spaazaData->setCreditmemoId($creditmemo->getEntityId());
            $this->spaazaDataManagement->save($spaazaData);
            $this->helper->debugLog('Saved Spaaza data for credit memo', ['creditmemo' => $creditmemo->getEntityId()]);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save Spaaza data for credit memo %1: %2', $creditmemo->getEntityId(), $e->getMessage()),
                $e
            );
        }
    }

    /**
     * Send a credit memo to Spaaza
     *
     * @param CreditmemoInterface $creditmemo
     * @return void
     */
    private function sendCreditmemo(CreditmemoInterface $creditmemo)
    {
        try {
            $this->creditmemoConnector->sendCreditmemo($creditmemo);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
    }
}
