<?php

namespace Spaaza\Loyalty\Plugin\Quote\Model;

use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class CartRepositoryPlugin
 *
 * @see CartRepositoryInterface
 * @package Spaaza\Loyalty
 */
class CartRepositoryPlugin
{

    /**
     * @var \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    private $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * CartRepositoryPlugin constructor.
     *
     * @param \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Helper\Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Spaaza\Loyalty\Model\QuoteAddress\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * @param CartRepositoryInterface $subject
     * @param CartInterface $cart
     * @return CartInterface
     */
    public function afterGetForCustomer(
        CartRepositoryInterface $subject,
        CartInterface $cart
    ) {
        $this->applyToCart($cart);
        return $cart;
    }

    /**
     * @param CartRepositoryInterface $subject
     * @param CartInterface $cart
     * @return CartInterface
     */
    public function afterGetActiveForCustomer(
        CartRepositoryInterface $subject,
        CartInterface $cart
    ) {
        $this->applyToCart($cart);
        return $cart;
    }

    /**
     * @param CartRepositoryInterface $subject
     * @param CartInterface $cart
     * @return CartInterface
     */
    public function afterGetActive(
        CartRepositoryInterface $subject,
        CartInterface $cart
    ) {
        $this->applyToCart($cart);
        return $cart;
    }

    /**
     * Apply the extension attribute to the applicable address
     *
     * @param CartInterface $cart
     * @return void
     */
    private function applyToCart(CartInterface $cart)
    {
        if ($cart->getIsVirtual()) {
            $address = $cart->getBillingAddress();
        } else {
            $address = $cart->getShippingAddress();
        }
        if ($address) {
            $this->spaazaDataManagement->applyExtensionAttributes($address);
        }
    }

    /**
     * Save Spaaza data extension attribute
     *
     * @param CartRepositoryInterface $subject
     * @param null $result
     * @param CartInterface $cart
     * @return CartInterface
     */
    public function afterSave(
        CartRepositoryInterface $subject,
        $result,
        CartInterface $cart
    ) {
        try {
            if ($cart->getIsVirtual()) {
                $address = $cart->getBillingAddress();
            } else {
                $address = $cart->getShippingAddress();
            }
            if (!$address) {
                return $cart;
            }
            $this->saveExtensionAttribute($address);
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }

        return $cart;
    }

    /**
     * Persist the Spaaza extension attribute
     *
     * @param AddressInterface $address
     * @return void
     * @throws CouldNotSaveException
     */
    private function saveExtensionAttribute(AddressInterface $address)
    {
        $extensionAttributes = $address->getExtensionAttributes();
        if (!$extensionAttributes) {
            return;
        }

        $spaazaData = $extensionAttributes->getSpaazaData();
        if (!$spaazaData) {
            return;
        }

        try {
            $spaazaData->setQuoteAddressId($address->getId());
            if ($address->getId() && !$address->isDeleted()) {
                $this->spaazaDataManagement->save($spaazaData);
            }
            $this->helper->debugLog(
                'Saved Spaaza data for quote address',
                ['quote_address' => $address->getId()]
            );
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save Spaaza data for quote address %1: %2', $address->getId(), $e->getMessage()),
                $e
            );
        }
    }
}
