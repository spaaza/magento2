<?php

namespace Spaaza\Loyalty\Plugin\Customer\Model;

use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\AddressRepositoryInterface;

class AddressRepositoryPlugin
{
    /**
     * @var \Spaaza\Loyalty\Model\Connector\Customer
     */
    protected $customerConnector;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    /**
     * @var \Spaaza\Loyalty\Helper\Data
     */
    protected $helper;

    /**
     * @var \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement
     */
    protected $spaazaDataManagement;

    public function __construct(
        \Spaaza\Loyalty\Model\Connector\Customer $customerConnector,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Spaaza\Loyalty\Model\Config $config,
        \Spaaza\Loyalty\Helper\Data $helper,
        \Spaaza\Loyalty\Model\Customer\SpaazaDataManagement $spaazaDataManagement,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->customerConnector = $customerConnector;
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->logger = $logger;
        $this->config = $config;
        $this->customerRepository = $customerRepository;
        $this->helper = $helper;
    }

    /**
     * Save Spaaza data extension attribute
     *
     * @param AddressRepositoryInterface $subject
     * @param AddressInterface $result
     * @param AddressInterface $address
     * @return AddressInterface
     */
    public function afterSave(
        AddressRepositoryInterface $subject,
        AddressInterface $result,
        AddressInterface $address
    ) {
        try {
            if ($this->config->isSyncEnabled()) {
                if ($address->getCustomerId()) {
                    $customer = $this->customerRepository->getById($address->getCustomerId());

                    if (!$this->spaazaDataManagement->getDoNotSyncOnSave($customer)) {
                        $trySynchronous = $this->config->isSynchronousSyncEnabled();
                        $this->helper->debugLog(
                            'Update Spaaza user after address save',
                            ['address' => $address->getId()]
                        );
                        $this->customerConnector->updateSpaazaUserFromCustomer($customer, $trySynchronous);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->critical($e);
            $this->helper->debugLog('ERROR: ' . $e->getMessage());
        }

        return $address;
    }
}
