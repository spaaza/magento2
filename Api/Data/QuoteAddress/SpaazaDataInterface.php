<?php

namespace Spaaza\Loyalty\Api\Data\QuoteAddress;

interface SpaazaDataInterface
{
    const KEY_QUOTE_ADDRESS_ID = 'quote_address_id';
    const KEY_MEMBER_NUMBER = 'member_number';
    const KEY_USER_ID = 'user_id';
    const KEY_VOUCHERS = 'vouchers';
    const KEY_VOUCHER_AMOUNT = 'voucher_amount';
    const KEY_BASE_VOUCHER_AMOUNT = 'base_voucher_amount';
    const KEY_LAST_HASH = 'last_hash';
    const KEY_LAST_REQUEST_AT = 'last_request_at';
    const KEY_VOUCHER_DISTRIBUTION = 'voucher_distribution';

    /**
     * Get Quote Address Id
     *
     * @return int|null
     */
    public function getQuoteAddressId(): ?int;

    /**
     * Set Quote Address Id
     *
     * @param int|null $quoteAddressId
     * @return $this
     */
    public function setQuoteAddressId(?int $quoteAddressId);

    /**
     * Get Member Number
     *
     * @return string|null
     */
    public function getMemberNumber(): ?string;

    /**
     * Set Member Number
     *
     * @param string|null $memberNumber
     * @return $this
     */
    public function setMemberNumber(?string $memberNumber);

    /**
     * Get User Id
     *
     * @return int|null
     */
    public function getUserId(): ?int;

    /**
     * Set User Id
     *
     * @param int|null $userId
     * @return $this
     */
    public function setUserId(?int $userId);

    /**
     * Get Vouchers
     *
     * @return \Spaaza\Loyalty\Api\Data\VoucherInfoInterface[]
     */
    public function getVouchers(): array;

    /**
     * Set Vouchers
     *
     * @param \Spaaza\Loyalty\Api\Data\VoucherInfoInterface[] $vouchers
     * @return $this
     */
    public function setVouchers($vouchers);

    /**
     * Get Voucher Amount
     *
     * @return float
     */
    public function getVoucherAmount(): float;

    /**
     * Set Voucher Amount
     *
     * @param float $voucherAmount
     * @return $this
     */
    public function setVoucherAmount(float $voucherAmount);

    /**
     * Get Base Voucher Amount
     *
     * @return float
     */
    public function getBaseVoucherAmount();

    /**
     * Set Base Voucher Amount
     *
     * @param float $baseVoucherAmount
     * @return $this
     */
    public function setBaseVoucherAmount(float $baseVoucherAmount);

    /**
     * Get Last Hash
     *
     * @return string|null
     */
    public function getLastHash(): ?string;

    /**
     * Set Last Hash
     *
     * @param string $lastHash
     * @return $this
     */
    public function setLastHash(?string $lastHash);

    /**
     * Get Last Request At
     *
     * @return string|null
     */
    public function getLastRequestAt(): ?string;

    /**
     * Set Last Request At
     *
     * @param string $lastRequestAt
     * @return $this
     */
    public function setLastRequestAt(?string $lastRequestAt);

    /**
     * Get Voucher Distribution
     *
     * @return \Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface[]|null
     */
    public function getVoucherDistribution();

    /**
     * Set Voucher Distribution
     *
     * @param \Spaaza\Loyalty\Api\Data\VoucherDistributionItemInterface[]|null $voucherDistribution
     * @return $this
     */
    public function setVoucherDistribution($voucherDistribution);
}
