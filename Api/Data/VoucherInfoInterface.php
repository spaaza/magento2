<?php

namespace Spaaza\Loyalty\Api\Data;

interface VoucherInfoInterface
{
    const KEY_TYPE = 'type';
    const KEY_KEY = 'key';
    const KEY_AMOUNT = 'amount';
    const KEY_LABEL = 'label';

    /**
     * Get Type
     *
     * @return string
     */
    public function getType();

    /**
     * Set Type
     *
     * @param string $type
     * @return $this
     */
    public function setType(string $type);

    /**
     * Get Code
     *
     * @return string
     */
    public function getKey();

    /**
     * Set Code
     *
     * @param string $key
     * @return $this
     */
    public function setKey(string $key);

    /**
     * Get Amount
     *
     * @return float
     */
    public function getAmount();

    /**
     * Set Amount
     *
     * @param float $amount
     * @return $this
     */
    public function setAmount(float $amount);

    /**
     * Get Code
     *
     * @return string
     */
    public function getLabel();

    /**
     * Set Label
     *
     * @param string $label
     * @return $this
     */
    public function setLabel(string $label);
}
