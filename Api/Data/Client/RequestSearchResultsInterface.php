<?php

namespace Spaaza\Loyalty\Api\Data\Client;

interface RequestSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get Request list.
     *
     * @return RequestInterface[]
     */
    public function getItems();

    /**
     * Set Request list.
     *
     * @param RequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
