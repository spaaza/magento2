<?php

namespace Spaaza\Loyalty\Api\Data;

interface AuthenticationResultInterface
{
    const AUTHENTICATED = 'authenticated';
    const CUSTOMER_ID = 'customer_id';
    const MESSAGE = 'message';
    const WEBSITE_CODE = 'website_code';
    const STORE_CODE = 'store_code';

    /**
     * Get authenticated flag
     *
     * @return bool
     */
    public function getAuthenticated(): bool;

    /**
     * Set authenticated flag
     *
     * @param bool $authenticated
     * @return $this
     */
    public function setAuthenticated($authenticated);

    /**
     * Get customer id
     *
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * Set customer id
     *
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId(?int $customerId);

    /**
     * Get message
     *
     * @return string|null
     */
    public function getMessage(): ?string;

    /**
     * Set message
     *
     * @param string|null $message
     * @return $this
     */
    public function setMessage(?string $message);

    /**
     * Get website code
     *
     * @return string
     */
    public function getWebsiteCode(): ?string;

    /**
     * Set website code
     *
     * @param string|null $websiteCode
     * @return $this
     */
    public function setWebsiteCode(?string $websiteCode);

    /**
     * Get store code
     *
     * @return string
     */
    public function getStoreCode(): ?string;

    /**
     * Set store code
     *
     * @param string|null $storeCode
     * @return $this
     */
    public function setStoreCode(?string $storeCode);
}
