<?php

namespace Spaaza\Loyalty\Api\Data\Customer;

interface SpaazaDataInterface
{
    /**
     * Get Customer Id
     *
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * Set Customer Id
     *
     * @param int|null $customerId
     * @return $this
     */
    public function setCustomerId(?int $customerId);

    /**
     * Get User Id
     *
     * @return int|null
     */
    public function getUserId(): ?int;

    /**
     * Set User Id
     *
     * @param int|null $userId
     * @return $this
     */
    public function setUserId(?int $userId);

    /**
     * Get Member Number
     *
     * @return string|null
     */
    public function getMemberNumber(): ?string;

    /**
     * Set Member Number
     *
     * @param string|null $memberNumber
     * @return $this
     */
    public function setMemberNumber(?string $memberNumber);

    /**
     * Get Programme Opted In
     *
     * @return bool|null
     */
    public function getProgrammeOptedIn(): ?bool;

    /**
     * Set Programme Opted In
     *
     * @param bool|null $programmeOptedIn
     * @return $this
     */
    public function setProgrammeOptedIn(?bool $programmeOptedIn);

    /**
     * Get Mailing List Subscribed
     *
     * @return bool|null
     */
    public function getMailingListSubscribed(): ?bool;

    /**
     * Set Mailing List Subscribed
     *
     * @param bool|null $mailingListSubscribed
     * @return $this
     */
    public function setMailingListSubscribed(?bool $mailingListSubscribed);

    /**
     * Get Printed Mailing List Subscribed
     *
     * @return bool|null
     */
    public function getPrintedMailingListSubscribed(): ?bool;

    /**
     * Set Printed Mailing List Subscribed
     *
     * @param bool|null $printedMailingListSubscribed
     * @return $this
     */
    public function setPrintedMailingListSubscribed(?bool $printedMailingListSubscribed);

    /**
     * Get Referral Code
     *
     * @return string|null
     */
    public function getReferralCode(): ?string;

    /**
     * Set Referral Code
     *
     * @param string|null $referralCode
     * @return $this
     */
    public function setReferralCode(?string $referralCode);

    /**
     * Get Signup Referral Code (the referring user's referral code)
     *
     * @return string|null
     */
    public function getSignupReferralCode(): ?string;

    /**
     * Set Signup Referral Code (the referring user's referral code)
     *
     * @param string|null $referralCode
     * @return $this
     */
    public function setSignupReferralCode(?string $referralCode);

    /**
     * Get Last Hash
     *
     * @return string|null
     */
    public function getLastHash(): ?string;

    /**
     * Set Last Hash
     *
     * @param string|null $lastHash
     * @return $this
     */
    public function setLastHash(?string $lastHash);
}
