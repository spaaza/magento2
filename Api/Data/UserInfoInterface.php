<?php

namespace Spaaza\Loyalty\Api\Data;

interface UserInfoInterface
{
    const KEY_FIRST_NAME = 'first_name';
    const KEY_LAST_NAME = 'last_name';
    const KEY_BIRTHDAY = 'birthday';
    const KEY_USERNAME = 'username';
    const KEY_WEBSHOP_CUSTOMER_ID = 'webshop_customer_id';
    const KEY_GENDER = 'gender';
    const KEY_USER_ID = 'user_id';
    const KEY_PRINTED_MAILING_LIST_SUBSCRIBED = 'printed_mailing_list_subscribed';
    const KEY_MAILING_LIST_SUBSCRIBED = 'mailing_list_subscribed';
    const KEY_MAILING_LIST_SUB_OFFERED = 'mailing_list_sub_offered';
    const KEY_PROGRAMME_OPTED_IN = 'programme_opted_in';
    const KEY_ADDRESS_STREETNAME = 'address_streetname';
    const KEY_ADDRESS_HOUSENUMBER = 'address_housenumber';
    const KEY_ADDRESS_HOUSENUMBER_EXTENSION = 'address_housenumber_extension';
    const KEY_ADDRESS_POSTALCODE = 'address_postalcode';
    const KEY_ADDRESS_TOWNCITY = 'address_towncity';
    const KEY_COUNTRY_CODE = 'country_code';
    const KEY_PHONE_NUMBER = 'phone_number';

    /**
     * Get First Name
     *
     * @return string|null
     */
    public function getFirstName() :?string;

    /**
     * Set First Name
     *
     * @param string $firstName
     * @return $this
     */
    public function setFirstName(string $firstName);

    /**
     * Get Last Name
     *
     * @return string|null
     */
    public function getLastName(): ?string;

    /**
     * Set Last Name
     *
     * @param string $lastName
     * @return $this
     */
    public function setLastName(?string $lastName);

    /**
     * Get Birthday
     *
     * @return string|null
     */
    public function getBirthday(): ?string;

    /**
     * Set Birthday
     *
     * @param string|null $birthday
     * @return $this
     */
    public function setBirthday(?string $birthday);

    /**
     * Get Username
     *
     * @return string|null
     */
    public function getUsername(): ?string;

    /**
     * Set Username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username);

    /**
     * Get Webshop Customer Id
     *
     * @return int|null
     */
    public function getWebshopCustomerId(): ?int;

    /**
     * Set Webshop Customer Id
     *
     * @param int|null $webshopCustomerId
     * @return $this
     */
    public function setWebshopCustomerId(?int $webshopCustomerId);

    /**
     * Get Gender
     *
     * @return string|null
     */
    public function getGender(): ?string;

    /**
     * Set Gender
     *
     * @param string|null $gender
     * @return $this
     */
    public function setGender(?string $gender);

    /**
     * Get User Id
     *
     * @return int|null
     */
    public function getUserId(): ?int;

    /**
     * Set User Id
     *
     * @param int $userId
     * @return $this
     */
    public function setUserId(int $userId);

    /**
     * Get Printed Mailing List Subscribed
     *
     * @return bool|null
     */
    public function getPrintedMailingListSubscribed();

    /**
     * Set Printed Mailing List Subscribed
     *
     * @param bool|null $printedMailingListSubscribed
     * @return $this
     */
    public function setPrintedMailingListSubscribed(?bool $printedMailingListSubscribed);

    /**
     * Get Mailing List Subscribed
     *
     * @return bool|null
     */
    public function getMailingListSubscribed(): ?bool;

    /**
     * Set Mailing List Subscribed
     *
     * @param bool|null $mailingListSubscribed
     * @return $this
     */
    public function setMailingListSubscribed(?bool $mailingListSubscribed);

    /**
     * Get Mailing List Sub Offered
     *
     * @return bool|null
     */
    public function getMailingListSubOffered(): ?bool;

    /**
     * Set Mailing List Sub Offered
     *
     * @param bool|null $mailingListSubOffered
     * @return $this
     */
    public function setMailingListSubOffered(?bool $mailingListSubOffered);

    /**
     * Get Programme Opted In
     *
     * @return bool|null
     */
    public function getProgrammeOptedIn(): ?bool;

    /**
     * Set Programme Opted In
     *
     * @param bool|null $programmeOptedIn
     * @return $this
     */
    public function setProgrammeOptedIn(?bool $programmeOptedIn);

    /**
     * Get Address Streetname
     *
     * @return string|null
     */
    public function getAddressStreetname(): ?string;

    /**
     * Set Address Streetname
     *
     * @param string|null $addressStreetname
     * @return $this
     */
    public function setAddressStreetname(?string $addressStreetname);

    /**
     * Get Address Housenumber
     *
     * @return string|null
     */
    public function getAddressHousenumber(): ?string;

    /**
     * Set Address Housenumber
     *
     * @param string|null $addressHousenumber
     * @return $this
     */
    public function setAddressHousenumber(?string $addressHousenumber);

    /**
     * Get Address Housenumber Extension
     *
     * @return string|null
     */
    public function getAddressHousenumberExtension(): ?string;

    /**
     * Set Address Housenumber Extension
     *
     * @param string|null $addressHousenumberExtension
     * @return $this
     */
    public function setAddressHousenumberExtension(?string $addressHousenumberExtension);

    /**
     * Get Address Postalcode
     *
     * @return string|null
     */
    public function getAddressPostalcode(): ?string;

    /**
     * Set Address Postalcode
     *
     * @param string|null $addressPostalcode
     * @return $this
     */
    public function setAddressPostalcode(?string $addressPostalcode);

    /**
     * Get Address Towncity
     *
     * @return string|null
     */
    public function getAddressTowncity(): ?string;

    /**
     * Set Address Towncity
     *
     * @param string|null $addressTowncity
     * @return $this
     */
    public function setAddressTowncity(?string $addressTowncity);

    /**
     * Get Country Code
     *
     * @return string|null
     */
    public function getCountryCode(): ?string;

    /**
     * Set Country Code
     *
     * @param string|null $countryCode
     * @return $this
     */
    public function setCountryCode(?string $countryCode);

    /**
     * Get Phone Number
     *
     * @return string|null
     */
    public function getPhoneNumber(): ?string;

    /**
     * Set Phone Number
     *
     * @param string|null $phoneNumber
     * @return $this
     */
    public function setPhoneNumber(?string $phoneNumber);
}
