<?php

namespace Spaaza\Loyalty\Api\Data;

interface VoucherDistributionItemInterface
{
    const KEY_ITEM_BARCODE = 'item_barcode';
    const KEY_AMOUNT_TOTAL = 'amount_total';
    const KEY_QTY = 'qty';
    const KEY_RETAILER_ITEM_CODE = 'retailer_item_code';

    /**
     * Get Item Barcode
     *
     * @return string
     */
    public function getItemBarcode(): string;

    /**
     * Get Total Amount
     *
     * @return float
     */
    public function getAmountTotal(): float;

    /**
     * Get Qty
     *
     * @return float
     */
    public function getQty(): float;

    /**
     * Get Retailer Item Code
     *
     * @return string
     */
    public function getRetailerItemCode(): ?string;

    /**
     * Set Item Barcode
     *
     * @param string $itemBarcode
     * @return $this
     */
    public function setItemBarcode(string $itemBarcode): VoucherDistributionItemInterface;

    /**
     * Set Total Amount
     *
     * @param float $amount
     * @return $this
     */
    public function setAmountTotal(float $amount): VoucherDistributionItemInterface;

    /**
     * Set Qty
     *
     * @param float $qty
     * @return $this
     */
    public function setQty(float $qty): VoucherDistributionItemInterface;

    /**
     * Get Retailer Item Code
     *
     * @param string|null $itemCode
     * @return $this
     */
    public function setRetailerItemCode(?string $itemCode): VoucherDistributionItemInterface;
}
