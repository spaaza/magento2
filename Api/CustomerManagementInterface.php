<?php

namespace Spaaza\Loyalty\Api;

interface CustomerManagementInterface
{

    /**
     * Authenticate a customer using email and password
     *
     * @param string $email
     * @param string $password
     * @param int|null $spaazaChainId  Spaaza Chain ID. Only required when multiple chains are configured.
     * @return \Spaaza\Loyalty\Api\Data\AuthenticationResultInterface
     */
    public function authenticate(string $email, string $password, ?int $spaazaChainId = null);

    /**
     * Update customer data using Spaaza userinfo
     *
     * @param mixed $userInfo User data; same as the 'user_info' result in `get-card` Spaaza endpoint
     * @param bool $createIfNotExists Create a customer if it cannot be identified?
     * @param int|null $spaazaChainId Spaaza Chain ID.  Only required when multiple chains are configured.
     * @param string|null $websiteCode  The website code to assign a new customer to
     * @param string|null $storeCode  The store code to assign a new customer to
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function update(
        $userInfo,
        bool $createIfNotExists = false,
        ?int $spaazaChainId = null,
        ?string $websiteCode = null,
        ?string $storeCode = null
    );

    /**
     * Get Spaaza Data by customer id
     *
     * @param int $customerId
     * @return \Spaaza\Loyalty\Api\Data\UserInfoInterface
     */
    public function getSpaazaUserInfoByCustomerId(int $customerId);

    /**
     * Get Spaaza Data by customer email
     *
     * @param string $customerEmail
     * @return \Spaaza\Loyalty\Api\Data\UserInfoInterface
     */
    public function getSpaazaUserInfoByCustomerEmail(string $customerEmail);

    /**
     * Get Spaaza Data by customer email
     *
     * @param int $spaazaChainId
     * @param string $customerEmail
     * @return \Spaaza\Loyalty\Api\Data\UserInfoInterface
     */
    public function getSpaazaUserInfoByCustomerEmailAndChainId(int $spaazaChainId, string $customerEmail);
}
