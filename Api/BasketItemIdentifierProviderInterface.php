<?php

namespace Spaaza\Loyalty\Api;

use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

/**
 * Basket Item Identifier Provider
 *
 * Provides the identifier for basket items that will be used in calls to Spaaza.
 *
 * You can implement this interface yourself and add a preference in di.xml for it.
 * Also, you can define plugins for the individual methods to return a different code.
 */
interface BasketItemIdentifierProviderInterface
{
    /**
     * Get the identification of a product to send to Spaaza
     *
     * For now, sku is the only attribute that can be used. Of course, feel free to create
     * a plugin for this method
     *
     * See the documentation of Spaaza:
     * https://docs.spaaza.com/#adding-a-completed-basket
     *
     * An example for a result:
     * [
     *     'item_barcode' => '8712800188339'
     * ]
     *
     * @api
     * @param OrderItemInterface $orderItem
     * @return array  An array containing the identifier. This will be merged with the rest of the basket item.
     */
    public function getIdentifierForOrderItem(OrderItemInterface $orderItem): array;

    /**
     * Get the identification of a product to send to Spaaza
     *
     * @api
     * @see getIdentifierForOrderItem()
     * @param CreditmemoItemInterface $creditmemoItem
     * @return array
     */
    public function getIdentifierForCreditmemoItem(CreditmemoItemInterface $creditmemoItem): array;

    /**
     * Get the identification for a quote item
     *
     * In the case of a composite product (configurable, bundle, ...), only the first simple item will be provided.
     * Nice to know, but slightly irrelevant here: for configurable products, the parent item has the SKU of the simple
     * product and the product_id of the configurable product.
     *
     * @api
     * @see getIdentifierForOrderItem()
     * @param CartItemInterface $cartItem  A cart item, in case of a composite product: the simple item
     * @return array
     */
    public function getIdentifierForCartItem(CartItemInterface $cartItem): array;
}
