define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils'
    ],
    function (Component, totals, quote, priceUtils) {
        "use strict";

        var newValues = [];

        return Component.extend({
            defaults: {
                displayZeroTotal: false,
                template: 'Spaaza_Loyalty/checkout/total'
            },
            getFloatValue: function () {
                var value = 0;
                var segment = totals.getSegment('spaaza_loyalty_voucher');
                if (segment) {
                    value = segment.value;
                }
                return value;
            },
            getTitle: function () {
                var title = '';
                var segment = totals.getSegment('spaaza_loyalty_voucher');
                if (segment) {
                    title = segment.title;
                }
                return title;
            },
            getValue: function () {
                return this.getFormattedPrice(this.getFloatValue());
            },
            isDisplayed: function () {
                var value = this.getFloatValue();
                return (value != 0 && value != null) || this.displayZeroTotal;
            },
            getTotalsVouchers: function () {
                if(newValues.length < 1){
                    var segment = totals.getSegment('spaaza_loyalty_voucher');
                    segment.value.forEach(function (arrayItem) {
                        if (arrayItem.value < 0 && arrayItem.title.length > 0) {
                            newValues.push({
                                title: arrayItem.title,
                                value: priceUtils.formatPrice(arrayItem.value)
                            })
                        }
                    });
                }
                return newValues;
            }
        });
    }
);
