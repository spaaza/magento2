<?php

namespace Spaaza\Loyalty\Block\Sales\Order\Creditmemo;

/**
 * Class Totals
 *
 * @method \Magento\Sales\Block\Order\Creditmemo\Totals getParentBlock()
 */
class Totals extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * Totals constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->config = $config;
    }

    /**
     * @return \Magento\Sales\Api\Data\CreditmemoInterface
     */
    public function getCreditmemo()
    {
        return $this->getParentBlock()->getCreditmemo();
    }

    /**
     * @return $this
     */
    public function initTotals()
    {
        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($this->getCreditmemo());

        if ($spaazaData->getVoucherAmount() != 0
            || $this->config->showVoucherZeroTotal($this->getCreditmemo()->getStoreId())
        ) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'spaaza_loyalty_vouchers',
                    'value' => -1 * $spaazaData->getVoucherAmount(),
                    'base_value' => -1 * $spaazaData->getBaseVoucherAmount(),
                    'label' => __($this->config->getVoucherTotalLabel($this->getCreditmemo()->getStoreId())),
                ]
            );
            $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        }
        return $this;
    }
}
