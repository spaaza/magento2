<?php

namespace Spaaza\Loyalty\Block\Adminhtml\Config\Renderer;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Modules extends Field
{
    /**
     * @var string
     */
    protected $_template = 'Spaaza_Loyalty::config/about/modules.phtml';

    /**
     * @var \Spaaza\Loyalty\Model\DebugInfo\SpaazaModules
     */
    protected $spaazaModules;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Spaaza\Loyalty\Model\DebugInfo\SpaazaModules $spaazaModules,
        array $data = []
    )
    {
        $this->spaazaModules = $spaazaModules;
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    public function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Returns module names and versions
     *
     * @return array Array with keys 'name', 'version' and 'enabled'
     */
    public function getModulesAndVersions(): array
    {
        return $this->spaazaModules->getModulesAndVersions();
    }

    public function getClientVersion(): ?string
    {
       return $this->spaazaModules->getClientVersion();
    }
}
