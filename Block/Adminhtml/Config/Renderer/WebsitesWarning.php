<?php

namespace Spaaza\Loyalty\Block\Adminhtml\Config\Renderer;

use Magento\Framework\Data\Form\Element\Renderer\RendererInterface;

class WebsitesWarning extends \Magento\Backend\Block\Template implements RendererInterface
{
    const KEY_TITLE = 'title';
    const KEY_DESCRIPTION = 'description';

    /**
     * @var string
     */
    protected $_template = 'Spaaza_Loyalty::config/websites-warning.phtml';

    /**
     * @var \Spaaza\Loyalty\Model\Config\ConfigValidator
     */
    protected $configValidator;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Spaaza\Loyalty\Model\Config\ConfigValidator $configValidator,
        array $data = []
    ) {
        $this->configValidator = $configValidator;
        parent::__construct($context, $data);
    }

    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        try {
            $this->configValidator->validateCustomerScope();
        } catch (\Spaaza\Loyalty\Exception\ConfigValidationException $exception) {
            $this->setData(self::KEY_TITLE, (string)$exception->getMessage());
            $this->setData(self::KEY_DESCRIPTION, (string)$exception->getDescription());
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->setData(self::KEY_TITLE, (string)$exception->getMessage());
        }
        return $this->toHtml();
    }

    public function getTitle(): ?string
    {
        return $this->getData(self::KEY_TITLE);
    }

    public function getDescription(): ?string
    {
        return $this->getData(self::KEY_DESCRIPTION);
    }
}
