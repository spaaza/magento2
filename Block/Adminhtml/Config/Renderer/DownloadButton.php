<?php

namespace Spaaza\Loyalty\Block\Adminhtml\Config\Renderer;

use Magento\Framework\Data\Form\Element\AbstractElement;

class DownloadButton extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(
        AbstractElement $element
    ) {
        return $this->getButtonHtml();
    }

    public function getButtonHtml()
    {
        return $this->getLayout()
            ->createBlock(\Magento\Backend\Block\Widget\Button::class)
            ->setData(
                [
                    'id' => 'spaaza-download-debug-info',
                    'label' => __('Download Debug Info'),
                    'onclick' => 'window.location = ' . json_encode($this->getDownloadUrl()),
                ]
            )
            ->toHtml();
    }

    /**
     * Get the URL that downloads the debug info
     *
     * @return string
     */
    public function getDownloadUrl()
    {
        return $this->getUrl('spaaza/debugInfo/download');
    }
}
