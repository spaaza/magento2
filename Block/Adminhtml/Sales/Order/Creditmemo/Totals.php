<?php

namespace Spaaza\Loyalty\Block\Adminhtml\Sales\Order\Creditmemo;

/**
 * Class Totals
 *
 * @method \Magento\Sales\Block\Adminhtml\Order\Creditmemo\Totals getParentBlock()
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Order\Creditmemo\Totals
{
    /**
     * @var \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement
     */
    private $spaazaDataManagement;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    private $config;

    /**
     * Totals constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $spaazaDataManagement
     * @param \Spaaza\Loyalty\Model\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \Spaaza\Loyalty\Model\Creditmemo\SpaazaDataManagement $spaazaDataManagement,
        \Spaaza\Loyalty\Model\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $registry, $adminHelper, $data);
        $this->spaazaDataManagement = $spaazaDataManagement;
        $this->config = $config;
    }

    public function initTotals()
    {
        $creditmemo = $this->getCreditmemo();

        $spaazaData = $this->spaazaDataManagement->applyExtensionAttributes($creditmemo);
        if ($spaazaData->getVoucherAmount() > 0 || $this->config->showVoucherZeroTotal($creditmemo->getStoreId())) {
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'spaaza_loyalty_vouchers',
                    'value' => -1 * $spaazaData->getVoucherAmount(),
                    'base_value' => -1 * $spaazaData->getBaseVoucherAmount(),
                    'label' => __($this->config->getVoucherTotalLabel($creditmemo->getStoreId()))
                ]
            );
            $this->getParentBlock()->addTotalBefore($total, 'grand_total');
        }
        return $this;
    }
}
