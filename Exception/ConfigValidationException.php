<?php

namespace Spaaza\Loyalty\Exception;

use Magento\Framework\Phrase;

class ConfigValidationException extends \Magento\Framework\Exception\LocalizedException
{
    /**
     * @var Phrase|null
     */
    protected $description;

    public function __construct(
        ?Phrase $message,
        ?Phrase $description = null,
        ?int $code = 0,
        ?\Exception $cause = null
    ) {
        parent::__construct($message, $cause, $code);
        $this->description = $description;
    }

    public function getDescription(): ?Phrase
    {
        return $this->description;
    }
}
