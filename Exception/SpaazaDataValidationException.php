<?php

namespace Spaaza\Loyalty\Exception;

use Magento\Framework\Exception\LocalizedException;

class SpaazaDataValidationException extends LocalizedException
{
}
