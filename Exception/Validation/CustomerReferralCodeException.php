<?php

namespace Spaaza\Loyalty\Exception\Validation;

class CustomerReferralCodeException extends \Spaaza\Loyalty\Exception\SpaazaDataValidationException
{
}
