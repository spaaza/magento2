<?php

namespace Spaaza\Loyalty\Cron;

class SendRequests
{
    /**
     * @var \Spaaza\Loyalty\Model\Client\Request\Queue
     */
    private $queue;

    /**
     * SendRequests constructor.
     *
     * @param \Spaaza\Loyalty\Model\Client\Request\Queue $queue
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Client\Request\Queue $queue
    ) {
        $this->queue = $queue;
    }

    /**
     * Send pending requests to Spaaza
     */
    public function execute()
    {
        $result = $this->queue->sendPendingRequests();
    }
}
