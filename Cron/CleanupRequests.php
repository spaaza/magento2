<?php

namespace Spaaza\Loyalty\Cron;

class CleanupRequests
{
    /**
     * @var \Spaaza\Loyalty\Model\Client\RequestManagement
     */
    protected $requestManagement;

    /**
     * SendRequests constructor.
     *
     * @param \Spaaza\Loyalty\Model\Client\RequestManagement $requestManagement
     */
    public function __construct(
        \Spaaza\Loyalty\Model\Client\RequestManagement $requestManagement
    ) {
        $this->requestManagement = $requestManagement;
    }

    /**
     * Clean up sent requests
     */
    public function execute()
    {
        $result = $this->requestManagement->cleanupRequests();
    }
}
