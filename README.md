Spaaza Magento 2 Module
=======================

The Spaaza Magento Module connects Magento 2 to Spaaza's loyalty and incentive marketing APIs. It provides basic
synchronisation via the Spaaza API, an internal API to handle vouchers in the checkout and an external (REST) API
to authenticate and synchronise customer data.

Please see more details on installation, use and other tips in the [docs](docs/readme.md) directory.

If you have questions about the Spaaza Magento 2 module or wish to find out more about our incentive marketing and
loyalty platform and services, please get in touch via the chat button on our [website](https://www.spaaza.com).
