<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Notification\MessageInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends AbstractHelper
{
    const TOTAL_CODE = 'spaaza_loyalty_voucher';

    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var \Magento\Framework\Notification\NotifierPool
     */
    protected $notifierPool;

    /**
     * @var \Magento\Catalog\Api\ProductAttributeRepositoryInterface
     */
    protected $productAttributeRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Spaaza\Loyalty\Model\Config\Source\EntityType
     */
    protected $entityTypeSource;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Spaaza\Loyalty\Model\Config
     */
    protected $config;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Spaaza\Loyalty\Model\Config\Source\EntityType $entityTypeSource,
        \Magento\Framework\Notification\NotifierPool $notifierPool,
        ProductAttributeRepositoryInterface $productAttributeRepository,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        \Spaaza\Loyalty\Model\Config $config,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->moduleList = $moduleList;
        $this->notifierPool = $notifierPool;
        $this->entityTypeSource = $entityTypeSource;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * Get the Magento module version
     *
     * @return string
     */
    public function getModuleVersion()
    {
        return (string)$this->moduleList->getOne('Spaaza_Loyalty')['setup_version'];
    }

    /**
     * Get the label for an entity type
     *
     * @param $type
     * @return string
     */
    public function getEntityTypeLabel($type)
    {
        return $this->entityTypeSource->getLabelForValue($type);
    }

    /**
     * Add a line to the debug log
     *
     * @param mixed $message  Logs this message; logs an error if this is an Exception object
     * @param array $context
     * @return void
     */
    public function debugLog($message, $context = [])
    {
        if ($this->config->isDebugLogEnabled()) {
            if ($message instanceof \Exception) {
                $context = ['type' => get_class($message)];
                $this->logger->error($message->getMessage(), $context);
            } else {
                $this->logger->info($message, $context);
            }
        }
    }

    // TODO: publish the error
    // phpcs:ignore
    public function publishError($type, $title, $message, $severity = MessageInterface::SEVERITY_MINOR, $context = [])
    {
    }

    /**
     * Get the sort order to use for displaying the vouchers total
     *
     * @return int
     */
    public function getVoucherTotalSortOrder()
    {
        return $this->config->getVoucherTotalSortOrder();
    }

    /**
     * Get the label to use for the vouchers total
     *
     * @return string
     */
    public function getVoucherTotalLabel()
    {
        return $this->config->getVoucherTotalLabel();
    }

    /**
     * Get is_promotional attribute if available.
     * Check if it returns a boolean and if this product makes use of it (e.g. it is set to true).
     */
    public function checkIfProductSkuIsSetToPromotional($sku)
    {
        $isPromotional = false;
        $attributeToLookFor = $this->config->getIsPromotionalAttribute();
        if(isset($attributeToLookFor) && strlen($attributeToLookFor) > 0) {
            try {
                $attribute = $this->productAttributeRepository->get(trim($attributeToLookFor));
            } catch (\Exception $e) {
                //
            }
        }
        if(isset($attribute)) {
            $storeId = $this->storeManager->getStore()->getId();
            try {
                $product = $this->productRepository->get($sku, false, $storeId);
            } catch (\Exception $e) {
                //
            }
            if(isset($product)) {
                $isPromotional = $product->getData(trim($attributeToLookFor));
            }
        } else{
            // attribute does not exist or product does not have this attribute, sku is '.$sku
        }
        return $isPromotional;
    }
}
