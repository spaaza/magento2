<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Quote\Api\Data\CartInterface;

/**
 * @deprecated Use {@link \Spaaza\Loyalty\Model\Connector\Identifier\LockingCodeProvider}
 */
class Quote extends AbstractHelper
{
    /**
     * @var \Spaaza\Loyalty\Model\Connector\Identifier\LockingCodeProvider
     */
    protected $lockingCodeProvider;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Spaaza\Loyalty\Model\Connector\Identifier\LockingCodeProvider $lockingCodeProvider
    ) {
        parent::__construct($context);
        $this->lockingCodeProvider = $lockingCodeProvider;
    }

    /**
     * Compose the voucher locking code for a quote
     *
     * @param CartInterface $quote
     * @return string|null
     * @deprecated Use {@link \Spaaza\Loyalty\Model\Connector\Identifier\LockingCodeProvider::getLockingCodeForCart()}
     */
    public function getVoucherLockingCode(CartInterface $quote)
    {
        if ($quote->getId() > 0) {
            return $this->lockingCodeProvider->getLockingCodeForCart((int)$quote->getId());
        }
        return null;
    }
}
