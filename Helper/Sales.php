<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Spaaza\Loyalty\Model\Connector\Identifier\BasketCodeProvider;

/**
 * @deprecated Use {@link \Spaaza\Loyalty\Model\Connector\Identifier\BasketCodeProvider}
 */
class Sales extends AbstractHelper
{
    /**
     * @var BasketCodeProvider
     */
    protected $basketCodeProvider;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        BasketCodeProvider $basketCodeProvider
    ) {
        parent::__construct($context);
        $this->basketCodeProvider = $basketCodeProvider;
    }

    /**
     * Get the owner code for an order
     *
     * @deprecated Use {@link BasketCodeProvider::getBasketCodeForOrder()}
     * @param OrderInterface $order
     * @return string
     */
    public function getOrderOwnerCode(OrderInterface $order)
    {
        return $this->basketCodeProvider->getBasketCodeForOrder($order);
    }

    /**
     * Get the owner code for a credit memo; distinct it from orders
     *
     * @deprecated Use {@link BasketCodeProvider::getBasketCodeForCreditmemo()}
     * @param CreditmemoInterface $creditmemo
     * @return string
     */
    public function getCreditmemoOwnerCode(CreditmemoInterface $creditmemo)
    {
        return $this->basketCodeProvider->getBasketCodeForCreditmemo($creditmemo);
    }
}
