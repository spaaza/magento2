<?php

namespace Spaaza\Loyalty\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Sales\Api\Data\CreditmemoItemInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface;

/**
 * @see \Spaaza\Loyalty\Model\Connector\Identifier\BasketItemIdentifierProvider
 * @deprecated Use {@link \Spaaza\Loyalty\Api\BasketItemIdentifierProviderInterface}
 */
class Catalog extends AbstractHelper
{
    /**
     * @var BasketItemIdentifierProviderInterface
     */
    protected $basketItemIdentifierProvider;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        BasketItemIdentifierProviderInterface $basketItemIdentifierProvider
    ) {
        parent::__construct($context);
        $this->basketItemIdentifierProvider = $basketItemIdentifierProvider;
    }

    /**
     * Get the identification of a product to send to Spaaza
     *
     * For now, sku is the only attribute that can be used. Of course, feel free to create
     * a plugin for this method!
     *
     * See the documentation of Spaaza:
     * https://docs.spaaza.com/#adding-a-completed-basket
     *
     * @deprecated Use {@link BasketItemIdentifierProviderInterface::getIdentifierForOrderItem()}
     * @param OrderItemInterface $orderItem
     * @return array
     */
    public function getBasketItemIdentification(OrderItemInterface $orderItem)
    {
        return $this->basketItemIdentifierProvider->getIdentifierForOrderItem($orderItem);
    }

    /**
     *
     * @deprecated Use {@link BasketItemIdentifierProviderInterface::getIdentifierForCreditmemoItem()}
     * @param CreditmemoItemInterface $creditmemoItem
     * @return array
     */
    public function getBasketItemIdentificationForCreditmemoItem(CreditmemoItemInterface $creditmemoItem)
    {
        return $this->basketItemIdentifierProvider->getIdentifierForCreditmemoItem($creditmemoItem);
    }

    /**
     * Get the identification for a quote item
     *
     * @deprecated Use {@link BasketItemIdentifierProviderInterface::getIdentifierForCartItem()}
     * @param \Magento\Quote\Api\Data\CartItemInterface $cartItem
     * @return array
     */
    public function getBasketItemIdentificationForCartItem(\Magento\Quote\Api\Data\CartItemInterface $cartItem): array
    {
        return $this->basketItemIdentifierProvider->getIdentifierForCartItem($cartItem);
    }
}
